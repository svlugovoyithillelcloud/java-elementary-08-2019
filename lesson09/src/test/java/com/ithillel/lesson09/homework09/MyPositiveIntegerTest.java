package com.ithillel.lesson09.homework09;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MyPositiveIntegerTest {

    @Test
    void parseIntPositive() {
        assertEquals(123, MyPositiveInteger.parseInt("123"));
    }

    @Test
    void testMyNumberFormatException() {
        assertThrows(MyPositiveInteger.MyNumberFormatException.class, () -> MyPositiveInteger.parseInt("aaa"));
    }

    @Test
    void testMyPositiveInteger() {
        assertThrows(MyPositiveInteger.MyNegativeIntegerException.class, () -> MyPositiveInteger.parseInt("-123"));
    }

    @Test
    void testMyGreatThenMaxIntegerException() {
        assertThrows(MyPositiveInteger.MyGreatThenMaxIntegerException.class, () -> MyPositiveInteger.parseInt("99999999999999999"));
    }

    @Test
    void testExactMessageMyNumberFormatException() {
        Throwable exception = assertThrows(MyPositiveInteger.MyNumberFormatException.class, () -> MyPositiveInteger.parseInt("aaa"));
        assertEquals("aaa has not integer value format.", exception.getMessage());
    }

    @Test
    void testExactMessageMyNegativeIntegerException() {
        Throwable exception = assertThrows(MyPositiveInteger.MyNegativeIntegerException.class, () -> MyPositiveInteger.parseInt("-123"));
        assertEquals("-123 is not positive integer value.", exception.getMessage());
    }

    @Test
    void testExactMessageMyGreatThenMaxIntegerException() {
        Throwable exception = assertThrows(MyPositiveInteger.MyGreatThenMaxIntegerException.class, () -> MyPositiveInteger.parseInt("99999999999999999"));
        assertEquals("99999999999999999 greater then max integer value.", exception.getMessage());
    }
}