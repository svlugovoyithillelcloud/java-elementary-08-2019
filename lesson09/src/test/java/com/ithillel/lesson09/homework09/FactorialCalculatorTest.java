package com.ithillel.lesson09.homework09;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class FactorialCalculatorTest {
    @Test
    void positiveCalculateFactorial() {
        FactorialCalculator calculator = new FactorialCalculator();

        Assertions.assertEquals(120, calculator.calculateFactorial(5));
        Assertions.assertEquals(1, calculator.calculateFactorial(0));
        Assertions.assertEquals(479001600, calculator.calculateFactorial(12));

    }

    @Test
    void negativeCalculateFactorial() {
        FactorialCalculator calculator = new FactorialCalculator();

        Assertions.assertThrows(IllegalArgumentException.class, () -> calculator.calculateFactorial(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> calculator.calculateFactorial(13));
        Assertions.assertNotEquals(1200, calculator.calculateFactorial(5));
    }
}