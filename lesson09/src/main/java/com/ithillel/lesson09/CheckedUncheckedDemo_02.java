package com.ithillel.lesson09;

import java.io.IOException;
import java.sql.SQLException;

public class CheckedUncheckedDemo_02 {

    void method1() throws IOException{}
    void method2() throws SQLException {}
    void method3() throws RuntimeException{}

    void method4() throws SQLException {
        try {
            method1();
        } catch (IOException e) {
            e.printStackTrace();
        }
        method2();
        method3();
    }

    void method5(){
        try {
            method4();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
