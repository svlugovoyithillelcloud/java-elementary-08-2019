package com.ithillel.lesson09;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.sql.SQLException;


public class MultiCatchDemo_03 {
    public static void main(String[] args) {

        try {
            execQueryDataBase();
            readFromFile();
            doSmth();
        } catch (SQLException e) {
            // handle
        } catch (IOException e) {
            // handle
        } catch (Exception e) {
            // handle
        }

        try {
            execQueryDataBase();
            readFromFile();
            doSmth();
        } catch (SQLException | IOException e) {
            // handle
        } catch (Exception e) {
            // handle
        }
    }

    static void execQueryDataBase() throws SQLException {
        System.out.println("Go to DB...");
    }

    static void readFromFile() throws IOException {
        System.out.println("Read from file...");
    }

    static void doSmth() throws Exception {
        System.out.println("Do smth...");
    }
}
