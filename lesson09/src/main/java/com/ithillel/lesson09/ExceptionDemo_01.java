package com.ithillel.lesson09;

public class ExceptionDemo_01 {
    public static void main(String[] args) {

        try {
            int a = 10 / 0;
            System.out.println("try block after 10 / 0");
        } catch (ArithmeticException ex) {
            System.out.println("catch block");
        } finally {
            System.out.println("finally block");
        }

        System.out.println("======================");

        try {
            int[] arr = {1};
            arr[10] = 15;
            int a = 10 / 0;
        } catch (ArithmeticException ex) {
            System.out.println("ArithmeticException catch block");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBoundsException catch block");
        } finally {
            System.out.println("finally block");
        }

        System.out.println("======================");

        try {
            int a = 10 / 0;
//        } catch (Exception ex) {
//            System.out.println("Exception catch block");
        } catch (ArithmeticException ex) {
            System.out.println("ArithmeticException catch block");
//            System.exit(0);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBoundsException catch block");
        } finally {
            System.out.println("finally block");
        }

        System.out.println("======================");

        try {
            int a = 10 / 0;
        } catch (ArithmeticException ex) {
            System.out.println(ex);
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
