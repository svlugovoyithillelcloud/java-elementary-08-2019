package com.ithillel.lesson09;

public class CustomException_04 {
    public static void main(String[] args) {
        Coin coin =new Coin();

        try {
            coin.setDiameter(-2.0);
        } catch (CoinLogicException e) {
            e.printStackTrace();
        }
    }
}

class Coin {
    private double diameter;
    private double weight;

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double value) throws CoinLogicException {
        if (value <= 0) {
            throw new CoinLogicException("diameter is incorrect");
        }
        diameter = value;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double value) {
        weight = value;
    }
}

class CoinLogicException extends Exception {

    public CoinLogicException() {
    }

    public CoinLogicException(String message, Throwable exception) {
        super(message, exception);
    }

    public CoinLogicException(String message) {
        super(message);
    }

    public CoinLogicException(Throwable exception) {
        super(exception);
    }
}