package com.ithillel.lesson09.homework09;

public class Runner {
    public static void main(String[] args) {

        int i = Integer.parseInt("123");

        System.out.println(MyPositiveInteger.parseInt("123"));
        MyPositiveInteger.parseInt("aaa"); // MyNumberFormatException
        MyPositiveInteger.parseInt("-123"); // MyNegativeIntegerException
        MyPositiveInteger.parseInt("99999999999999999"); // MyGreatThenMaxIntegerException

        FactorialCalculator fc = new FactorialCalculator();
        int res = fc.calculateFactorial(5); //120
        System.out.println(res);
    }
}
