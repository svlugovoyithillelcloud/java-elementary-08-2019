package com.ithillel.lesson09.homework09;

public class FactorialCalculator {

    public int calculateFactorial(int i) {
        int res = 1;

        if (i == 0) {
            return res;
        } else if (i < 0 || i > 12) {
            throw new IllegalArgumentException("Factorial input value should be between 0 and 12");
        } else {
            for (int j = 1; j <= i; j++) {
                res *= j;
            }
            return res;
        }
    }
}
