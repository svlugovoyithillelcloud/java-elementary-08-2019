package com.ithillel.lesson09.homework09;

public class MyPositiveInteger {

    static int parseInt(String s) {
        long num = 0;
        for (int i = 0; i < s.length(); i++) {
            if (((int) s.charAt(i) >= 48) && ((int) s.charAt(i) <= 57) || ((int) s.charAt(i) == 45)) {
                num = num * 10 + ((int) s.charAt(i) - 48);
            } else {
                throw new MyNumberFormatException(s);
            }

        }
        if (num < 0) {
            throw new MyNegativeIntegerException(s);
        }

        if (num > Integer.MAX_VALUE) {
            throw new MyGreatThenMaxIntegerException(s);
        }

        return (int) num;
    }

    public static class MyNumberFormatException extends RuntimeException {
        MyNumberFormatException(String message) {
            super(message + " has not integer value format.");
        }
    }

    public static class MyNegativeIntegerException extends RuntimeException {
        MyNegativeIntegerException(String message) {
            super(message + " is not positive integer value.");
        }
    }

    public static class MyGreatThenMaxIntegerException extends RuntimeException {
        MyGreatThenMaxIntegerException(String message) {
            super(message + " greater then max integer value.");
        }
    }

}
