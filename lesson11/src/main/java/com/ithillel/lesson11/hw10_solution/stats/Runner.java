package com.ithillel.lesson11.hw10_solution.stats;

public class Runner {
    public static void main(String[] args) {

        TextStatistics stats = FileAnalyzer.analyzeTextFile("/Users/serhiiluhovyi/Movies/hello.txt");
        System.out.println(stats);

        boolean success = FileAnalyzer.writeStatisticsToTextFile(stats, "/Users/serhiiluhovyi/Movies/hello_stats.txt");
        System.out.println(success);

    }
}
