import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Greeter {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("What is your first name?");
        String firstName = scanner.next();

        System.out.println("What is your last name?");
        String lastName = scanner.next();

        System.out.println("What date you was born (dd-mm-yyyy)?");
        String birthday = scanner.next();

        int age = calcAge(birthday, "dd-MM-yyyy");

        System.out.println("Hello, " + firstName + " " + lastName + ". Your age is " + age + ".");
    }

    private static int calcAge(String birthday, String pattern) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern);
        LocalDate dateOfBirthday = LocalDate.parse(birthday, dtf);
        LocalDate now = LocalDate.now();

        return Period.between(dateOfBirthday, now).getYears();
    }
}
