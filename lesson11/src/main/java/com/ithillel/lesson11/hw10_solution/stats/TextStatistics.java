package com.ithillel.lesson11.hw10_solution.stats;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TextStatistics {
    private String fileName;
    private String absolutePathToFile;
    private int linesCount;
    private int wordsCount;
    private int maxLineLength;
    private int minLineLength;
    private int spacesCount;
}
