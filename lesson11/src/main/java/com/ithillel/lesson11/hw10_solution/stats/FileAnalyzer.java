package com.ithillel.lesson11.hw10_solution.stats;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FileAnalyzer {

    public static TextStatistics analyzeTextFile(String pathToFile) {

        File file = new File(pathToFile);
        if (!file.exists()) {
            throw new RuntimeException("File not exists.");
        }

        List<String> strings = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(pathToFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                strings.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        strings.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.compare(o1.length(), o2.length());
            }
        });

        TextStatistics stats = new TextStatistics();
        stats.setFileName(file.getName());
        stats.setAbsolutePathToFile(file.getAbsolutePath());
        stats.setLinesCount(strings.size());
        stats.setMinLineLength(strings.get(0).length());
        stats.setMaxLineLength(strings.get(strings.size() - 1).length());
        stats.setWordsCount(calcWordCount(strings));
        stats.setSpacesCount(calcSpaceCount(strings));

        return stats;
    }

    private static int calcWordCount(List<String> strings) {
        int wordCount = 0;
        for (String str : strings) {
            String[] arr = str.split(" ");
            wordCount += arr.length;
        }
        return wordCount;
    }

    private static int calcSpaceCount(List<String> strings) {
        int spaceCount = 0;
        for (String str : strings) {
            for (char c : str.toCharArray()) {
                if (c == ' ') {
                    spaceCount++;
                }
            }
        }
        return spaceCount;
    }

    public static boolean writeStatisticsToTextFile(TextStatistics statistics, String pathToFile) {

        String res = generatePrettyReport(statistics);

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(pathToFile))) {
            bw.write(res);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static String generatePrettyReport(TextStatistics stats) {
        return "Statistics:\n" +
                "\nfileName: " + stats.getFileName() +
                "\nabsolutePathToFile: " + stats.getAbsolutePathToFile() +
                "\nlinesCount: " + stats.getLinesCount() +
                "\nwordsCount: " + stats.getWordsCount() +
                "\nmaxLineLength: " +stats.getMaxLineLength() +
                "\nminLineLength: " +stats.getMinLineLength() +
                "\nspacesCount: " + stats.getWordsCount() + "\n";
    }
}
