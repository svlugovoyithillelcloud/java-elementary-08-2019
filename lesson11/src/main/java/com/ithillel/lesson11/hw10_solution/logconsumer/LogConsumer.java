package com.ithillel.lesson11.hw10_solution.logconsumer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class LogConsumer {
    public static void main(String[] args) throws InterruptedException {

        File folder = new File("/Users/serhiiluhovyi/Movies/logconsumer");

        while (true) {

            System.out.println("===== Started processing...");

            File[] listOfFiles = folder.listFiles();

            for (File file : listOfFiles) {
                if (file.isFile() && !file.isHidden()) {
                    System.out.println(file.getName());
                    printToConsole(file);
                    File renamed = renameFile(file);
                    createArcFolderIfNorExists();
                    moveFileToArcFolder(renamed);
                }
            }

            System.out.println("===== Completed processing...");

            Thread.sleep(15000);

        }
    }

    private static void printToConsole(File file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File renameFile(File file) {
        Instant instant = Instant.now();
        long timeStampMillis = instant.toEpochMilli();
        String fileName = file.getName();
        String nameBeforePoint = fileName.substring(0, fileName.length() - 4);
        String newName = "/Users/serhiiluhovyi/Movies/logconsumer/" + nameBeforePoint + "-" + timeStampMillis + "-arc.txt";
        File dest = new File(newName);
        file.renameTo(dest);
        return dest;
    }

    private static void createArcFolderIfNorExists() {
        File folder = new File("/Users/serhiiluhovyi/Movies/logconsumer");
        File arc = new File("/Users/serhiiluhovyi/Movies/logconsumer/arc");
        File[] listOfFiles = folder.listFiles();
        List<String> folders = new ArrayList<>();
        for (File file : listOfFiles) {
            folders.add(file.getName());
        }
        if (!folders.contains("arc")) {
            arc.mkdir();
        }
    }

    private static void moveFileToArcFolder(File file) {
        String fileName = file.getName();
        file.renameTo(new File("/Users/serhiiluhovyi/Movies/logconsumer/arc/" + fileName));
    }
}
