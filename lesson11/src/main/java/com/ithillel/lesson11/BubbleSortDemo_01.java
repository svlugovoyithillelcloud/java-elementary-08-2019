package com.ithillel.lesson11;

import java.util.Arrays;

public class BubbleSortDemo_01 {
    public static void main(String args[]) {

        int[] arr = {64, 34, 25, 12, 22, 11, 90};
        System.out.println(Arrays.toString(arr));

        bubbleSort(arr);

        System.out.println("Sorted array");
        System.out.println(Arrays.toString(arr));
    }

    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++)
            for (int j = 0; j < arr.length - i - 1; j++)
                if (arr[j] > arr[j + 1]) {
                    // swap arr[j+1] and arr[i]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
    }
}
