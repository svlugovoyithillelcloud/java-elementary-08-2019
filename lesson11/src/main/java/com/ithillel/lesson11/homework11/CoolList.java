package com.ithillel.lesson11.homework11;

public interface CoolList<T> {

    boolean add(T element);

    T get(int index);

    int size();

    int capacity();

    boolean isEmpty();

    void clear();
}
