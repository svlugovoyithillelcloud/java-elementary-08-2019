package com.ithillel.lesson11.homework11;

import lombok.AllArgsConstructor;
import lombok.ToString;

public class Runner {
    public static void main(String[] args) {

        CoolList<Integer> ints = new ArrayCoolList<>();

        ints.add(1);
        ints.add(5);
        ints.add(10);

        System.out.println(ints.size()); //3
        System.out.println(ints.get(2)); //10
        System.out.println(ints.isEmpty()); //false
        System.out.println(ints.capacity()); //10

        for (int i = 0; i < 8; i++) {
            ints.add(99);
        }

        System.out.println(ints.size()); //11
        System.out.println(ints.get(5)); //99
        System.out.println(ints.capacity()); //20

        ints.clear();
        System.out.println(ints.isEmpty()); //true
        System.out.println(ints.size()); //0

        CoolList<String> strings = new ArrayCoolList<>();

        strings.add("hello");
        strings.add("world");
        System.out.println(strings.size()); //2
        System.out.println(strings.capacity()); //10

        CoolList<Person> persons = new ArrayCoolList<>();
        persons.add(new Person(1L, "John"));
        persons.add(new Person(2L, "Anne"));
        System.out.println(persons.size()); //2
    }
}

@AllArgsConstructor
@ToString
class Person {
    private Long id;
    private String name;
}