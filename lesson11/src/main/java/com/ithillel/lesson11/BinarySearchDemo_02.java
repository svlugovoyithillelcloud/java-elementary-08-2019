package com.ithillel.lesson11;

import java.util.Arrays;

public class BinarySearchDemo_02 {
    public static void main(String args[]) {

        int[] arr = {10, 20, 30, 40, 50};
        int key = 40;
        int last = arr.length - 1;

        binarySearch(arr, 0, last, key);

        int res1 = binarySearchRec(arr, 0, last, key);
        if (res1 == -1) {
            System.out.println("Element is not found!");
        } else {
            System.out.println("Element is found at index: " + res1);
        }

        int res2 = Arrays.binarySearch(arr, key);
        if (res2 < 0)
            System.out.println("Element is not found!");
        else
            System.out.println("Element is found at index: " + res2);

    }

    public static void binarySearch(int[] arr, int first, int last, int key) {
        int mid = (first + last) / 2;
        while (first <= last) {
            if (arr[mid] < key) {
                first = mid + 1;
            } else if (arr[mid] == key) {
                System.out.println("Element is found at index: " + mid);
                break;
            } else {
                last = mid - 1;
            }
            mid = (first + last) / 2;
        }
        if (first > last) {
            System.out.println("Element is not found!");
        }
    }

    public static int binarySearchRec(int[] arr, int first, int last, int key) {
        if (last >= first) {
            int mid = first + (last - first) / 2;
            if (arr[mid] == key) {
                return mid;
            }
            if (arr[mid] > key) {
                return binarySearchRec(arr, first, mid - 1, key);//search in left subarray
            } else {
                return binarySearchRec(arr, mid + 1, last, key);//search in right subarray
            }
        }
        return -1;
    }

}
