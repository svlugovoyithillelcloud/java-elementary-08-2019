package com.ithillel.lesson11;

public class MyLinkedListRunner {
    public static void main(String[] args) {

        MyList<Integer> integers = new MyLinkedList<>();
        integers.add(1);
        integers.add(1, 2);
        integers.add(3);

        System.out.println(integers.size());
        System.out.println(integers.isEmpty());
        System.out.println(integers.contains(3));
        System.out.println(integers.contains(33));
        System.out.println(integers.get(0));

        MyList<String> strings = MyLinkedList.of("hello", "world", "java");

        System.out.println(strings.size());
        System.out.println(strings.isEmpty());
        System.out.println(strings.contains("hello"));
        System.out.println(strings.contains("kotlin"));
        System.out.println(strings.get(1));
    }


}
