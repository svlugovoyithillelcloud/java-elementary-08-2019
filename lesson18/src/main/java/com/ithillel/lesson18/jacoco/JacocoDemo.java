package com.ithillel.lesson18.jacoco;

public class JacocoDemo {

    public String demo(int i) {
        if (i == 1) {
            return "One";
        } else if (i == 2){
            return "Two";
        }
        return "Unknown";
    }
}
