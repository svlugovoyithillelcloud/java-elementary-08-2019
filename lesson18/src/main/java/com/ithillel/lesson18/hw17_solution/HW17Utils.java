package com.ithillel.lesson18.hw17_solution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

public class HW17Utils {

    private static final Logger LOG = LoggerFactory.getLogger(HW17Utils.class);

    public static void main(String[] args) {

        Map<Integer, String> map = new HashMap<>() {{
            put(1, "One");
            put(2, "Two");
            put(3, "Three");
            put(5, "Two");
        }};

        Map<String, Collection<Integer>> inversed = inverse(map);
        System.out.println(inversed); //{One=[1], Two=[2, 5], Three=[3]}


        String[] arr = {"a", "b", "a", "b", "c"};
        System.out.println(countValues(arr)); //{a=2, b=2, c=1}

        Integer[] squares = new Integer[100];
        fill(squares, integer -> integer * integer);
        System.out.println(Arrays.toString(squares)); //[0, 1, 4, 9, 16, 25, 36, 49, 64, ... ]

        LOG.trace("HW17Utils - Hello World!");
    }

    public static <K, V> Map<V, Collection<K>> inverse(Map<K, V> map) {

        Map<V, Collection<K>> resultMap = new HashMap<>();
        Set<K> keys = map.keySet();

        for (K key : keys) {
            V value = map.get(key);
            resultMap.compute(
                    value,
                    (v, ks) -> {
                        if (ks == null) {
                            ks = new HashSet<>();
                        }
                        ks.add(key);
                        return ks;
                    });
        }
        return resultMap;
    }

    public static <K> Map<K, Integer> countValues(K[] ks) {

        Map<K, Integer> map = new HashMap<>();

        for (K k : ks) {
            map.compute(
                    k,
                    new BiFunction<K, Integer, Integer>() {
                        @Override
                        public Integer apply(K k, Integer count) {
                            return count == null ? 1 : count + 1;
                        }
                    });
        }
        return map;
    }

    public static <T> void fill(T[] objects, Function<Integer, ? extends T> function) {

        for (int i = 0; i < objects.length; i++) {
            objects[i] = function.apply(i);
        }
    }

}
