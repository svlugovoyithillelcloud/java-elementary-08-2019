package com.ithillel.lesson18.jacoco;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JacocoDemoTest {

    @Test
    void demo() {
        JacocoDemo jacocoDemo = new JacocoDemo();
        String res = jacocoDemo.demo(1);
        assertEquals("One", res);

        String res1 = jacocoDemo.demo(2);
        assertEquals("Two", res1);
    }
}