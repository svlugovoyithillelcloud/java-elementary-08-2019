package com.ithillel.lesson21.hw20_solution.book_hw.service;



import com.ithillel.lesson21.hw20_solution.book_hw.dao.BookDao;
import com.ithillel.lesson21.hw20_solution.book_hw.dao.BookDaoImpl;
import com.ithillel.lesson21.hw20_solution.book_hw.model.Book;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

public class BookServiceImpl implements BookService {

    private BookDao bookDao;

    public BookServiceImpl(DataSource dataSource) {
        this.bookDao = new BookDaoImpl(dataSource);
    }

    @Override
    public List<Book> findBooksWithPagesGreaterThan(int count) {

        List<Book> books = bookDao.findAll();

        return books.stream()
                .filter(b -> b.getPageCount() > 300)
                .collect(Collectors.toList());
    }

    @Override
    public List<Book> searchInBooksContainsStringInName(String str) {

        List<Book> books = bookDao.findAll();

        return books.stream()
                .filter(b -> b.getName().contains(str))
                .collect(Collectors.toList());
    }
}
