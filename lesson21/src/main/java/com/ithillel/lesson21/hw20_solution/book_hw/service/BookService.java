package com.ithillel.lesson21.hw20_solution.book_hw.service;


import com.ithillel.lesson21.hw20_solution.book_hw.model.Book;

import java.util.List;

public interface BookService {

    List<Book> findBooksWithPagesGreaterThan(int count);

    List<Book> searchInBooksContainsStringInName(String str);

}
