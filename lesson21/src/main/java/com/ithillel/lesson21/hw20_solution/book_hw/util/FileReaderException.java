package com.ithillel.lesson21.hw20_solution.book_hw.util;

public class FileReaderException extends RuntimeException {
    public FileReaderException(String message, Exception e) {
        super(message, e);
    }
}
