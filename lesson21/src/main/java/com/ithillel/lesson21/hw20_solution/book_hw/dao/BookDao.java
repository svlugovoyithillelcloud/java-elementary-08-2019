package com.ithillel.lesson21.hw20_solution.book_hw.dao;


import com.ithillel.lesson21.hw20_solution.book_hw.model.Book;

import java.util.List;

public interface BookDao {

    Long save(Book book);

    List<Book> findAll();

    Book findById(Long id);

    void update(Book book);

    void remove(Long id);

}
