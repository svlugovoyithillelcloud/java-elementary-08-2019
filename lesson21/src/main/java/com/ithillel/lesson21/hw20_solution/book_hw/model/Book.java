package com.ithillel.lesson21.hw20_solution.book_hw.model;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@ToString
public class Book {
    private Long id;
    private String name;
    private String author;
    private int pageCount;
    private LocalDate publishDate;
}
