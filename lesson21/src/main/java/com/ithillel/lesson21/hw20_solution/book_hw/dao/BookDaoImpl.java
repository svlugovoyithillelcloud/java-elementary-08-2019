package com.ithillel.lesson21.hw20_solution.book_hw.dao;


import com.ithillel.lesson21.hw20_solution.book_hw.exception.DaoOperationException;
import com.ithillel.lesson21.hw20_solution.book_hw.model.Book;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BookDaoImpl implements BookDao {

    public static final String FIND_ALL_SQL = "SELECT * FROM books";
    public static final String SAVE_SQL = "INSERT INTO books (name, author, page_count, publish_date) VALUES (?, ?, ?, ?);";
    public static final String FIND_BY_ID_SQL = "SELECT * FROM books WHERE id = ?";
    public static final String UPDATE_SQL = "UPDATE books SET name = ?, author = ?, page_count = ?, publish_date = ? WHERE id = ?";
    public static final String DELETE_SQL = "DELETE FROM books WHERE id = ?";

    private DataSource dataSource;

    public BookDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Long save(Book book) {
        Objects.requireNonNull(book);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SAVE_SQL, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, book.getName());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setInt(3, book.getPageCount());
            preparedStatement.setDate(4, Date.valueOf(book.getPublishDate()));
            preparedStatement.executeUpdate();

            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                long id = generatedKey.getLong("id");
                book.setId(id);
                return id;
            } else {
                throw new DaoOperationException("No Id returned after save book");
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error saving book", e);
        }
    }

    @Override
    public List<Book> findAll() {
        List<Book> books = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(FIND_ALL_SQL);

            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getLong("id"));
                book.setName(resultSet.getString("name"));
                book.setAuthor(resultSet.getString("author"));
                book.setPageCount(resultSet.getInt("page_count"));
                book.setPublishDate(resultSet.getDate("publish_date").toLocalDate());
                books.add(book);
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error during find all books", e);
        }
        return books;
    }

    @Override
    public Book findById(Long id) {
        Objects.requireNonNull(id);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_SQL);
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getLong("id"));
                book.setName(resultSet.getString("name"));
                book.setAuthor(resultSet.getString("author"));
                book.setPageCount(resultSet.getInt("page_count"));
                book.setPublishDate(resultSet.getDate("publish_date").toLocalDate());
                return book;
            } else {
                throw new DaoOperationException("Can not find book with id " + id);
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error during find book", e);
        }
    }

    @Override
    public void update(Book book) {
        Objects.requireNonNull(book);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL);
            preparedStatement.setString(1, book.getName());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setInt(3, book.getPageCount());
            preparedStatement.setDate(4, Date.valueOf(book.getPublishDate()));
            preparedStatement.setLong(5, book.getId());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new DaoOperationException("Can not update book with id " + book.getId());
            }
        } catch (SQLException e) {
            throw new DaoOperationException("Error during update book", e);
        }
    }

    @Override
    public void remove(Long id) {
        Objects.requireNonNull(id);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoOperationException("Error during delete actor with id = " + id, e);
        }
    }
}
