Homework 21

1) Создаем приватный репо на битбакете contact-list-app
Добавляем меня в коллабораторы

2) Создаем мавен проект contact-list-app
Добавляем ремоут на репо

3) Db model

Person
- firstName
- lastName
- gender
- birthday
- city
- email
- List<PhoneNumber>

PhoneNumber
- phone
- type

Продумываем 2 таблицы, типы данных, констрейнты
В resources создаем db/migration папку со скриптами
- table_initialization.sql
- table_population.sql (10 записей)

4) Java model
Создаем пакет model и классы для доменной области

5) Заглушки для будущей работы
Создаем пакеты dao, service, exception и классы/интерфейсы без реализации (просто структуру)


Жду ссылку на репо как решение домашки.

