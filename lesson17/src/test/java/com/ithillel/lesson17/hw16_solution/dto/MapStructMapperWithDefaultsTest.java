package com.ithillel.lesson17.hw16_solution.dto;

import com.ithillel.lesson17.hw16_solution.dto.mappers.MapStructMapperWithDefaults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class MapStructMapperWithDefaultsTest {

    MapStructMapperWithDefaults mapper = Mappers.getMapper(MapStructMapperWithDefaults.class);
    private SecretPerson person;
    private SecretPersonDto dto;

    @BeforeEach
    void setUp() {
        person = new SecretPerson(99L, "qwerty", "John", "Smith", LocalDate.of(1990, 10, 25));
        dto = new SecretPersonDto("Genry", "Stone", LocalDate.of(1980, 5, 13));
    }

    @Test
    void fromDto() {
        SecretPerson person = mapper.fromDto(dto);
        assertEquals(
                new SecretPerson(99999L, "Default Password", "Genry", "Stone", LocalDate.of(1980, 5, 13)),
                person);
    }

    @Test
    void toDto() {
        SecretPersonDto dto = mapper.toDto(person);
        assertEquals(
                new SecretPersonDto("John", "Smith", LocalDate.of(1990, 10, 25)),
                dto);
    }
}