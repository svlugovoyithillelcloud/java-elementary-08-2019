package com.ithillel.lesson17.stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectorsPartitioningAndGroupingDemo_04 {
    public static void main(String[] args) {

//        Partitioning and Grouping

        List<String> strings = Arrays.asList("this", "is", "a", "long", "list", "of",
                "strings", "to", "use", "as", "a", "demo");

        Map<Boolean, List<String>> lengthMapEven = strings.stream()
                .collect(
                        Collectors.partitioningBy(
                                s -> s.length() % 2 == 0)
                );
        lengthMapEven.forEach((key,value) -> System.out.printf("%5s: %s%n", key, value));



        Map<Integer, List<String>> lengthMap = strings.stream()
                .collect(Collectors.groupingBy(String::length));
        lengthMap.forEach((k,v) -> System.out.printf("%d: %s%n", k, v));

    }
}
