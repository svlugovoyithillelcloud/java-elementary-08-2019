package com.ithillel.lesson17.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectorsDownstreamDemo_05 {
    public static void main(String[] args) {

//        Downstream Collectors

//          Problem
//          You want to postprocess the collections returned by a groupingBy or partitioningBy operation.

        List<String> strings = Arrays.asList("this", "is", "a", "long", "list", "of",
                "strings", "to", "use", "as", "a", "demo");

        Map<Boolean, List<String>> lengthMapEven = strings.stream()
                .collect(
                        Collectors.partitioningBy(s -> s.length() % 2 == 0)
                );
        lengthMapEven.forEach((key,value) -> System.out.printf("%5s: %s%n", key, value));
//        false: [a, strings, use, a]
//        true: [this, is, long, list, of, to, as, demo]


        Map<Boolean, Long> numberLengthMap = strings.stream()
                .collect(
                        Collectors.partitioningBy(s -> s.length() % 2 == 0,
                        Collectors.counting()));
        numberLengthMap.forEach((k,v) -> System.out.printf("%5s: %d%n", k, v));
//        false: 4
//        true: 8

    }
}
