package com.ithillel.lesson17.stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectorsCollectionToMapDemo_03 {
    public static void main(String[] args) {

//        Adding a Linear Collection to a Map

        List<Book> books = Arrays.asList(
                new Book(1, "Modern Java Recipes", 49.99),
                new Book(2, "Java 8 in Action", 49.99),
                new Book(3, "Java SE8 for the Really Impatient", 39.99),
                new Book(4, "Functional Programming in Java", 27.64),
                new Book(5, "Making Java Groovy", 45.99),
                new Book(6, "Gradle Recipes for Android", 23.76)
        );

        Map<Integer, Book> bookMap1 = books.stream()
                .collect(Collectors.toMap(Book::getId, b -> b));

        Map<Integer, Book> bookMap2 = books.stream()
                .collect(Collectors.toMap(Book::getId, Function.identity()));

//        The first toMap uses the getId method to map to the key and an
//        explicit lambda expression that simply returns its parameter.

//        The second example uses the static identity method in Function to do the same thing.
    }
}

@AllArgsConstructor
@Getter
@ToString
class Book {
    private int id;
    private String name;
    private double price;
}