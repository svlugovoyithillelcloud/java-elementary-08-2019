package com.ithillel.lesson17.stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class CollectorsMaxAndMinValuesDemo_06 {
    public static void main(String[] args) {

//        Finding Max and Min Values

//          static <T> BinaryOperator<T> maxBy(Comparator<? super T> comparator)
//          static <T> BinaryOperator<T> minBy(Comparator<? super T> comparator)

        List<Employee> employees = Arrays.asList(
                new Employee("Cersei", 250_000, "Lannister"),
                new Employee("Jamie", 150_000, "Lannister"),
                new Employee("Tyrion", 1_000, "Lannister"),
                new Employee("Tywin", 1_000_000, "Lannister"),
                new Employee("Jon Snow", 75_000, "Stark"),
                new Employee("Robb", 120_000, "Stark"),
                new Employee("Eddard", 125_000, "Stark"),
                new Employee("Sansa", 0, "Stark"),
                new Employee("Arya", 1_000, "Stark"));
        Employee defaultEmployee =
                new Employee("A man (or woman) has no name", 0, "Black and White");

        //1
        Optional<Employee> optionalEmp = employees.stream()
                .reduce(BinaryOperator.maxBy(Comparator.comparingInt(Employee::getSalary)));
        System.out.println("Emp with max salary: " + optionalEmp.orElse(defaultEmployee));

        //2
        optionalEmp = employees.stream()
                .max(Comparator.comparingInt(Employee::getSalary));
        System.out.println("Emp with max salary: " + optionalEmp.orElse(defaultEmployee));

        //3
        OptionalInt maxSalary = employees.stream()
                .mapToInt(Employee::getSalary)
                .max();
        System.out.println("The max salary is " + maxSalary);

        //4
        optionalEmp = employees.stream()
                .collect(Collectors.maxBy(Comparator.comparingInt(Employee::getSalary)));
        System.out.println("Emp with max salary: " + optionalEmp.orElse(defaultEmployee));


//        Using Collectors.maxBy as a downstream collector
        Map<String, Optional<Employee>> map = employees.stream()
                .collect(Collectors.groupingBy(
                        Employee::getDepartment,
                        Collectors.maxBy( //minBy
                                Comparator.comparingInt(Employee::getSalary))
                        )
                );
        map.forEach((house, emp) ->
                System.out.println(house + ": " + emp.orElse(defaultEmployee)));

    }
}

@AllArgsConstructor
@Getter
@ToString
class Employee {
    private String name;
    private Integer salary;
    private String department;
}