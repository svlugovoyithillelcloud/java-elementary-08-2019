package com.ithillel.lesson17.stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class Actor {
    private String name;
    private String role;
}
