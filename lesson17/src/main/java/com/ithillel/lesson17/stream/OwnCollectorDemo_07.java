package com.ithillel.lesson17.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Stream;

public class OwnCollectorDemo_07 {
    public static void main(String[] args) {

        Collector<String, List<String>, List<String>> myToList =
                Collector.of(
                        ArrayList::new,
                        List::add,
                        (l1, l2) -> {
                            l1.addAll(l2);
                            return l1;
                        }
                );

        List<String> superHeroes =
                Stream.of("Mr. Furious", "The Blue Raja", "The Shoveler",
                        "The Bowler", "Invisible Boy", "The Spleen", "The Sphinx")
                        .collect(myToList);

        System.out.println(superHeroes);
    }
}
