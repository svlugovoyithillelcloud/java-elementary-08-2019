package com.ithillel.lesson17.stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectorsStreamIntoCollectionDemo_02 {
    public static void main(String[] args) {

//        Converting a Stream into a Collection

        List<String> superHeroes =
                Stream.of("Mr. Furious", "The Blue Raja", "The Shoveler",
                        "The Bowler", "Invisible Boy", "The Spleen", "The Sphinx")
                        .collect(Collectors.toList());

        Set<String> villains =
                Stream.of("Casanova Frankenstein", "The Disco Boys",
                        "The Not-So-Goodie Mob", "The Suits", "The Suzies",
                        "The Furriers", "The Furriers")
                        .collect(Collectors.toSet());

        List<String> actors =
                Stream.of("Hank Azaria", "Janeane Garofalo", "William H. Macy",
                        "Paul Reubens", "Ben Stiller", "Kel Mitchell", "Wes Studi")
                        .collect(Collectors.toCollection(LinkedList::new));

        String[] wannabes =
                Stream.of("The Waffler", "Reverse Psychologist", "PMS Avenger")
                        .toArray(String[]::new);




//        Creating a Map

        Set<Actor> movieActors = new HashSet<>() {{
            add(new Actor("John Smith", "Bond007"));
            add(new Actor("Brad Pit", "Batman"));
            add(new Actor("Leo DiCaprio", "Mr Cool"));
        }};

        Map<String, String> actorMap = movieActors.stream()
                .collect(Collectors.toMap(Actor::getName, Actor::getRole));
        System.out.println(actorMap);
    }
}
