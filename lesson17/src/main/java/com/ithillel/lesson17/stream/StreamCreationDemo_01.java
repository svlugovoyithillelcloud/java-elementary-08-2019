package com.ithillel.lesson17.stream;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StreamCreationDemo_01 {
    public static void main(String[] args) throws Exception {


//        1) Из коллекции:
        Stream<String> fromCollection = Arrays.asList("x", "y", "z").stream();


//        2) Из набора значений:
        Stream<String> fromValues = Stream.of("x", "y", "z");


//        3) Из массива:
        String[] array = {"x", "y", "z"};
        Stream<String> fromArray = Arrays.stream(array);


//        4) Из файла (каждая строка в файле будет отдельным элементом в стриме):
        Stream<String> fromFile = Files.lines(Paths.get("lesson17/input.txt"));
        fromFile.forEach(System.out::println);


//        5) Из строки:
        IntStream fromString = "01234abcDE56789".chars();
        fromString.forEach(System.out::println);


//        6) С помощью Stream.builder():
        Stream.Builder<String> builder = Stream.builder();

        Stream<String> fromBuilder =builder
                .add("z")
                .add("y")
                .add("z").build();
        fromBuilder.forEach(System.out::println);


//        7) С помощью Stream.iterate() (бесконечный) - static <T> Stream<T> iterate(T seed, UnaryOperator<T> f)
        Stream<Integer> fromIterate = Stream.iterate(1, n -> n + 1);
//        fromIterate.forEach(el -> {
//            try {
//                Thread.sleep(1000);
//                System.out.println(el);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });

        Stream<BigDecimal> fromIterate1 = Stream.iterate(BigDecimal.ONE, n -> n.add(BigDecimal.ONE) )
                        .limit(10);
        fromIterate1.forEach(System.out::println);

//          prints 10 days starting from today
        Stream.iterate(LocalDate.now(), ld -> ld.plusDays(1L))
                .limit(10)
                .forEach(System.out::println);


//        8) С помощью Stream.generate() (бесконечный) - static <T> Stream<T> generate(Supplier<T> s)
        Stream<String> fromGenerate = Stream.generate(() -> "0");
//        fromGenerate.forEach(el -> {
//            try {
//                Thread.sleep(1000);
//                System.out.println(el);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });

        Stream.generate(Math::random)
                .limit(10)
                .forEach(System.out::println);


//        9) Using range and rangeClosed
        IntStream.range(10, 15)
                .boxed()
//                .mapToObj(i -> Integer.valueOf(i))
                .forEach(System.out::println);
        // prints [10, 11, 12, 13, 14]

        LongStream.rangeClosed(10, 15)
                .forEach(System.out::println);
        // prints [10, 11, 12, 13, 14, 15]

    }
}
