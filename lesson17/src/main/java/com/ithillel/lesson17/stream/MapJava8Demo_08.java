package com.ithillel.lesson17.stream;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MapJava8Demo_08 {
    public static void main(String[] args) {

//      Default Methods in Map

        Map<Integer, String> map = new HashMap<>() {{
            put(1, "One");
            put(2, "Two");
            put(3, "Three");
        }};

        //putIfAbsent
        map.putIfAbsent(1, "Aa");
        map.putIfAbsent(50, "Hello");
        System.out.println(map);

        //forEach
        map.forEach((k, v) -> System.out.println(k + "===" + v));

        //getOrDefault
        System.out.println(map.getOrDefault(1, "Default"));
        System.out.println(map.getOrDefault(111, "Default"));

       //compute
        String compute = map.compute(1, (k, v) -> String.valueOf(k).concat(v));
        System.out.println(compute);
        System.out.println(map);

        //computeIfAbsent
        String s1 = map.computeIfAbsent(4, n -> n + ": Four");
        String s2 = map.computeIfAbsent(3, n -> n + ": Three");
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(map);

        //computeIfPresent
        String passage = "NSA agent walks into a bar. Bartender says, " +
                "'Hey, I have a new joke for you.' Agent says, 'heard it'.";
        Map<String, Integer> counts = countWords(passage, "NSA", "agent", "joke");
        counts.forEach((word, count) -> System.out.println(word + "=" + count));

        //merge
        map.merge(1, "NEW", (value, newValue) -> value.concat(newValue));
        map.merge(150, "NEW", (value, newValue) -> value.concat(newValue));
        System.out.println(map);
    }

    public static Map<String, Integer> countWords(String passage, String... strings) {
        Map<String, Integer> wordCounts = new HashMap<>();
        Arrays.stream(strings).forEach(s -> wordCounts.put(s, 0));
        Arrays.stream(passage.split(" ")).forEach(word ->
                wordCounts.computeIfPresent(word, (key, val) -> val + 1));
        return wordCounts;
    }
}
