package com.ithillel.lesson17.hw16_solution.dto.mappers;

public interface CoolMapper<T, D> {
    T fromDto(D dto);
    D toDto(T obj);
}
