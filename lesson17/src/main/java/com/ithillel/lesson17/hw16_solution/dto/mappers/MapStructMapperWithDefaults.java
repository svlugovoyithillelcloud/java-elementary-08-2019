package com.ithillel.lesson17.hw16_solution.dto.mappers;

import com.ithillel.lesson17.hw16_solution.dto.SecretPerson;
import com.ithillel.lesson17.hw16_solution.dto.SecretPersonDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public abstract class MapStructMapperWithDefaults {

    public SecretPerson fromDto(SecretPersonDto dto){
        SecretPerson person = new SecretPerson();
        person.setFirstName(dto.getFirst_name());
        person.setLastName(dto.getLast_name());
        person.setBirthday(dto.getBirth());
        person.setId(99999L);
        person.setPassword("Default Password");

        return person;
    }

    @Mappings({
            @Mapping(target="first_name", source="obj.firstName"),
            @Mapping(target="last_name", source="obj.lastName"),
            @Mapping(target="birth", source="obj.birthday")
    })
    public abstract SecretPersonDto toDto(SecretPerson obj);
}
