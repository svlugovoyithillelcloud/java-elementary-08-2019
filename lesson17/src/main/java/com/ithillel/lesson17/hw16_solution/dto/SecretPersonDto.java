package com.ithillel.lesson17.hw16_solution.dto;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SecretPersonDto {
    private String first_name;
    private String last_name;
    private LocalDate birth;
}
