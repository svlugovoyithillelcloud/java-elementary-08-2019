package com.ithillel.lesson17.hw16_solution;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class ArrayListCapacity {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        ArrayList<Integer> arrayList = new ArrayList<>();

        System.out.println(getCapacity(arrayList)); //0

        arrayList.add(1);
        System.out.println(getCapacity(arrayList)); //10

        arrayList.remove(0);
        System.out.println(getCapacity(arrayList)); //10

        for (int i = 0; i < 12; i++) {
            arrayList.add(i);
        }
        System.out.println(getCapacity(arrayList)); //15

    }

    public static int getCapacity(ArrayList<?> al) throws NoSuchFieldException, IllegalAccessException {

        Field dataField = ArrayList.class.getDeclaredField("elementData");
        dataField.setAccessible(true);

        return ((Object[])dataField.get(al)).length;
    }
}
