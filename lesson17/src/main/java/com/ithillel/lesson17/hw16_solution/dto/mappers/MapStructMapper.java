package com.ithillel.lesson17.hw16_solution.dto.mappers;

import com.ithillel.lesson17.hw16_solution.dto.SecretPerson;
import com.ithillel.lesson17.hw16_solution.dto.SecretPersonDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface MapStructMapper {

    @Mappings({
            @Mapping(target="firstName", source="dto.first_name"),
            @Mapping(target="lastName", source="dto.last_name"),
            @Mapping(target="birthday", source="dto.birth")
    })
    SecretPerson fromDto(SecretPersonDto dto);

    @Mappings({
            @Mapping(target="first_name", source="obj.firstName"),
            @Mapping(target="last_name", source="obj.lastName"),
            @Mapping(target="birth", source="obj.birthday")
    })
    SecretPersonDto toDto(SecretPerson obj);
}
