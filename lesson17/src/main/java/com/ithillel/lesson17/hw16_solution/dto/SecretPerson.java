package com.ithillel.lesson17.hw16_solution.dto;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SecretPerson {
    private Long id; // secret field from database
    private String password; // secret field from database
    private String firstName;
    private String lastName;
    private LocalDate birthday;
}
