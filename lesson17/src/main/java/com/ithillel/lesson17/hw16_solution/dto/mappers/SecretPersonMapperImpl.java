package com.ithillel.lesson17.hw16_solution.dto.mappers;

import com.ithillel.lesson17.hw16_solution.dto.SecretPerson;
import com.ithillel.lesson17.hw16_solution.dto.SecretPersonDto;

public class SecretPersonMapperImpl implements CoolMapper<SecretPerson, SecretPersonDto>{

    @Override
    public SecretPerson fromDto(SecretPersonDto dto) {
        SecretPerson person = new SecretPerson();
        person.setFirstName(dto.getFirst_name());
        person.setLastName(dto.getLast_name());
        person.setBirthday(dto.getBirth());
        person.setId(99999L);
        person.setPassword("Default Password");

        return person;
    }

    @Override
    public SecretPersonDto toDto(SecretPerson obj) {
        SecretPersonDto dto = new SecretPersonDto();
        dto.setFirst_name(obj.getFirstName());
        dto.setLast_name(obj.getLastName());
        dto.setBirth(obj.getBirthday());

        return dto;
    }
}
