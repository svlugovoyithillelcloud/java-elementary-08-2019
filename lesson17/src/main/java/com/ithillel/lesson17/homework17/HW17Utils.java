package com.ithillel.lesson17.homework17;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class HW17Utils {

    public static void main(String[] args) {

        Map<Integer, String> map = new HashMap<>() {{
            put(1, "One");
            put(2, "Two");
            put(3, "Three");
            put(5, "Two");
        }};

        Map<String, Collection<Integer>> inversed = inverse(map);
        System.out.println(inversed); //{One=[1], Two=[2, 5], Three=[3]}


        String[] arr = {"a", "b", "a", "b", "c"};
        System.out.println(countValues(arr)); //{a=2, b=2, c=1}

        Integer[] squares = new Integer[100];
        fill(squares, integer -> integer * integer);
        System.out.println(Arrays.toString(squares)); //[0, 1, 4, 9, 16, 25, 36, 49, 64, ... ]
    }

    public static <K, V> Map<V, Collection<K>> inverse(Map<K, V> map) {
        throw new RuntimeException("Please implement me!");
    }

    public static <K> Map<K, Integer> countValues(K[] ks) {
        throw new RuntimeException("Please implement me!");
    }

    public static <T> void fill(T[] objects, Function<Integer, ? extends T> function) {
        throw new RuntimeException("Please implement me!");
    }

}
