package com.ithillel.lesson06;

public class VarargsDemo {

    public static void main(String... args) {

        Operation op = new Operation();

        System.out.println(op.sumElements(1));
        System.out.println(op.sumElements(2, 2));
        System.out.println(op.sumElements(2, 2, 2, 2, 2));
    }
}

class Operation {
    int sumElements(int q, int... ints) {
        int sum = 0;

        for (int e : ints) {
            sum = sum + e;
        }
        return sum;
    }
}