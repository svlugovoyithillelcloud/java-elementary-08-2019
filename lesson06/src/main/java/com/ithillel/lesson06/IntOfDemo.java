package com.ithillel.lesson06;

public class IntOfDemo {

    public static void main(String[] args) {

        String s = "qqq";

        System.out.println(s instanceof String);

        Child child = new Child();

        System.out.println(child instanceof Child);
        System.out.println(child instanceof Parent);
        System.out.println(child instanceof SomeInterf);




    }
}

class Child extends Parent /* implements SomeInterf */ {

}

class Parent {}

interface SomeInterf{}


