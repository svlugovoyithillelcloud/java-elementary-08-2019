package com.ithillel.lesson06;

public class ClassDemoParent {

    { System.out.println("Parent Block..."); }

    static { System.out.println("Parent Static Block... 1"); }

    static { System.out.println("Parent Static Block... 2"); }

    //состояние
    private String nameP;



    //способы создания объектов

    public ClassDemoParent(String name) {
        this.nameP = name;
        System.out.println("Parent Constructor...");
    }


}
