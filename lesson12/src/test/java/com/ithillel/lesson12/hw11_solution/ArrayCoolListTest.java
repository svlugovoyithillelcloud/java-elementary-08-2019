package com.ithillel.lesson12.hw11_solution;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayCoolListTest {

    private CoolList<String> list;

    @BeforeEach
    void setUp() {
        list = new ArrayCoolList<>();
    }

    @Test
    void shouldAddToList() {
        assertEquals(0, list.size());
        boolean success = list.add("hello");
        assertTrue(success);
        assertEquals(1, list.size());
        ((ArrayCoolList) list).display();
    }

    @Test
    void shouldGetFromListByIndex() {
        list.add("hello");
        assertEquals("hello", list.get(0));
        list.add("world");
        assertEquals("world", list.get(1));
        ((ArrayCoolList) list).display();
    }

    @Test
    void shouldThrowExceptions() {
        list.add("hello");
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(10));
        assertThrows(IllegalArgumentException.class, () -> list.get(-1));
    }

    @Test
    void shouldIncreaseSize() {
        assertEquals(0, list.size());
        list.add("hello");
        list.add("world");
        assertEquals("hello", list.get(0));
        assertEquals("world", list.get(1));
        assertEquals(2, list.size());
        for (int i = 0; i < 10; i++) {
            list.add("qwerty");
        }
        assertEquals(12, list.size());
        ((ArrayCoolList) list).display();
    }

    @Test
    void shouldIncreaseCapacity() {
        assertEquals(10, list.capacity());
        list.add("hello");
        assertEquals(10, list.capacity());
        for (int i = 0; i < 10; i++) {
            list.add("qwerty");
        }
        assertEquals(20, list.capacity());
    }

    @Test
    void shouldCheckIsEmpty() {
        assertTrue(list.isEmpty());
        list.add("hello");
        assertFalse(list.isEmpty());
    }

    @Test
    void shouldClearList() {
        list.add("hello");
        list.clear();
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());
    }

    @Test
    void ClearShouldNotChangeCapacity() {
        list.add("hello");
        list.clear();
        assertEquals(10, list.capacity());
        for (int i = 0; i < 25; i++) {
            list.add("qwerty");
        }
        assertEquals(40, list.capacity());
        list.clear();
        assertEquals(40, list.capacity());
    }
}