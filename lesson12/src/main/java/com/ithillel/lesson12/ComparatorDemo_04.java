package com.ithillel.lesson12;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.*;

public class ComparatorDemo_04 {
    public static void main(String[] args) {

        List<Employee> employees = Arrays.asList(
                new Employee(5L, "John", 22, 15000),
                new Employee(23L, "Anna", 55, 2000),
                new Employee(1L, "Nick", 44, 10000)
        );

        System.out.println(employees);
        Collections.sort(employees);
        System.out.println(employees);

        List<Employee> employees2 = new ArrayList<>() {{
            add(new Employee(5L, "John", 22, 6000));
            add(new Employee(15L, "Bill", 36, 3000));
            add(new Employee(35L, "Kevin", 28, 2000));
        }};

        employees2.sort(new AgeComparator());
        System.out.println(employees2);

        employees2.sort(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.salary - o2.salary;
            }
        });
        for (Employee e : employees2) {
            System.out.print(e.getSalary() + " ");
        }

    }
}

@AllArgsConstructor
@ToString
@Getter
class Employee implements Comparable {
    private long id;
    private String name;
    int age;
    int salary;

    @Override
    public int compareTo(Object o) {
        return this.name.compareTo(((Employee) o).name);
    }
}

class AgeComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        int age1 = ((Employee) o1).getAge();
        int age2 = ((Employee) o2).getAge();

        if (age1 > age2) {
            return 1;
        } else if (age1 < age2) {
            return -1;
        } else {
            return 0;
        }
    }
}