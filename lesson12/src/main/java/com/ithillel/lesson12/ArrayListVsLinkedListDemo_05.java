package com.ithillel.lesson12;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayListVsLinkedListDemo_05 {
    public static void main(String[] args) {

        insertInTheBeginning(new ArrayList());
        insertInTheBeginning(new LinkedList());

        insertInTheEnd(new ArrayList());
        insertInTheEnd(new LinkedList());

    }

    public static void insertInTheBeginning(List list) {
        long start = System.nanoTime();
        for (int i = 0; i < 100_000; i++) {
            list.add(0, new Object());
        }
        long res = (System.nanoTime() - start) / 1000000;
        System.out.println(res + " ms - " + list.getClass().getName() + " - insertInTheBeginning");
    }

    public static void insertInTheEnd(List list) {
        long start = System.nanoTime();
        for (int i = 0; i < 100_000; i++) {
            list.add(new Object());
        }
        long res = (System.nanoTime() - start) / 1000000;
        System.out.println(res + " ms - " + list.getClass().getName() + " - insertInTheEnd");
    }
}
