package com.ithillel.lesson12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CollectionDemo_01 {
    public static void main(String[] args) {

        Collection<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        Collection<Integer> list2 = new ArrayList<>();
        list2.addAll(list);

        System.out.println("list.add(4): " + list.add(4));
        System.out.println("list.size(): " + list.size());
        System.out.println("list.isEmpty(): " + list.isEmpty());
        System.out.println("list.contains(1): " + list.contains(1));

        System.out.println("list2.size(): " + list2.size());
        list2.clear();
        System.out.println("list2.isEmpty(): " + list2.isEmpty());
        System.out.println("list2.size(): " + list2.size());

        Object[] arr = list.toArray();
        System.out.println("Arrays.toString(arr): " + Arrays.toString(arr));

        List<Integer> al = new ArrayList<>();
        al.add(1);
        al.add(2);
        al.add(3);

        System.out.println(al.get(0));
        al.set(0, 10);
        al.add(2, 20);
        System.out.println(al);
        System.out.println(al.indexOf(2));
        System.out.println(al.indexOf(2000));
    }
}
