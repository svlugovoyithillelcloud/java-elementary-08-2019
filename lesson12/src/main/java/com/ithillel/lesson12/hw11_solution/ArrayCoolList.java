package com.ithillel.lesson12.hw11_solution;

import java.util.Arrays;

public class ArrayCoolList<T> implements CoolList<T> {

    private static final int INITIAL_CAPACITY = 10;
    private int size = 0;
    private int capacity = 0;
    private Object[] arrayObjs = {};

    public ArrayCoolList() {
        arrayObjs = new Object[INITIAL_CAPACITY];
        capacity = INITIAL_CAPACITY;
    }

    @Override
    public boolean add(T element) {
        if (size == arrayObjs.length) {
            increaseCapacity();
        }
        arrayObjs[size++] = element;
        return true;
    }

    private void increaseCapacity() {
        int newIncreasedCapacity = arrayObjs.length * 2;
        arrayObjs = Arrays.copyOf(arrayObjs, newIncreasedCapacity);
        capacity = newIncreasedCapacity;
    }

    @Override
    public T get(int index) {
        if (index > arrayObjs.length - 1) {
            throw new ArrayIndexOutOfBoundsException("Wrong index value");
        }
        if (index < 0) {
            throw new IllegalArgumentException("Negative Value");
        }
        return (T) arrayObjs[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return capacity;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void clear() {
        arrayObjs = new Object[capacity];
        size = 0;
    }

    public void display() {
        System.out.print("Displaying list: [ ");
        for (int i = 0; i < size; i++) {
            System.out.print(arrayObjs[i] + " ");
        }
        System.out.print("]");
    }
}
