package com.ithillel.lesson12.hw11_solution;

public interface CoolList<T> {

    boolean add(T element);

    T get(int index);

    int size();

    int capacity();

    boolean isEmpty();

    void clear();
}
