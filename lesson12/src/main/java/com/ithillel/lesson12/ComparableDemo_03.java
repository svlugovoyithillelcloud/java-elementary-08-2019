package com.ithillel.lesson12;

import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ComparableDemo_03 {
    public static void main(String[] args) {

        List<Person> persons = Arrays.asList(
                new Person(5L, "John"),
                new Person(23L, "Bob"),
                new Person(1L, "Nick")

        );

        System.out.println(persons);

        Collections.sort(persons);

        System.out.println(persons);

    }
}

//@AllArgsConstructor
//@ToString
//class Person {
//    private long id;
//    private String name;
//}

@AllArgsConstructor
@ToString
class Person implements Comparable {
    private long id;
    private String name;

    @Override
    public int compareTo(Object o) {
        if (this.id > ((Person)o).id) {
            return 1;
        } else if (this.id < ((Person)o).id) {
            return -1;
        } else {
            return 0;
        }
    }
}
