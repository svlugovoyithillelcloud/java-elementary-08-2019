package com.ithillel.lesson12;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class IteratorDemo_02 {
    public static void main(String[] args) {

        List<Integer> al = new ArrayList<>();

        al.add(12);
        al.add(51);
        al.add(8);

        System.out.println(al);

        Iterator<Integer> iterator = al.iterator();

        while (iterator.hasNext()) {
            Integer tmp = iterator.next();
            System.out.println(tmp);
        }

        ListIterator<Integer> listIterator = al.listIterator();

        while (listIterator.hasNext()) {
            Integer tmp = listIterator.next();
            System.out.println(tmp);
        }

        while (listIterator.hasPrevious()) {
            Integer tmp = listIterator.previous();
            System.out.println(tmp);
        }

        List<Developer> developers = new ArrayList<>() {{
            add(new Developer(11L, "Java"));
            add(new Developer(21L, "Python"));
            add(new Developer(31L, "Kotlin"));
        }};

        Iterator<Developer> developerIterator = developers.iterator();
        while (developerIterator.hasNext()) {
            Developer dev = developerIterator.next();
            if (dev.getId() == 21L) {
                dev.setLang("Java");
            }
            if (dev.getId() == 31L) {
//                developers.remove(dev); //ConcurrentModificationException
                developerIterator.remove();
            }
        }
        System.out.println(developers);

        ListIterator<Developer> developerListIterator = developers.listIterator();
        while (developerListIterator.hasNext()) {
            Developer dev = developerListIterator.next();
            if (dev.getId() == 11L) {
                developerListIterator.add(new Developer(101L, "C++"));
            }
            if (dev.getId() == 21L) {
                developerListIterator.set(new Developer(201L, "Javascript"));
            }
        }
        System.out.println(developers);
    }
}

@AllArgsConstructor
@ToString
@Getter
@Setter
class Developer {
    private long id;
    private String lang;
}
