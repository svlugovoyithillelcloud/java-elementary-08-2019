package com.ithillel.lesson15.homework15;

import java.math.BigDecimal;
import java.time.Month;
import java.util.*;

public class BankAccountUtils {

    // *@gmail.com
    public static List<BankAccount> getAccountsThatHaveGoogleEmail(List<BankAccount> accounts) {
        return new ArrayList<>();
    }

    public static List<String> getAllAccountsEmails(List<BankAccount> accounts) {
        return new ArrayList<>();
    }

    public static Map<String, String> getEmailLastNamePairs(List<BankAccount> accounts) {
        return new HashMap<>();
    }

    public static BigDecimal calculateTotalBalance(List<BankAccount> accounts) {
        return BigDecimal.ZERO;
    }

    public static Optional<BankAccount> findRichestPerson(List<BankAccount> accounts) {
        return null;
    }

    public static Map<Long, BigDecimal> findAccountIdWithMaxBalance(List<BankAccount> accounts) {
        return new HashMap<>();
    }

    public static Map<Month, List<BankAccount>> groupAccountsByItsBirthdaysMonth(List<BankAccount> accounts) {
        return new HashMap<>();
    }

    //{a=6, b=1, c=2, d=2, e=2, i=3, j=1, l=5, n=8, o=4, r=2, s=3, t=2, u=3, v=2, y=1}
    public static Map<Character, Long> getCharacterFrequencyIgnoreCaseInFirstAndLastNames(List<BankAccount> accounts) {
        return new HashMap<>();
    }

    public static Map<Gender, Long> calculateMaleAndFemaleCount(List<BankAccount> accounts) {
        return new HashMap<>();
    }

    public static Map<Month, Long> calculateAccountCountByBirthdayMonth(List<BankAccount> accounts) {
        return new HashMap<>();
    }

    //map where key is true or false, and value is list of male (for key = true), and female accounts (for key = false)
    public static Map<Boolean, List<BankAccount>> partitionMaleAccounts(List<BankAccount> accounts) {
        return new HashMap<>();
    }

    public static Map<Month, BigDecimal> groupTotalBalanceByCreationMonth(List<BankAccount> accounts) {
        return new HashMap<>();
    }

}
