package com.ithillel.lesson15.datetime;

import java.time.*;
import java.time.format.DateTimeFormatter;


public class DTDemo {
    public static void main(String[] args) {

        LocalDate localDate = LocalDate.now();
        System.out.println(localDate.toString());
        System.out.println(localDate.getDayOfWeek().toString());
        System.out.println(localDate.getDayOfMonth());
        System.out.println(localDate.getDayOfYear());
        System.out.println(localDate.isLeapYear());
        System.out.println(localDate.plusDays(12).toString());

        System.out.println("=====================");

        //LocalTime localTime = LocalTime.now();
        LocalTime localTime = LocalTime.of(12, 20, 33);
        System.out.println(localTime.toString());
        System.out.println(localTime.getHour());
        System.out.println(localTime.getMinute());
        System.out.println(localTime.getSecond());
        System.out.println(localTime.MIDNIGHT);
        System.out.println(localTime.NOON);

        System.out.println("=====================");

        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime.toString());
        System.out.println(localDateTime.getDayOfMonth());
        System.out.println(localDateTime.getHour());
        System.out.println(localDateTime.getNano());

        System.out.println("=====================");

        OffsetDateTime offsetDateTime = OffsetDateTime.now();
        System.out.println(offsetDateTime.toString());

        offsetDateTime = OffsetDateTime.now(ZoneId.of("+05:30"));
        System.out.println(offsetDateTime.toString());

        offsetDateTime = OffsetDateTime.now(ZoneId.of("-06:30"));
        System.out.println(offsetDateTime.toString());

        ZonedDateTime zonedDateTime =
                ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        System.out.println(zonedDateTime.toString());

        System.out.println("=====================");

        Instant instant = Instant.now();
        System.out.println(instant.toString());
        System.out.println(instant.toEpochMilli());
        System.out.println(instant.plus(Duration.ofMillis(5000)).toString());
        System.out.println(instant.minus(Duration.ofMillis(5000)).toString());
        System.out.println(instant.minusSeconds(10).toString());

        System.out.println("=====================");

        Duration duration = Duration.ofMillis(5000);
        System.out.println(duration.toString());     //PT5S

        duration = Duration.ofSeconds(60);
        System.out.println(duration.toString());     //PT1M

        duration = Duration.ofMinutes(10);
        System.out.println(duration.toString());     //PT10M

        duration = Duration.ofHours(2);
        System.out.println(duration.toString());     //PT2H

        duration = Duration.between(Instant.now(), Instant.now().plus(Duration.ofMinutes(10)));
        System.out.println(duration.toString());  //PT10M

        System.out.println("=====================");

        Period period = Period.ofDays(6);
        System.out.println(period.toString());    //P6D

        period = Period.ofMonths(6);
        System.out.println(period.toString());    //P6M

        period = Period.between(LocalDate.now(),
                LocalDate.now().plusDays(60));
        System.out.println(period.toString());   //P1M29D

        System.out.println("=====================");

        //day-of-week to represent, from 1 (Monday) to 7 (Sunday)
        System.out.println(DayOfWeek.of(2));                    //TUESDAY

        DayOfWeek day = DayOfWeek.FRIDAY;
        System.out.println(day.getValue());                     //5

        LocalDate ld = LocalDate.now();
        System.out.println(ld.with(DayOfWeek.FRIDAY)); //when was friday in current week?

        System.out.println("=====================");

        DateTimeFormatter formatter =
                DateTimeFormatter
                        .ofPattern("MMM dd, yyyy - HH:mm");

        LocalDateTime parsed = LocalDateTime.parse("Oct 06, 2019 - 07:13", formatter);
        String string = formatter.format(parsed);
        System.out.println(string);     // Oct 06, 2019 - 07:13

    }
}
