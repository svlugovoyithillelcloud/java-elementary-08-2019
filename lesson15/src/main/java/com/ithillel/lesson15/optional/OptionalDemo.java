package com.ithillel.lesson15.optional;

import java.util.Optional;

public class OptionalDemo {
    public static void main(String[] args) {

        //Creating Optional objects
        Optional<Integer> opt1 = Optional.empty();
        Optional<Integer> opt2 = Optional.of(10);
        Optional<Integer> opt3 = Optional.ofNullable(null);
        Optional<Integer> opt4 = Optional.ofNullable(5);

        //Do something If Optional value is present
        Optional<Integer> possible = Optional.of(5);
        possible.ifPresent(System.out::println);

        if (possible.isPresent()) {
            System.out.println(possible.get());
        }

        //Default/absent values and actions

        //Assume this value has returned from a method
        Optional<Integer> intOptional = Optional.empty();

        //Now check optional; if value is present then return it,
        //else create a new Integer object and return it
        Integer intValue = intOptional.orElse(10);
        System.out.println(intValue);

        //OR you can throw an exception as well
//        Integer intValue2 = intOptional.orElseThrow(IllegalStateException::new);


        System.out.println("=====================");

        Optional<String> name = Optional.of("JOHN");
        System.out.println(name.map(String::toLowerCase));  //output Optional[john]

        Optional<String> empty = Optional.empty();
        System.out.println(empty.map(String::toLowerCase)); //output Optional.empty

        System.out.println("=====================");

        Optional<Optional<String>> name1 = Optional.of(Optional.of("JOHN"));
        Optional<String> lowerCaseName = name1.flatMap(o -> o.map(String::toLowerCase));
        System.out.println(lowerCaseName);  //output Optional[john]

        Optional<Optional<String>> empty1 = Optional.of(Optional.empty());
        System.out.println(empty1.flatMap(o -> o.map(String::toLowerCase))); //output Optional.empty
    }
}


