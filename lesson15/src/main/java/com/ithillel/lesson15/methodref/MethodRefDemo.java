package com.ithillel.lesson15.methodref;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MethodRefDemo {
    public static void main(String[] args) {

        //Class::staticMethodName
        List<Integer> integers = Arrays.asList(1, 12, 433, 5);
        Optional<Integer> max = integers.stream()
                .reduce(Math::max);
        max.ifPresent(value -> System.out.println(value));


        //ClassInstance::instanceMethodName
        List<Integer> integers1 = Arrays.asList(1, 12, 433, 5);
        Optional<Integer> max1 = integers1.stream()
                .reduce(Math::max);
        max1.ifPresent( System.out::println );


        //Class::instanceMethodName
        List<String> strings = Arrays
                .asList("b", "a", "d", "c");

        List<String> sorted = strings
                .stream()
                .sorted((s1, s2) -> s1.compareTo(s2))
                .collect(Collectors.toList());
        System.out.println(sorted);

        List<String> sortedAlt = strings
                .stream()
                .sorted( String::compareTo )
                .collect(Collectors.toList());
        System.out.println(sortedAlt);

        //Class::new
        List<Integer> integers2 = IntStream
                .range(1, 100)
                .boxed()
                .collect(Collectors.toCollection( ArrayList::new ));
        System.out.println(integers2);

    }
}
