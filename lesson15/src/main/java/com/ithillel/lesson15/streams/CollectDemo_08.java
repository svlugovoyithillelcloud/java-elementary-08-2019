package com.ithillel.lesson15.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.ithillel.lesson15.streams.PersonGenerator.prettyPrintList;

public class CollectDemo_08 {
    public static void main(String[] args) {

        List<Person> people = PersonGenerator.generate12Person();
        prettyPrintList(people);


        //3 apple, 2 banana, others 1
        List<String> items =
                Arrays.asList("apple", "apple", "banana",
                        "apple", "orange", "banana", "papaya");

        Map<String, Long> result = items.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(result);


        String joining = people.stream()
                .map(p -> p.getFirstName())
                .collect(Collectors.joining(", "));
        System.out.println(joining);


        Map<Person.Sex, List<String>> byGender = people.stream()
                .collect(
                        Collectors.groupingBy(
                                Person::getGender,
                                Collectors.mapping(Person::getLastName, Collectors.toList())
                        )
                );
        System.out.println(byGender);


        Map<Person.Sex, String> byGenderJoining = people.stream()
                .collect(
                        Collectors.groupingBy(
                                Person::getGender,
                                Collectors.mapping(Person::getLastName, Collectors.joining("+"))
                        )
                );
        System.out.println(byGenderJoining);
    }
}
