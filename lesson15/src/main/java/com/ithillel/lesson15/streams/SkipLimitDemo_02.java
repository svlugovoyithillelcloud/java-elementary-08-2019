package com.ithillel.lesson15.streams;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ithillel.lesson15.streams.PersonGenerator.prettyPrintList;

public class SkipLimitDemo_02 {
    public static void main(String[] args) {

        List<Person> people = PersonGenerator.generate12Person();
        prettyPrintList(people);

        List<Person> p1 = people.stream()
                .skip(5)
                .collect(Collectors.toList());
        prettyPrintList(p1);

        List<Person> p2 = people.stream()
                .limit(3)
                .collect(Collectors.toList());
        prettyPrintList(p2);

        List<Person> p3 = people.stream()
                .skip(5)
                .limit(3)
                .collect(Collectors.toList());
        prettyPrintList(p3);


    }

}
