package com.ithillel.lesson15.streams;

import java.util.List;
import java.util.Optional;

import static com.ithillel.lesson15.streams.PersonGenerator.prettyPrintList;

public class ReduceDemo_07 {
    public static void main(String[] args) {

        List<Person> people = PersonGenerator.generate12Person();
        prettyPrintList(people);

        Optional<String> reduced = people.stream()
                        .map(p -> p.getFirstName())
                        .reduce((fn1, fn2) -> fn1 + ";" + fn2);
        reduced.ifPresent(System.out::println);

        int sum = people.stream()
                .mapToInt(p -> p.getAge())
                .reduce(0, (a1, a2) -> a1 + a2);

        System.out.println(sum);

    }
}
