package com.ithillel.lesson15.streams;

import java.util.List;
import java.util.stream.Collectors;

import static com.ithillel.lesson15.streams.PersonGenerator.prettyPrintList;

public class FlatMapDemo_05 {
    public static void main(String[] args) {

        List<Person> people = PersonGenerator.generate12Person();
        prettyPrintList(people);

        List<List<String>> phones = people.stream()
                .map(p -> p.getPhones())
                .collect(Collectors.toList());
        System.out.println(phones);

        List<String> phonesflat = people.stream()
                .map(p -> p.getPhones())
                .flatMap(l -> l.stream())
                .collect(Collectors.toList());
        System.out.println(phonesflat);
    }
}
