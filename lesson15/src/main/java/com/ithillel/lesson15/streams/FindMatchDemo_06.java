package com.ithillel.lesson15.streams;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.ithillel.lesson15.streams.PersonGenerator.prettyPrintList;

public class FindMatchDemo_06 {
    public static void main(String[] args) {

        List<Person> people = PersonGenerator.generate12Person();
        prettyPrintList(people);

        boolean anyFirstNameStartsWithA =
                people.stream()
                        .map(p -> p.getFirstName())
                        .anyMatch((s) -> s.startsWith("A"));

        System.out.println(anyFirstNameStartsWithA);      // true

        boolean allFirstNameStartsWithA =
                people.stream()
                        .map(p -> p.getFirstName())
                        .allMatch((s) -> s.startsWith("A"));

        System.out.println(allFirstNameStartsWithA);      // false

        boolean noneFirstNameStartsWithZ =
                people.stream()
                        .map(p -> p.getFirstName())
                        .noneMatch((s) -> s.startsWith("Z"));

        System.out.println(noneFirstNameStartsWithZ);      // true


        Optional<Person> first = people.stream()
                .findFirst();
        System.out.println(first);

        Optional<Person> any = people.stream()
                .findAny();
        System.out.println(any.get());

        //sequential stream
        Stream.of("one", "two", "three", "four")
                .findAny()
                .ifPresent(System.out::println);

        //parallel stream
        Stream.of("one", "two", "three", "four")
                .parallel() // In non-parallel streams, findAny() will return the first element in most of the cases but this behavior is not gauranteed.
                .findAny()
                .ifPresent(System.out::println);
    }
}
