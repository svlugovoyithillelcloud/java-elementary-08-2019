package com.ithillel.lesson15.streams;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ithillel.lesson15.streams.PersonGenerator.prettyPrintList;

public class FilterDistinctDemo_01 {
    public static void main(String[] args) {

        List<Person> people = PersonGenerator.generate12Person();
        prettyPrintList(people);

        List<Person> young = people.stream()
                .filter(p -> p.getAge() < 18)
                .collect(Collectors.toList());
        prettyPrintList(young);

        List<Person> youngdist = people.stream()
                .filter(p -> p.getAge() < 18)
                .distinct()
                .collect(Collectors.toList());
        prettyPrintList(youngdist);

        Set<Person> youngset = people.stream()
                .filter(p -> p.getAge() < 18)
                .collect(Collectors.toSet());
        System.out.println(youngset);

    }

}
