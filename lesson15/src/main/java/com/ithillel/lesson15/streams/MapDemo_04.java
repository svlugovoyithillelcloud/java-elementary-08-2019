package com.ithillel.lesson15.streams;

import java.util.List;
import java.util.stream.Collectors;

import static com.ithillel.lesson15.streams.PersonGenerator.prettyPrintList;

public class MapDemo_04 {
    public static void main(String[] args) {

        List<Person> people = PersonGenerator.generate12Person();
        prettyPrintList(people);

        List<String> names = people.stream()
                .map(p -> p.getFirstName())
                .collect(Collectors.toList());
        System.out.println(names);

        List<String> cities = people.stream()
                .map(p -> p.getCity())
                .distinct()
                .collect(Collectors.toList());
        System.out.println(cities);

        List<String> lastUpper = people.stream()
                .map(p -> p.getLastName())
                .map(s -> s.toUpperCase())
                .map(s -> "=== " + s + " ===")
                .collect(Collectors.toList());
        System.out.println(lastUpper);

        List<Integer> ages = people.stream()
                .map(p -> p.getAge())
                .collect(Collectors.toList());
        System.out.println(ages);

        Double aver = people.stream()
                .mapToInt(p -> p.getAge())
                .average().getAsDouble();
        System.out.println(aver);
    }

}
