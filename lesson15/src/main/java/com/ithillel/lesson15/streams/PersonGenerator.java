package com.ithillel.lesson15.streams;

import lombok.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonGenerator {

    public static List<Person> generate12Person(){
        return new ArrayList<>(){{
            add(new Person(1L, "John", "Smith", Arrays.asList("0973005040", "0974006666"), 30, "NY", Person.Sex.MALE));
            add(new Person(2L, "Anne", "Petrova", Arrays.asList("0373005040", "0974006666"), 20, "NY", Person.Sex.FEMALE));
            add(new Person(3L, "Ivan", "Sidorov", Arrays.asList("0963005040", "0674006666"), 5, "Kyiv", Person.Sex.MALE));
            add(new Person(4L, "Nina", "Rock", Arrays.asList("0973005040", "0974006666"), 50, "Kyiv", Person.Sex.FEMALE));
            add(new Person(5L, "Nick", "Rock", Arrays.asList("0973005040", "0974006666"), 22, "NY", Person.Sex.MALE));
            add(new Person(5L, "Nick", "Rock", Arrays.asList("0973005040", "0974006666"), 22, "NY", Person.Sex.MALE));
            add(new Person(6L, "Oksana", "Last", Arrays.asList("0973005040", "0974006666"), 33, "NY", Person.Sex.FEMALE));
            add(new Person(7L, "Anton", "Good", Arrays.asList("0573005040", "0974006666"), 55, "Kyiv", Person.Sex.MALE));
            add(new Person(8L, "Alina", "Smith", Arrays.asList("0573005040", "0974006666"), 25, "NY", Person.Sex.FEMALE));
            add(new Person(9L, "Serhii", "Serhiiv", Arrays.asList("0973005040", "0974006666"), 35, "Warsaw", Person.Sex.MALE));
            add(new Person(10L, "Olha", "Olhina", Arrays.asList("0973005040", "0574006666"), 15, "NY", Person.Sex.FEMALE));
            add(new Person(10L, "Olha", "Olhina", Arrays.asList("0973005040", "0574006666"), 15, "NY", Person.Sex.FEMALE));
        }};
    }

    public static void prettyPrintList(List<Person> people) {
        System.out.println("People list size: " + people.size());
        people.forEach((p) -> System.out.println(p));
        System.out.println("===================");
    }
}

@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
class Person {
    private Long id;
    private String firstName;
    private String lastName;
    private List<String> phones;
    private int age;
    private String city;
    private Sex gender;

    public enum Sex {MALE, FEMALE}
}
