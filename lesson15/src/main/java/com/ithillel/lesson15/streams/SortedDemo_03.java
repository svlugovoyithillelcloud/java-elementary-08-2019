package com.ithillel.lesson15.streams;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.ithillel.lesson15.streams.PersonGenerator.prettyPrintList;

public class SortedDemo_03 {
    public static void main(String[] args) {

        List<Person> people = PersonGenerator.generate12Person();
        prettyPrintList(people);

//        List<Person> p1 = people.stream()
//                .sorted()
//                .collect(Collectors.toList());
//        prettyPrintList(p1);

        List<Person> p2 = people.stream()
                .sorted((person1, person2) -> person1.getFirstName().compareTo(person2.getFirstName()))
                .collect(Collectors.toList());
        prettyPrintList(p2);

        Comparator<Person> byCity = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getCity().compareTo(o2.getCity());
            }
        };

        List<Person> p3 = people.stream()
                .sorted(byCity)
                .collect(Collectors.toList());
        prettyPrintList(p3);
    }
}
