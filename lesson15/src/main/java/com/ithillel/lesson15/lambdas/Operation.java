package com.ithillel.lesson15.lambdas;

public interface Operation {
    int calc(int a, int b);
}
