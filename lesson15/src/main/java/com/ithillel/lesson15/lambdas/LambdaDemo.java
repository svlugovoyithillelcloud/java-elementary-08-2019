package com.ithillel.lesson15.lambdas;

public class LambdaDemo {

    public void doSmth() {
        Operation operation;
        operation = new Operation() {
            @Override
            public int calc(int a, int b) {
                return 0;
            }
        };

//        Operation operation = (a, b) -> 0;
    }
}

//What Lambda Expressions are compiled to?
//https://www.logicbig.com/tutorials/core-java-tutorial/java-8-enhancements/java-lambda-functional-aspect.html
//https://habr.com/ru/post/111456/