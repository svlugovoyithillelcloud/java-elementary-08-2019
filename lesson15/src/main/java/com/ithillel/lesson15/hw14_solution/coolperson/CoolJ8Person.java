package com.ithillel.lesson15.hw14_solution.coolperson;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class CoolJ8Person {
    private Long id;
    private String name;
    private String city;
    private int age;
}
