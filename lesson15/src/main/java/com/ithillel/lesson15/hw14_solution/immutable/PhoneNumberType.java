package com.ithillel.lesson15.hw14_solution.immutable;

public enum PhoneNumberType {
    PERSONAL, WORK
}
