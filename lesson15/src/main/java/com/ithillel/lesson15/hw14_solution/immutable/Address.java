package com.ithillel.lesson15.hw14_solution.immutable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Address {
    private Long id;
    private String country;
    private String city;

}
