package com.ithillel.lesson15.hw14_solution.immutable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@ToString
public final class ImmutablePerson {
    private Long id;
    private String name;
    private Date date;
    private Address address;
    private List<PhoneNumber> phones;
}
