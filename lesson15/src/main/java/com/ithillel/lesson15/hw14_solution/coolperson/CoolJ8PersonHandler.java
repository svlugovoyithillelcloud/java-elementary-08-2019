package com.ithillel.lesson15.hw14_solution.coolperson;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CoolJ8PersonHandler {

    //java 8
    //должно возвращаться что-то типа
    //{Kyiv=John;Anna;Nick, New York=James;Igor, Warsaw=Olha}
    public static HashMap<String, String> groupPersonNamesByCity(List<CoolJ8Person> persons){
        return (HashMap<String, String>) persons.stream()
                .collect(
                        Collectors.groupingBy(
                                CoolJ8Person::getCity,
                                Collectors.mapping(CoolJ8Person::getName, Collectors.joining(";"))
                        )
                );
    }

    //Список только имен персон страрше 18 лет из переданношо города
    public static List<String> filterPersonNamesByCityAndAgeGreater18(List<CoolJ8Person> persons, String city){
        return persons.stream()
                .filter(p -> p.getAge() > 18)
                .filter(p -> p.getCity().equals(city))
                .map(p -> p.getName())
                .collect(Collectors.toList());
    }


}


