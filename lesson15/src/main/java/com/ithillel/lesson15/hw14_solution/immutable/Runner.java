package com.ithillel.lesson15.hw14_solution.immutable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Runner {
    public static void main(String[] args) {

        Date date = new Date();

        List<PhoneNumber> phones = new ArrayList<>() {{
            add(new PhoneNumber(1L, "12345", PhoneNumberType.PERSONAL));
            add(new PhoneNumber(2L, "3456787", PhoneNumberType.WORK));
        }};

        Address address = new Address(1L, "Ukraine", "Kyiv");

        ImmutablePerson person = new ImmutablePerson(1L, "John", date, address, phones);
        System.out.println(person);


        // todo: show possible cases

//        person.setName("Alex");

//        date.setTime(22222222L);
//        address.setCity("Lviv");
//        phones.add(new PhoneNumber(3L, "8888888", PhoneNumberType.WORK));


//        person.getDate().setTime(123456L);
//        person.getAddress().setCity("Warsaw");
//        person.getPhones().add(new PhoneNumber(3L, "8888888", PhoneNumberType.WORK));

//        phones.get(0).setId(55L);

        System.out.println(person);


    }
}
