package com.ithillel.lesson15.java9_12;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Java9_12Demo {
    public static void main(String[] args) {

        Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .takeWhile(n -> n < 5)
                .forEach(System.out::println); // 1 2 3 4

        Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .dropWhile(n -> n < 6)
                .forEach(System.out::println); // 6 7 8 9 10

        IntStream.iterate(0, n -> n + 1)
                .takeWhile(n -> n < 100)
                .forEach(System.out::println);

    }
}
