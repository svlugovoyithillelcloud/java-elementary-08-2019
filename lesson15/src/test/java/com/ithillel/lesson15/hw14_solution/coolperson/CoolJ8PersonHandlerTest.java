package com.ithillel.lesson15.hw14_solution.coolperson;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CoolJ8PersonHandlerTest {

    private List<CoolJ8Person> list;

    @BeforeEach
    void setUp() {
        list = new ArrayList<>(){{
            add(new CoolJ8Person(1L, "John", "Kyiv", 28));
            add(new CoolJ8Person(2L, "Anne", "Dnepr", 33));
            add(new CoolJ8Person(3L, "Kevin", "Kyiv", 5));
            add(new CoolJ8Person(4L, "Serhii", "Dnepr", 44));
            add(new CoolJ8Person(5L, "Olha", "Kyiv", 10));
        }};
    }

    @Test
    void groupPersonNamesByCity() {
        HashMap<String, String> map = CoolJ8PersonHandler.groupPersonNamesByCity(list);
        System.out.println(map);
    }

    @Test
    void filterPersonNamesByCityAndAgeGreater18() {
        List<String> list = CoolJ8PersonHandler.filterPersonNamesByCityAndAgeGreater18(this.list, "Kyiv");
        System.out.println(list);
    }
}