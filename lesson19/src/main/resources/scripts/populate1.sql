CREATE SCHEMA `cinema_db_my` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE cinema_db_my;

CREATE TABLE actors (

  id BIGINT NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  birthday DATE NOT NULL,
  gender VARCHAR(6) NOT NULL,
  instagram VARCHAR(255),

  CONSTRAINT actors_PK PRIMARY KEY (id),
  CONSTRAINT actors_instagram_AK UNIQUE (instagram)

);


CREATE TABLE directors (

  id BIGINT NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  birthday DATE NOT NULL,
  gender VARCHAR(6) NOT NULL,
  instagram VARCHAR(255),

  CONSTRAINT directors_PK PRIMARY KEY (id),
  CONSTRAINT directors_instagram_AK UNIQUE (instagram)

);


CREATE TABLE movies (

  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  year_of_creation SMALLINT NOT NULL,
  genre VARCHAR(255) NOT NULL,
  directors_id BIGINT NOT NULL,

  CONSTRAINT movies_PK PRIMARY KEY (id),
  CONSTRAINT movies_directors_FK FOREIGN KEY (directors_id) REFERENCES directors (id)

);


CREATE TABLE actors_movies (

  actors_id BIGINT,
  movies_id BIGINT,

  CONSTRAINT actors_movies_PK PRIMARY KEY (actors_id, movies_id),
  CONSTRAINT actors_movies_actors_FK FOREIGN KEY (actors_id) REFERENCES actors (id) ON DELETE CASCADE,
  CONSTRAINT actors_movies_movies_FK FOREIGN KEY (movies_id) REFERENCES movies (id)

);




-- Kill Bill: Vol. 1

INSERT INTO actors (first_name, last_name, birthday, gender, instagram) VALUES ('Uma', 'Thurman', '1970-04-29', 'FEMALE', 'instagram.com/umma');
INSERT INTO directors (first_name, last_name, birthday, gender, instagram) VALUES ('Quentin', 'Tarantino', '1963-03-27', 'MALE', 'instagram.com/qwerty');
INSERT INTO movies (name, year_of_creation, genre, directors_id) VALUES ('Kill Bill: Vol. 1', 2003, 'Action', (select id from directors where last_name='Tarantino'));
INSERT INTO actors_movies (actors_id, movies_id) VALUES ((select id from actors where last_name='Thurman'), (select id from movies where name='Kill Bill: Vol. 1'));


