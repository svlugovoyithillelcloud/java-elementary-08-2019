package com.ithillel.lesson19.hw18_solution;

import java.util.Arrays;
import java.util.function.Function;

public class HW18Utils {

    public static void main(String[] args) {

        String[] arr1 = new String[]{"a", "bb", "a", null, "15"};

        String[] newArr1 = filter(arr1, s -> s != null);
        System.out.println(Arrays.toString(newArr1)); //[a, bb, a, 15]


        String[] arr2 = new String[]{"a", "bb", "a", "15", "abc", "xyz"};

        String[] newArr2 = filter(arr2, s -> s.startsWith("a"));
        System.out.println(Arrays.toString(newArr2)); //[a, a, abc]


    }


    public static <T> T[] filter(T[] array, Function<? super T, Boolean> filter) {
        throw new RuntimeException("Please implement me!");
    }


}
