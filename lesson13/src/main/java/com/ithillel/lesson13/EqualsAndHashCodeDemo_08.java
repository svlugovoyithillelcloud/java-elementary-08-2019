package com.ithillel.lesson13;

import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class EqualsAndHashCodeDemo_08 {
    public static void main(String[] args) {

        Map<Employee, Number> employees = new HashMap<>();

        Employee john = new Employee(1, "John", 25, "IT");
        employees.put(john, 10000);

        System.out.println(john.hashCode());
        System.out.println(employees.get(john));

        Employee copyJohn = new Employee(1, "John", 25, "IT");
        System.out.println(copyJohn.hashCode());
        System.out.println(employees.get(copyJohn));

//        copyJohn.setAge(26);
//        System.out.println(copyJohn.hashCode());
//        System.out.println(employees.get(copyJohn));

//        Map<String[], Number> map = new HashMap<>();
//        map.put(new String[]{"hello"}, 1000);
//
//        System.out.println(map.get(new String[]{"hello"}));
    }
}

@AllArgsConstructor
@ToString
class Employee {
    private int id;
    private String name;
    private int age;
    private String department;
}

//@AllArgsConstructor
//@ToString
//class Employee {
//    private int id;
//    private String name;
//    private int age;
//    private String department;
//
//    @Override
//    public int hashCode() {
//        return 42;
//    }
//}

//@AllArgsConstructor
//@ToString
//class Employee {
//    private int id;
//    private String name;
//    private int age;
//    private String department;
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Employee employee = (Employee) o;
//
//        return id == employee.id;
//    }
//
//    @Override
//    public int hashCode() {
//        return 42;
//    }
//}

//@AllArgsConstructor
//@ToString
//class Employee {
//    private int id;
//    private String name;
//    private int age;
//    private String department;
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Employee employee = (Employee) o;
//
//        return id == employee.id;
//    }
//
//    @Override
//    public int hashCode() {
//        return ThreadLocalRandom.current().nextInt(10, 1000);
//    }
//}

//@AllArgsConstructor
//@Setter
//@ToString
//class Employee {
//    private int id;
//    private String name;
//    private int age;
//    private String department;
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Employee employee = (Employee) o;
//
//        if (id != employee.id) return false;
//        if (age != employee.age) return false;
//        if (!name.equals(employee.name)) return false;
//        return department.equals(employee.department);
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id;
//        result = 31 * result + name.hashCode();
//        result = 31 * result + age;
//        result = 31 * result + department.hashCode();
//        return result;
//    }
//}