package com.ithillel.lesson13;

import java.util.*;

public class HashMapDemo_06 {
    public static void main(String[] args) {

        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(1, "one");
        hm.put(2, "two");
        hm.put(3, "three");

        System.out.println(hm);

        Collection<String> values = hm.values();
        System.out.println(values);

        Set<Integer> keys = hm.keySet();
        System.out.println(keys);

        Set<Map.Entry<Integer, String>> entries = hm.entrySet();
        System.out.println(entries);

        for (Map.Entry<Integer, String> entry : entries) {
            System.out.println(entry.getKey() + " ===== " + entry.getValue());
        }

        hm.put(null, "hello");
        hm.put(2, "two-NEW");
        System.out.println(hm);
    }
}
