package com.ithillel.lesson13;

import java.util.*;

public class CollectionsDemo_09 {
    public static void main(String[] args) {

        Set orders;
        orders = (Set) Collections.checkedSet(new HashSet<>(), String.class);
        orders.add("hello");
//        orders.add(1); // runtime error

        List<String> strings = Arrays.asList("hello", "world", "!!!");
        List<String> copiedStrings = Arrays.asList("1", "2", "3", "4", "5", "5");
        System.out.println(copiedStrings);

        Collections.copy(copiedStrings, strings);
        System.out.println(copiedStrings);

        List<String> empty = Collections.emptyList();
        System.out.println(empty.getClass().getName());

        Collections.reverse(copiedStrings);
        System.out.println(copiedStrings);

        Collections.shuffle(copiedStrings);
        System.out.println(copiedStrings);

        System.out.println(Collections.frequency(copiedStrings, "5"));

        Collections.sort(copiedStrings, Comparator.comparingInt(String::length));
        System.out.println(copiedStrings);

        Collections.swap(copiedStrings, 0, copiedStrings.size() - 1);
        System.out.println(copiedStrings);

        List<String> unmodifiableList = Collections.unmodifiableList(strings);
//        unmodifiableList.add("New");
        System.out.println(unmodifiableList.get(0));
//        unmodifiableList.set(0, "new-hello");
    }
}
