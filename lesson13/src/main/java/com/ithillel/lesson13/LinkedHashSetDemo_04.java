package com.ithillel.lesson13;

import java.util.*;

public class LinkedHashSetDemo_04 {
    public static void main(String[] args) {

        Set<String> nums = new LinkedHashSet<>();
        nums.add("1");
        nums.add("222");
        nums.add("3");
        nums.add(null);
        nums.add("3");
        nums.add("4");
        nums.add("5");
        nums.add(null);

        System.out.println(nums);
        System.out.println(nums.size());

    }
}
