package com.ithillel.lesson13;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HashSetDemo_03 {
    public static void main(String[] args) {

        Set<String> cars = new HashSet<>();
        cars.add("Opel");
        cars.add("BMW");
        cars.add("Mercedes");
        cars.add("Opel");
        cars.add("Fiat");
        cars.add(null);
        cars.add("Mazda");

        System.out.println(cars);
        System.out.println(cars.size());
        System.out.println(cars.contains("Mercedes"));
        System.out.println(cars.containsAll(Arrays.asList("Mercedes", "Mazda")));

        List<String> list = Arrays.asList("One", "Two", "One", "Two", "One");
        Set<String> hs = new HashSet<>(list);
        System.out.println(hs.size());
    }
}
