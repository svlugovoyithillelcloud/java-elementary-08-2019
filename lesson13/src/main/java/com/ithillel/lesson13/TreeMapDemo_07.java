package com.ithillel.lesson13;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.TreeMap;

public class TreeMapDemo_07 {
    public static void main(String[] args) {

        TreeMap<Integer, String> tm = new TreeMap<>();

//        tm.put(null, "hello");

        tm.put(1, "1");
        tm.put(22, "22");
        tm.put(10, "10");
        tm.put(22, "NEW");

        System.out.println(tm);

        TreeMap<Dog, String> dogs = new TreeMap<>();
//        dogs.put(new Dog("Sharik"), "Hello world");

        TreeMap<Cat, String> cats = new TreeMap<>();
        cats.put(new Cat("Murka"), "Hello world");
        cats.put(new Cat("Aura"), "Hello world");
        cats.put(new Cat("Sem"), "Hello world");

        System.out.println(cats);
    }
}

@AllArgsConstructor
@ToString
class Dog {
    private String name;
}

@AllArgsConstructor
@Getter
@ToString
class Cat implements Comparable{
    private String name;

    @Override
    public int compareTo(Object o) {
        return this.name.compareTo(((Cat) o).getName());
    }
}