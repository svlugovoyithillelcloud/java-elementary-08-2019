package com.ithillel.lesson13.hw12_solution;

import java.util.Comparator;

public class CoolComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        CoolPerson cp1 = (CoolPerson) o1;
        CoolPerson cp2 = (CoolPerson) o2;

        if (cp1.getCity().compareTo(cp2.getCity()) > 0) {
            return 1;
        } else if (cp1.getCity().compareTo(cp2.getCity()) < 0) {
            return -1;
        } else {

            if (cp1.getAge() < cp2.getAge()) {
                return 1;
            } else if (cp1.getAge() > cp2.getAge()) {
                return -1;
            } else {
                return cp1.getName().compareTo(cp2.getName());
            }
        }
    }


    //1. Сортируем по городу
    //2. Если город совпал - сортируем по возрасту в обратном порядке (старшие вначале)
    //3. Если возраст тоже совпал сортируем по имени в алфавитном порядке

}
