package com.ithillel.lesson13.hw12_solution;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<T> implements Iterator<T> {

    private T[] array;
    private int index = 0;

    public ArrayIterator(T[] array) {
        this.array = array;
    }

    @Override
    public boolean hasNext() {
        return index < array.length;
    }

    @Override
    public T next() {
        if(!hasNext())
            throw new NoSuchElementException();
        return array[index++];
    }

    public static void main(String[] args) {

        String[] strings = {"1", "2", "3", "4", "hello"};

        ArrayIterator<String> arrayIterator = new ArrayIterator<>(strings);

        while (arrayIterator.hasNext()){
            System.out.println(arrayIterator.next());
        }
    }
}
