package com.ithillel.lesson13.hw12_solution;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CoolPerson {
    private Long id;
    private String name;
    private String city;
    private int age;
}

