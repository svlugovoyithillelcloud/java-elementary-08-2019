package com.ithillel.lesson13;

import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.*;

public class TreeSetDemo_05 {
    public static void main(String[] args) {

        NavigableSet<Integer> nums = new TreeSet<>();
        nums.add(7);
        nums.add(2);
        nums.add(14);
        nums.add(-99);
        System.out.println(nums);

        NavigableSet<Car> cars = new TreeSet<>();
        // Добавляем объект Car в коллекцию
        cars.add(new Car(50000, "BMW"));
        cars.add(new Car(15000, "Opel"));
        cars.add(new Car(30000, "Jeep"));
        System.out.println(cars);

//        cars.add(null); //NullPointerException

        boolean res = cars.add(new Car(30000, "Jeep111"));
        System.out.println(res);
        System.out.println(cars);
    }
}

@AllArgsConstructor
@ToString
class Car implements Comparable<Car> {
    private int price;
    private String name;

    @Override
    public int compareTo(Car car) {
        return this.price - car.price;
    }
}