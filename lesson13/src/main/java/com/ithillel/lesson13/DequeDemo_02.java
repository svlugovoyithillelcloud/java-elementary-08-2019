package com.ithillel.lesson13;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayDeque;

public class DequeDemo_02 {
    public static void main(String[] args) {

        ArrayDeque<String> countries = new ArrayDeque<>();
        countries.add("Ukraine");
        countries.add("Poland");
        countries.add("France");

        countries.push("Spain");

        //without remove
        System.out.println(countries.getFirst());
        System.out.println(countries.getLast());
        System.out.println(countries.size());

        while (countries.peek() != null) {
            System.out.println("pop - " + countries.pop());
        }
        System.out.println("Queue size after 4 pop(): " + countries.size());

        ArrayDeque<Person> people = new ArrayDeque<>();
        people.add(new Person("David"));
        people.addFirst(new Person("Anne"));
        people.addLast(new Person("John"));

        for (Person p : people) {
            System.out.println(p.getName());
        }

        System.out.println(people.getFirst().getName());
    }
}

@AllArgsConstructor
@Getter
class Person {
    private String name;
}