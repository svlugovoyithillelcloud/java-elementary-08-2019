package com.ithillel.lesson13;

import java.util.*;

public class QueueDemo_01 {
    public static void main(String[] args) throws InterruptedException {

        Comparator<Integer> fromGreatToLess = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        };

        Queue<Integer> pq = new PriorityQueue<>();
//        Queue<Integer> pq = new PriorityQueue<>(fromGreatToLess);

        Queue<Integer> ll = new LinkedList<>();

        ll.offer(1);
        ll.offer(99);
        ll.offer(55);
        System.out.println("LinkedList queue: " + ll);

        ll.offer(10);
        ll.offer(666);
        System.out.println("LinkedList queue: " + ll);

        pq.offer(1);
        System.out.println("PriorityQueue queue: " + pq);
        pq.offer(99);
        System.out.println("PriorityQueue queue: " + pq);
        pq.offer(56);
        System.out.println("PriorityQueue queue: " + pq);

        pq.offer(10);
        System.out.println("PriorityQueue queue: " + pq);
        pq.offer(666);
        System.out.println("PriorityQueue queue: " + pq);

        System.out.println(pq.poll());
        System.out.println(pq.poll());
        System.out.println(pq.poll());
        System.out.println(pq.poll());
        System.out.println(pq);

        System.out.println(ll.poll());
        System.out.println(ll.poll());
        System.out.println(ll.poll());
        System.out.println(ll.poll());
        System.out.println(ll);
    }
}
