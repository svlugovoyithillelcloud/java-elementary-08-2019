package com.ithillel.lesson13.hw12_solution;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CoolComparatorTest {

    CoolComparator coolComparator = new CoolComparator();

    @Test
    void shouldSortCorrect() {
        List<CoolPerson> coolPeople = new ArrayList<>(){{
            add(new CoolPerson(122L, "Richard", "NY", 25));
            add(new CoolPerson(1L, "Anna", "Kyiv", 25));
            add(new CoolPerson(15L, "Denis", "Astana", 26));
            add(new CoolPerson(12L, "Boris", "Kyiv", 25));
            add(new CoolPerson(16L, "Richard", "NY", 75));
            add(new CoolPerson(31L, "Anna", "Kyiv", 35));
            add(new CoolPerson(131L, "Anna", "Kyiv", 55));
        }};

        List<CoolPerson> sorted = new ArrayList<>(){{
            add(new CoolPerson(15L, "Denis", "Astana", 26));
            add(new CoolPerson(131L, "Anna", "Kyiv", 55));
            add(new CoolPerson(31L, "Anna", "Kyiv", 35));
            add(new CoolPerson(1L, "Anna", "Kyiv", 25));
            add(new CoolPerson(12L, "Boris", "Kyiv", 25));
            add(new CoolPerson(16L, "Richard", "NY", 75));
            add(new CoolPerson(122L, "Richard", "NY", 25));
        }};

        assertNotEquals(sorted, coolPeople);

        coolPeople.sort(coolComparator);

        assertEquals(sorted, coolPeople);
    }
}