package com.ithillel.lesson16.hw15_solution;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class BankAccountUtilsTest {

    List<BankAccount> list;

    @BeforeEach
    void setUp() {
        list = new ArrayList<>(){{
            add(new BankAccount(1L, "Ivan", "Ivanov", "ivan12@gmail.com", LocalDate.of(1980, 10, 10),
                    Gender.MALE, LocalDateTime.of(2019, 8, 10, 14, 55), BigDecimal.valueOf(10000)));
            add(null);
            add(new BankAccount(22L, "Petr", "Petrov", "petr12@gmail.com", LocalDate.of(1990, 11, 15),
                    Gender.MALE, LocalDateTime.of(2018, 6, 24, 15, 25), BigDecimal.valueOf(80000)));
            add(new BankAccount(33L, "Olha", "Petrova", "olha25@gmail.com", LocalDate.of(1995, 7, 15),
                    Gender.FEMALE, LocalDateTime.of(2017, 6, 28, 11, 15), BigDecimal.valueOf(34000)));
            add(new BankAccount(44L, "Olha", null, null, null,
                    Gender.FEMALE, LocalDateTime.of(2019, 6, 28, 11, 15), BigDecimal.valueOf(13000)));
        }};
    }

    @Test
    void getAccountsThatHaveGoogleEmail() {
        List<BankAccount> accounts = BankAccountUtils.getAccountsThatHaveGoogleEmail(list);
        System.out.println(accounts);
    }

    @Test
    void getAllAccountsEmails() {
        List<String> accounts = BankAccountUtils.getAllAccountsEmails(list);
        System.out.println(accounts);
    }

    @Test
    void getEmailLastNamePairs() {
        Map<String, String> accounts = BankAccountUtils.getEmailLastNamePairs(list);
        System.out.println(accounts);
    }

    @Test
    void calculateTotalBalance() {
        BigDecimal sum = BankAccountUtils.calculateTotalBalance(list);
        System.out.println(sum);
    }

    @Test
    void findRichestPerson() {
        Optional<BankAccount> richestPerson = BankAccountUtils.findRichestPerson(list);
        System.out.println(richestPerson);
    }

    @Test
    void findAccountIdWithMaxBalance() {
        Map<Long, BigDecimal> accounts = BankAccountUtils.findAccountIdWithMaxBalance(list);
        System.out.println(accounts);
    }

    @Test
    void groupAccountsByItsBirthdaysMonth() {
        Map<Month, List<BankAccount>> accounts = BankAccountUtils.groupAccountsByItsBirthdaysMonth(list);
        System.out.println(accounts);
    }

    @Test
    void calculateMaleAndFemaleCount() {
        Map<Gender, Long> stats = BankAccountUtils.calculateMaleAndFemaleCount(list);
        System.out.println(stats);
    }

    @Test
    void calculateAccountCountByBirthdayMonth() {
        Map<Month, Long> stats = BankAccountUtils.calculateAccountCountByBirthdayMonth(list);
        System.out.println(stats);
    }

    @Test
    void partitionMaleAccounts() {
        Map<Boolean, List<BankAccount>> stats = BankAccountUtils.partitionMaleAccounts(list);
        System.out.println(stats);
    }

    @Test
    void groupTotalBalanceByCreationMonth() {
        Map<Month, BigDecimal> stats = BankAccountUtils.groupTotalBalanceByCreationMonth(list);
        System.out.println(stats);
    }

    @Test
    void getCharacterFrequencyIgnoreCaseInFirstAndLastNames() {
        Map<Character, Long> stats = BankAccountUtils.getCharacterFrequencyIgnoreCaseInFirstAndLastNames(list);
        System.out.println(stats);
    }
}