package com.ithillel.lesson16.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class ReflectionMethodsDemo_03 {

    private static final class Dog {
        public String name = "Sharik";
        private int age = 7;
        String owner = "Eric";
        protected long tail = 5L;

        public Dog() {
        }

        public Dog(int age) {
            this.age = age;
        }

        public Dog(String name, int age, String owner, long tail) {
            this.name = name;
            this.age = age;
            this.owner = owner;
            this.tail = tail;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public long getTail() {
            return tail;
        }

        public void setTail(long tail) {
            this.tail = tail;
        }
    }

    public static void main(String[] args) {

        Class<?> cl = Dog.class;

        System.out.println("Constructors:");
        Constructor<?>[] constructors = cl.getConstructors();
        int i = 0;
        for (Constructor<?> ctr : constructors) {
            System.out.print("\tConstructor "  + (++i) + ": ");
            Class<?>[] paramTypes = ctr.getParameterTypes();
            for (Class<?> paramType : paramTypes) {
                System.out.print(paramType.getName() + "   ");
            }
            System.out.println();
        }

        try {
            Class<?>[] paramTypes = new Class<?>[]{
                    int.class
            };
            Constructor<?> ctr = cl.getConstructor(paramTypes);
            Dog dog = (Dog) ctr.newInstance(Integer.valueOf(1));
            System.out.println("Fields: Age - " + dog.getAge() + ", Name - " + dog.getName() + ", Owner - " + dog.getOwner() + ", Tail - " + dog.getTail());
            System.out.println("-----");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Method[] methods = cl.getMethods();
        for (Method method : methods) {
            System.out.println("Name: " + method.getName());
            System.out.println("\tReturn type: " + method.getReturnType().getName());
            Class<?>[] paramTypes = method.getParameterTypes();
            System.out.print("\tParam types:");
            for (Class<?> paramType : paramTypes) {
                System.out.print("   " + paramType.getName());
            }
            System.out.println("\n-----");
        }

        try {
            Dog dog1 = new Dog();
            Class<?>[] paramTypes = new Class<?>[]{int.class};
            Method method = cl.getMethod("setAge", paramTypes);
            Object[] objArguments = new Object[]{Integer.valueOf(8)};

            method.invoke(dog1, objArguments);
            System.out.println("Age: " + dog1.getAge());
            System.out.println("-----");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            Dog obj = new Dog();
            Class<?>[] paramTypes = new Class<?>[]{String.class};
            // Try to invoke not existing method
            Method method = cl.getMethod("hellWorld", paramTypes);
            Object[] arguments = new Object[]{Integer.valueOf("Hello")};
            method.invoke(obj, arguments);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

}
