package com.ithillel.lesson16.reflection;

import java.lang.reflect.Modifier;

public class ReflectionModifierDemo_04 {

    private static final class Duck {
    }

    public static void main(String[] args) {

        Class cl = Duck.class;

        System.out.println("Class name: " + cl.getName());
        System.out.print("Modifiers of class: ");

        int mods = cl.getModifiers();

        if (Modifier.isPrivate(mods)) {
            System.out.print("private ");
        }
        if (Modifier.isAbstract(mods)) {
            System.out.print("abstract ");
        }
        if (Modifier.isStatic(mods)) {
            System.out.print("static ");
        }
        if (Modifier.isFinal(mods)) {
            System.out.print("final ");
        }

    }
}
