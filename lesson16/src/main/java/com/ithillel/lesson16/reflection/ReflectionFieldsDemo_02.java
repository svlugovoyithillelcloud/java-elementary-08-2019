package com.ithillel.lesson16.reflection;

import java.lang.reflect.Field;

public class ReflectionFieldsDemo_02 {

    private static final class Cat {
        public String name = "Murzik";
        private int age = 5;
        String owner = "Ivan";
        protected long tail = 20L;
    }

    public static void main(String[] args) throws IllegalAccessException {

        Class<?> cl = Cat.class;

        Cat cat = new Cat(); //            System.out.println("\tValue: " + field.get(cat));

        System.out.println("Public Reflection fields:");
        Field[] fields = cl.getFields();
        for (Field field : fields) {
            Class<?> fieldType = field.getType();
            System.out.println("\tName: " + field.getName());
            System.out.println("\tType: " + fieldType.getName());
            System.out.println("-----");
        }

        System.out.println("All Modifiers Reflection fields:");
        fields = cl.getDeclaredFields();
        for (Field field : fields) {
            Class<?> fieldType = field.getType();
            System.out.println("\tName: " + field.getName());
            System.out.println("\tType: " + fieldType.getName());
            System.out.println("-----");
        }
    }
}
