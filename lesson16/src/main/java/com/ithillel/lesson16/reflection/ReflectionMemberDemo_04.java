package com.ithillel.lesson16.reflection;

import java.lang.reflect.Member;

// -> java.lang.String
public class ReflectionMemberDemo_04 {

    public static void main(String[] args) {
        try {
            Class<?> c = Class.forName(args[0]);
            System.out.println(c);

            printMembers(c.getFields());
            printMembers(c.getConstructors());
            printMembers(c.getMethods());

        } catch (ClassNotFoundException e) {
            System.out.println("Неизвестный класс:" + args[0]);
        }
    }

    private static void printMembers(Member[] mems) {
        for (int i = 0; i < mems.length; i++) {
            if (mems[i].getDeclaringClass() == Object.class) {
                continue;
            }
            String decl = mems[i].toString();
            System.out.print("   ");
            System.out.println(decl);
        }
        System.out.println("-----");
    }
}
