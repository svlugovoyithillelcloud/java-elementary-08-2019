package com.ithillel.lesson16.reflection;

import java.lang.reflect.Field;

public class ReflectionAccessDemo_04 {

    private static final class Bird {
        private int age = 2;
    }

    public static void main(String[] args) {

        try {

            Class<?> cl = Bird.class;
            Bird bird = new Bird();

            Field field = cl.getDeclaredField("age");
            field.setAccessible(true);

            System.out.println("Private field value: " + field.getInt(bird));

            field.setInt(bird, 10);
            System.out.println("New private field value: " + field.getInt(bird));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
