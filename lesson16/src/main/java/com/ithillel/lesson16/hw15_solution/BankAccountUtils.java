package com.ithillel.lesson16.hw15_solution;

import java.math.BigDecimal;
import java.time.Month;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.reducing;

public class BankAccountUtils {

    // *@gmail.com
    public static List<BankAccount> getAccountsThatHaveGoogleEmail(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getEmail() != null && a.getEmail().endsWith("@gmail.com"))
                .collect(Collectors.toList());
    }

    public static List<String> getAllAccountsEmails(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .map(BankAccount::getEmail)
                .collect(Collectors.toList());
    }

    public static Map<String, String> getEmailLastNamePairs(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getEmail() != null && a.getLastName() != null)
                .collect(Collectors.toMap(
                        BankAccount::getEmail,
                        BankAccount::getLastName)
                );
    }

    public static BigDecimal calculateTotalBalance(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .map(BankAccount::getBalance)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static Optional<BankAccount> findRichestPerson(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .max(Comparator.comparing(
                        BankAccount::getBalance)
                );
    }

    public static Map<Long, BigDecimal> findAccountIdWithMaxBalance(List<BankAccount> accounts) {
        Optional<BankAccount> richestPerson = findRichestPerson(accounts);
        Map<Long, BigDecimal> info = new HashMap<>();
        richestPerson.ifPresent((acc) -> {
            info.put(acc.getId(), acc.getBalance());
        });
        return info;
    }

    public static Map<Month, List<BankAccount>> groupAccountsByItsBirthdaysMonth(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getBirthday() != null)
                .collect(Collectors.groupingBy(
                        a -> a.getBirthday().getMonth())
                );
    }

    //{a=6, b=1, c=2, d=2, e=2, i=3, j=1, l=5, n=8, o=4, r=2, s=3, t=2, u=3, v=2, y=1}
    public static Map<Character, Long> getCharacterFrequencyIgnoreCaseInFirstAndLastNames(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getFirstName() != null && a.getLastName() != null)
                .flatMap(a -> Stream.of(a.getFirstName(), a.getLastName()))
                .map(String::toLowerCase)
//                .peek(System.out::println)
                .flatMapToInt(s -> s.chars())
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        Collectors.counting())
                );
    }

    public static Map<Gender, Long> calculateMaleAndFemaleCount(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getGender() != null)
                .collect(Collectors.groupingBy(
                        BankAccount::getGender,
                        Collectors.counting()
                ));
    }

    public static Map<Month, Long> calculateAccountCountByBirthdayMonth(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getBirthday() != null)
                .collect(Collectors.groupingBy(
                        a -> a.getBirthday().getMonth(),
                        Collectors.counting()
                ));
    }

    //map where key is true or false, and value is list of male (for key = true), and female accounts (for key = false)
    public static Map<Boolean, List<BankAccount>> partitionMaleAccounts(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getGender() != null)
                .collect(Collectors.partitioningBy(
                        a -> a.getGender().equals(Gender.MALE))
                );
    }

    public static Map<Month, BigDecimal> groupTotalBalanceByCreationMonth(List<BankAccount> accounts) {
        return accounts.stream()
                .filter(Objects::nonNull)
                .filter(a -> a.getBalance() != null && a.getCreationDate() != null)
                .collect(Collectors.groupingBy(
                        a -> a.getCreationDate().getMonth(),
                        mapping(BankAccount::getBalance,
                                reducing(BigDecimal.ZERO, BigDecimal::add)))
                );
    }

}
