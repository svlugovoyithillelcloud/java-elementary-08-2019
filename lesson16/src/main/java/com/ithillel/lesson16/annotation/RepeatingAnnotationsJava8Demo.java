package com.ithillel.lesson16.annotation;

import java.lang.annotation.Repeatable;

public class RepeatingAnnotationsJava8Demo {

}


@interface Hints {
    Hint[] value();
}

@Repeatable(Hints.class)
@interface Hint {
    String value();
}

@Hints({@Hint("hint1"), @Hint("hint2")})
class PersonJ7 {}

@Hint("hint1")
@Hint("hint2")
class PersonJ8 {}
