package com.ithillel.lesson16.annotation;

import java.lang.annotation.*;
import java.lang.reflect.Method;

public class OwnAnnotationDemo_02 {
    public static void main(String[] args) throws Exception {

        System.out.println("Testing...");

        int passed = 0, failed = 0, count = 0, ignore = 0;

        Class<TestExample> obj = TestExample.class;

        // Process @TesterInfo
        if (obj.isAnnotationPresent(MyTesterInfo.class)) {

            Annotation annotation = obj.getAnnotation(MyTesterInfo.class);
            MyTesterInfo testerInfo = (MyTesterInfo) annotation;

            System.out.printf("%nPriority :%s", testerInfo.priority());
            System.out.printf("%nCreatedBy :%s", testerInfo.createdBy());
            System.out.printf("%nTags :");

            int tagLength = testerInfo.tags().length;
            for (String tag : testerInfo.tags()) {
                if (tagLength > 1) {
                    System.out.print(tag + ", ");
                } else {
                    System.out.print(tag);
                }
                tagLength--;
            }
            System.out.printf("%nLastModified :%s%n%n", testerInfo.lastModified());
        }

        // Process @Test
        for (Method method : obj.getDeclaredMethods()) {

            // if method is annotated with @MyTest
            if (method.isAnnotationPresent(MyTest.class)) {

                Annotation annotation = method.getAnnotation(MyTest.class);
                MyTest test = (MyTest) annotation;

                // if enabled = true (default)
                if (test.enabled()) {
                    try {
                        method.invoke(obj.newInstance());
                        System.out.printf("%s - Test '%s' - passed %n", ++count, method.getName());
                        passed++;
                    } catch (Throwable ex) {
                        System.out.printf("%s - Test '%s' - failed: %s %n", ++count, method.getName(), ex.getCause());
                        failed++;
                    }
                } else {
                    System.out.printf("%s - Test '%s' - ignored%n", ++count, method.getName());
                    ignore++;
                }
            }
        }
        System.out.printf("%nResult : Total : %d, Passed: %d, Failed %d, Ignore %d%n", count, passed, failed, ignore);
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) //can use in method only.
@interface MyTest {
    //should ignore this test?
    public boolean enabled() default true;
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) //on class level
@interface MyTesterInfo {

    public enum Priority {
        LOW, MEDIUM, HIGH
    }
    Priority priority() default Priority.MEDIUM;
    String[] tags() default "";
    String createdBy() default "Serhii";
    String lastModified() default "08/10/2019";
}

@MyTesterInfo(
        priority = MyTesterInfo.Priority.HIGH,
        createdBy = "ITHillel",
        tags = {"sales", "test" }
)
class TestExample {

    @MyTest
    void testA() {
        if (true)
            throw new RuntimeException("This test always failed");
    }

    @MyTest(enabled = false)
    void testB() {
        if (false)
            throw new RuntimeException("This test always passed");
    }

    @MyTest(enabled = true)
    void testC() {
        if (10 > 1) {
            // do nothing, this test always passed.
        }
    }
}