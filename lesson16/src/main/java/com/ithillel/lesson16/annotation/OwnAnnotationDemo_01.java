package com.ithillel.lesson16.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

public class OwnAnnotationDemo_01 {
    public static void main(String[] args) throws Exception {

        ValidLengthHandler validator = new ValidLengthHandler();
        Person person = new Person();
        person.setName("John");

        validator.handle(person);

        person.setName("John555");
        validator.handle(person);
    }
}

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@interface ValidLength {
    int maxLength() default 3;
}

class Person {
    @ValidLength(maxLength = 6)
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class ValidLengthHandler {
    public void handle(Object ob) throws Exception {
        Field[] fields = ob.getClass().getFields();

        for (Field field : fields) {
            if (field.isAnnotationPresent(ValidLength.class)) {

                ValidLength vl = field.getAnnotation(ValidLength.class);
                int maxLen = vl.maxLength();
                System.out.println("Max length is:" + maxLen);

                if (maxLen < field.get(ob).toString().length()) {
                    throw new RuntimeException("Name string greater than max length = " + maxLen);
                } else {
                    System.out.println("Person is valid!");
                }
            }
        }
    }

}