package com.ithillel.lesson16.classloader;

import java.lang.reflect.Method;

public class ClassLoadersDemo {
    public static void main(String[] args) throws Exception {

        ClassLoader sys = ClassLoader.getSystemClassLoader();
        ClassLoader ext = sys.getParent();
        ClassLoader boot = ext.getParent();

        System.out.println(sys);
        System.out.println(ext);
        System.out.println(boot);

        Class<?> clazz = Class.forName("com.ithillel.hello.SimpleHello");
//        Class<?> clazz = Class.forName("com.ithillel.lesson16.classloader.SimpleHello");
        Method method = clazz.getMethod("sayJustHello");
        Object obj = clazz.newInstance();
        method.invoke(obj);

//        URLClassLoader urlClassLoader = new URLClassLoader(new URL[] {
//                new URL("file:///Users/serhiiluhovyi/Movies/hello.jar")
//        });
//
//        Class<?> clazz = Class.forName("com.ithillel.hello.SimpleHello", true, urlClassLoader);
//        Method method = clazz.getMethod("sayJustHello");
//        Object obj = clazz.newInstance();
//        System.out.println(method.invoke(obj));

    }
}
