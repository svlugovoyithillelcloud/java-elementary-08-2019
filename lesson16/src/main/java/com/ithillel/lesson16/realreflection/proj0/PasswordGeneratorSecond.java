package com.ithillel.lesson16.realreflection.proj0;

import com.ithillel.lesson16.realreflection.proj1.PasswordGenerator;

public class PasswordGeneratorSecond implements PasswordGenerator {

    @Override
    public String generate() {
        return "SecondPassword2";
    }
}
