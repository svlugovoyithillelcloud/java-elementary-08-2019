package com.ithillel.lesson16.realreflection.proj1;

public class PasswordGeneratorFirst implements PasswordGenerator {

    static {
        System.out.println("PasswordGeneratorFirst loaded...");
    }

    @Override
    public String generate() {
        return "FirstPassword1";
    }
}
