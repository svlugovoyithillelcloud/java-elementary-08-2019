package com.ithillel.lesson16.realreflection.proj2;

public class PasswordGeneratorThird implements PasswordGenerator {

    static {
        System.out.println("PasswordGeneratorThird loaded...");
    }

    @Override
    public String generate() {
        return "ThirdPassword3";
    }

}
