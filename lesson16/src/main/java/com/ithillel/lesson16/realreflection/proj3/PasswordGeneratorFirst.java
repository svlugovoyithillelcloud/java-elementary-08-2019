package com.ithillel.lesson16.realreflection.proj3;

import com.ithillel.lesson16.realreflection.proj3.annotation.Generator;

public class PasswordGeneratorFirst implements PasswordGenerator {

    static {
        System.out.println("PasswordGeneratorFirst loaded...");
    }

    @Generator
    @Override
    public String generate() {
        return "FirstPassword1";
    }
}
