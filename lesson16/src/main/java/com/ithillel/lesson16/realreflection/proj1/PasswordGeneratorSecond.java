package com.ithillel.lesson16.realreflection.proj1;

public class PasswordGeneratorSecond implements PasswordGenerator {

    static {
        System.out.println("PasswordGeneratorSecond loaded...");
    }

    @Override
    public String generate() {
        return "SecondPassword2";
    }
}
