package com.ithillel.lesson16.realreflection.proj3;

import com.ithillel.lesson16.realreflection.proj3.annotation.Generator;

public class PasswordGeneratorThird implements PasswordGenerator {

    static {
        System.out.println("PasswordGeneratorThird loaded...");
    }

    @Generator
    @Override
    public String generate() {
        return "ThirdPassword3";
    }

}
