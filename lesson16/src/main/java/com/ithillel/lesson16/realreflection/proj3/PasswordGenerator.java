package com.ithillel.lesson16.realreflection.proj3;

public interface PasswordGenerator {

    String generate();

}
