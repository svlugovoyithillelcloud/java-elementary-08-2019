package com.ithillel.lesson16.realreflection.proj3;

import com.ithillel.lesson16.realreflection.proj3.annotation.Generator;

import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.Properties;

public class PasswordFactory {

    public static String getPassword() {
        try {
            String clazz = getGenerator();
            Class<?> genClass = Class.forName(clazz);
            Object generator = genClass.newInstance();

            Method[] methods = genClass.getMethods();

            for (Method method : methods) {
                System.out.println("Check method:" + method.getName());

                Generator ann = method.getAnnotation(Generator.class);

                if (ann != null) {
                    String pswd = (String) method.invoke(generator);
                    return pswd;
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return "default";
    }

    private static String getGenerator() throws Exception {
        Properties p = new Properties();
        p.load(new FileReader("lesson16/generator3.properties"));
        return p.getProperty("generator");
    }

}
