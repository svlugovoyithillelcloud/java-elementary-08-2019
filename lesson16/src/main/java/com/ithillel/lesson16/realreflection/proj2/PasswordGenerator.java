package com.ithillel.lesson16.realreflection.proj2;

public interface PasswordGenerator {

    String generate();

}
