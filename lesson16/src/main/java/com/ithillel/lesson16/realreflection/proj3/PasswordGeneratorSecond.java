package com.ithillel.lesson16.realreflection.proj3;

import com.ithillel.lesson16.realreflection.proj3.annotation.Generator;

public class PasswordGeneratorSecond implements PasswordGenerator {

    static {
        System.out.println("PasswordGeneratorSecond loaded...");
    }

    @Generator
    @Override
    public String generate() {
        return "SecondPassword2";
    }
}
