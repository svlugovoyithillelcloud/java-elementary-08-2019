package com.ithillel.lesson16.realreflection.proj1;

public interface PasswordGenerator {

    String generate();

}
