package com.ithillel.lesson16.realreflection.proj2;

public class PasswordGeneratorSecond implements PasswordGenerator {

    static {
        System.out.println("PasswordGeneratorSecond loaded...");
    }

    @Override
    public String generate() {
        return "SecondPassword2";
    }
}
