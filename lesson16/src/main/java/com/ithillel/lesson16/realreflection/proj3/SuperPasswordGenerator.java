package com.ithillel.lesson16.realreflection.proj3;

import com.ithillel.lesson16.realreflection.proj3.annotation.Generator;

public class SuperPasswordGenerator {

    public void aaa() {
    }

    public void bbb() {
    }

    @Generator
    public String createPassword() {
        return "SUPERpassword!!!";
    }
}
