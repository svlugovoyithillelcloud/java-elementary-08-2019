package com.ithillel.lesson16.realreflection.proj1;

import java.io.FileReader;
import java.util.Properties;

public class PasswordGeneratorFactory {

    public static PasswordGenerator getPasswordGenerator() {
        try {
            String clazz = getGenerator();

            Class<?> genClass = Class.forName(clazz);
            PasswordGenerator generator = (PasswordGenerator) genClass.newInstance();

            return generator;

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return new PasswordGeneratorFirst();
    }

    private static String getGenerator() throws Exception {
        Properties p = new Properties();
        p.load(new FileReader("lesson16/generator1.properties"));
        return p.getProperty("generator");
    }
}
