package com.ithillel.lesson16.realreflection.proj2;

public class PasswordGeneratorFirst implements PasswordGenerator {

    static {
        System.out.println("PasswordGeneratorFirst loaded...");
    }

    @Override
    public String generate() {
        return "FirstPassword1";
    }
}
