package com.ithillel.lesson16.realreflection.proj4;

import com.ithillel.lesson16.realreflection.proj4.annotation.Generator;

public class SuperPasswordGenerator {

    private String algorithm;
    private String name;

    @Generator
    private String createPassword() {
        return "SUPERpassword!!!" + algorithm + "," + name;
    }
}
