package com.ithillel.lesson16.homework16.dto;

import java.time.LocalDate;

public class SecretPerson {
    private Long id; // secret field from database
    private String password; // secret field from database
    private String firstName;
    private String lastName;
    private LocalDate birthday;
}
