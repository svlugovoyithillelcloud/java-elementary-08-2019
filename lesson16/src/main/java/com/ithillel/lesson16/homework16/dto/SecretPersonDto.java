package com.ithillel.lesson16.homework16.dto;

import java.time.LocalDate;

public class SecretPersonDto {
    private String first_name;
    private String last_name;
    private LocalDate birth;
}
