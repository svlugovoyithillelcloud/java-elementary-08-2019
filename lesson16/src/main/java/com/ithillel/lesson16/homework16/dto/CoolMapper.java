package com.ithillel.lesson16.homework16.dto;

public interface CoolMapper<T, D> {
    T fromDto(D dto);
    D toDto(T obj);
}
