package com.ithillel.lesson16.mapstruct;

import org.mapstruct.factory.Mappers;

public class MapStructDemo {
    public static void main(String[] args) {

        SourceObj sourceObj = new SourceObj("Hello", "World", "sercet");

        SourceDestinationMapper mapper = Mappers.getMapper(SourceDestinationMapper.class);

        DestinationObj destinationObj = mapper.sourceToDestination(sourceObj);

        System.out.println(sourceObj);
        System.out.println(destinationObj);

    }
}
