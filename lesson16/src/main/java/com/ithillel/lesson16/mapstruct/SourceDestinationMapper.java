package com.ithillel.lesson16.mapstruct;

import org.mapstruct.Mapper;

@Mapper
public interface SourceDestinationMapper {

    DestinationObj sourceToDestination(SourceObj source);

    SourceObj destinationToSource(DestinationObj destination);
}