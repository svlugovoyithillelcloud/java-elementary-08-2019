package com.ithillel.lesson16.mapstruct;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class DestinationObj {
    private String name;
    private String description;
}

