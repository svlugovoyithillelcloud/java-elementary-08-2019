package com.ithillel.lesson32.factorymethod.impl;

import com.ithillel.lesson32.factorymethod.Polygon;

public class Pentagon implements Polygon {

    @Override
    public String getType() {
        return "Pentagon";
    }

}
