package com.ithillel.lesson32.factorymethod;

public interface Polygon {

    String getType();

}
