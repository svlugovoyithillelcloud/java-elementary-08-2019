package com.ithillel.lesson32.factorymethod.impl;

import com.ithillel.lesson32.factorymethod.Polygon;

public class Triangle implements Polygon {

    @Override
    public String getType() {
        return "Triangle";
    }

}
