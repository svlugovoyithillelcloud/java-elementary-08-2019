package com.ithillel.lesson32.observer;

public interface Observer {

    public void update(Message m);

}
