package com.ithillel.lesson32.observer.subscribers;

import com.ithillel.lesson32.observer.Message;
import com.ithillel.lesson32.observer.Observer;

public class MessageSubscriberTwo implements Observer {

    @Override
    public void update(Message m) {
        System.out.println("MessageSubscriberTwo :: " + m.getMessageContent());
    }
}
