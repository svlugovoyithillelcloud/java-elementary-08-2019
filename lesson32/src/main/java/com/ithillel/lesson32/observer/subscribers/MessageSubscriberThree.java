package com.ithillel.lesson32.observer.subscribers;

import com.ithillel.lesson32.observer.Message;
import com.ithillel.lesson32.observer.Observer;

public class MessageSubscriberThree implements Observer {

    @Override
    public void update(Message m) {
        System.out.println("MessageSubscriberThree :: " + m.getMessageContent());
    }
}
