package com.ithillel.lesson32.proxy;

public interface ServiceInterface {

    String doOperation();
}
