document.onreadystatechange = function () {
    if (document.readyState === "complete") {
        var counter = 0;
        var button = document.getElementById("my-button");
        var reset = document.getElementById("my-reset");


        button.onclick = function () {
            counter++;
            console.log("Counter value - " + counter);
            button.innerHTML = "Click me: " + counter;
        };

        reset.onclick = function () {
            counter = 0;
            console.log("Counter value reset to 0");
            button.innerHTML = "Click me: " + counter;
            alert("Congrats. Counter reset!")
        };



    }
};
