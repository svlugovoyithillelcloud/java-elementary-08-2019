#!/bin/bash

mycomp="MacBook"
myos=`uname -a`

echo "$mycomp"
echo "$myos"
echo "Script name is $0"
echo "Hello 1st arg $1"
echo "Hello 2 nd arg $2"

num1=10
num2=40
sum=$((num1+num2))

echo "$num1 + $num2 = $sum"

myhost=`hostname`
google="8.8.8.8"

ping -c 4 $myhost
ping -c 4 $google

echo "Done."