#!/bin/bash

hello() {
  echo "Hello world!!!"
  echo "1st - $1"
  echo "2nd - $2"
  echo "3rd - $3"
  sum=$(($1+$2))
}

hello 10 25 Ivan

echo "Sum is $sum"

