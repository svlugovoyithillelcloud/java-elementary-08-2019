package com.ithillel.lesson20;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcDemo {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.cj.jdbc.Driver");

        Connection con = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/my_new_db?useSSL=false&serverTimezone=UTC", "root", "Password1");

        Statement stmt = con.createStatement();


//        String tableSql = "CREATE TABLE IF NOT EXISTS employees"
//                + "(emp_id int PRIMARY KEY AUTO_INCREMENT, name varchar(255),"
//                + "position varchar(30), salary int)";
//        stmt.execute(tableSql);


//        String insertSql = "INSERT INTO employees(name, position, salary)"
//                + " VALUES('John', 'developer', 2000)," +
//                " ('Ivan', 'manager', 3000)," +
//                " ('Anna', 'hr', 1500)";
//        stmt.executeUpdate(insertSql);


//        String selectSql = "SELECT * FROM employees";
//        ResultSet resultSet = stmt.executeQuery(selectSql);
//
//        List<Employee> employees = new ArrayList<>();
//
//        while (resultSet.next()) {
//            Employee emp = new Employee();
//            emp.setId(resultSet.getInt("emp_id"));
//            emp.setName(resultSet.getString("name"));
//            emp.setPosition(resultSet.getString("position"));
//            emp.setSalary(resultSet.getInt("salary"));
//            employees.add(emp);
//        }
//        employees.forEach(System.out::println);


//        String updatePositionSql = "UPDATE employees SET position=?, salary=? WHERE name=?";
//        PreparedStatement pstmt = con.prepareStatement(updatePositionSql);
//
//        pstmt.setString(1, "lead developer");
//        pstmt.setInt(2, 6000);
//        pstmt.setString(3, "John");
//
//        int rowsAffected = pstmt.executeUpdate();
//        System.out.println(rowsAffected);


        con.close();

    }
}
