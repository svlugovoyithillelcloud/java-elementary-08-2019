package com.ithillel.lesson20.homework20.book_hw.dao;

import com.ithillel.lesson20.homework20.book_hw.model.Book;

import javax.sql.DataSource;
import java.util.List;

public class BookDaoImpl implements BookDao {

    private DataSource dataSource;

    public BookDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Long save(Book book) {
        return null;
    }

    @Override
    public List<Book> findAll() {
        return null;
    }

    @Override
    public Book findById(Long id) {
        return null;
    }

    @Override
    public void update(Book book) {

    }

    @Override
    public void remove(Long id) {

    }
}
