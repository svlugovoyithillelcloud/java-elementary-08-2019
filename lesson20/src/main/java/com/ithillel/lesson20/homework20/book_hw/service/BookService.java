package com.ithillel.lesson20.homework20.book_hw.service;

import com.ithillel.lesson20.homework20.book_hw.model.Book;

import java.util.List;

public interface BookService {

    List<Book> findBooksWithPagesGreaterThan(int count);

    List<Book> searchInBooksContainsStringInName(String str);

}
