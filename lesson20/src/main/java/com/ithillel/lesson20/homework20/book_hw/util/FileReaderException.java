package com.ithillel.lesson20.homework20.book_hw.util;

public class FileReaderException extends RuntimeException {
    public FileReaderException(String message, Exception e) {
        super(message, e);
    }
}
