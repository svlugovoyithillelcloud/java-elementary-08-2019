package com.ithillel.lesson20.homework20.book_hw.dao;

import com.ithillel.lesson20.homework20.book_hw.model.Book;

import java.util.List;

public interface BookDao {

    Long save(Book book);

    List<Book> findAll();

    Book findById(Long id);

    void update(Book book);

    void remove(Long id);

}
