package com.ithillel.lesson20.homework20.book_hw;

import com.ithillel.lesson20.homework20.book_hw.dao.BookDao;
import com.ithillel.lesson20.homework20.book_hw.dao.BookDaoImpl;
import com.ithillel.lesson20.homework20.book_hw.exception.DaoOperationException;
import com.ithillel.lesson20.homework20.book_hw.model.Book;
import com.ithillel.lesson20.homework20.book_hw.service.BookService;
import com.ithillel.lesson20.homework20.book_hw.service.BookServiceImpl;
import com.ithillel.lesson20.homework20.book_hw.util.FileReader;
import com.ithillel.lesson20.homework20.book_hw.util.JdbcUtil;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Runner {

    private final static String TABLE_INITIALIZATION_SQL_FILE = "db/migration/table_initialization.sql";
    private final static String TABLE_POPULATION_SQL_FILE = "db/migration/table_population.sql";

    private static DataSource dataSource;
    private static BookDao bookDao;
    private static BookService bookService;

    public static void main(String[] args) {

        initDatasource();
        initTablesInDB();
        populateTablesInDB();
        initDao();
        initSvc();


//        List<Book> books = bookDao.findAll();
//        books.forEach(System.out::println);
//        System.out.println("#####***######");
//
//        Book newBook = new Book();
//        //сеттим поля для объекта вашими значениями (придумайте сами)
//        Long savedId = bookDao.save(newBook);
//        Book newBookById = bookDao.findById(savedId);
//        System.out.println(newBook);
//        System.out.println(newBookById);
//        System.out.println("#####***######");
//
//        Book bookById = bookDao.findById(5L);
//        System.out.println(bookById);
//        bookById.setName("UPDATED");
//        bookDao.update(bookById);
//        Book updatedBookById = bookDao.findById(5L);
//        System.out.println(updatedBookById);
//        System.out.println("#####***######");
//
//        bookDao.remove(5L);
//        try {
//            bookDao.findById(5L);
//            throw new RuntimeException("Should not be here");
//        } catch (DaoOperationException e){
//            System.out.println("Actor deleted successful");
//        }
//        System.out.println("#####***######");
//
//        List<Book> booksWithPagesGreaterThan300 = bookService.findBooksWithPagesGreaterThan(300);
//        booksWithPagesGreaterThan300.forEach(System.out::println);
//        System.out.println("#####***######");
//
//        List<Book> javaBooks = bookService.searchInBooksContainsStringInName("Java");
//        javaBooks.forEach(System.out::println);
//        System.out.println("#####***######");

    }

    private static void initDatasource() {
        dataSource = JdbcUtil.createPostgresDataSource(
                "jdbc:postgresql://localhost:5432/books_hw_db", "postgres", "Password1");
    }

    private static void initTablesInDB() {
        String createTablesSql = FileReader.readWholeFileFromResources(TABLE_INITIALIZATION_SQL_FILE);
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute(createTablesSql);
            connection.commit();
        } catch (SQLException e) {
            throw new DaoOperationException("Shit happened during tables init.", e);
        }
    }

    private static void populateTablesInDB() {
        String createTablesSql = FileReader.readWholeFileFromResources(TABLE_POPULATION_SQL_FILE);
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute(createTablesSql);
            connection.commit();
        } catch (SQLException e) {
            throw new DaoOperationException("Shit happened during tables population.", e);
        }
    }

    private static void initDao() {
        bookDao = new BookDaoImpl(dataSource);
    }

    private static void initSvc() {
        bookService = new BookServiceImpl(dataSource);
    }
}
