package com.ithillel.lesson20.homework20.book_hw.service;

import com.ithillel.lesson20.homework20.book_hw.dao.BookDao;
import com.ithillel.lesson20.homework20.book_hw.dao.BookDaoImpl;
import com.ithillel.lesson20.homework20.book_hw.model.Book;

import javax.sql.DataSource;
import java.util.List;

public class BookServiceImpl implements BookService {

    private BookDao bookDao;

    public BookServiceImpl(DataSource dataSource) {
        this.bookDao = new BookDaoImpl(dataSource);
    }

    @Override
    public List<Book> findBooksWithPagesGreaterThan(int count) {
        return null;
    }

    @Override
    public List<Book> searchInBooksContainsStringInName(String str) {
        return null;
    }
}
