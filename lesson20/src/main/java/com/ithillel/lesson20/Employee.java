package com.ithillel.lesson20;

import lombok.Data;

@Data
public class Employee {
    private int id;
    private String name;
    private String position;
    private int salary;
}
