INSERT INTO books (name, author, page_count, publish_date) VALUES ('Head First Java', 'Kathy Sierra', 654, '2015-10-10');
INSERT INTO books (name, author, page_count, publish_date) VALUES ('Effective Java 3rd Edition', 'Josh Bloch', 430, '2018-09-15');
INSERT INTO books (name, author, page_count, publish_date) VALUES ('Java in 24 Hours', 'Rogers Cadenhead', 354, '2015-10-10');
INSERT INTO books (name, author, page_count, publish_date) VALUES ('Head First Java', 'Jesse Liberty', 722, '2014-10-10');
INSERT INTO books (name, author, page_count, publish_date) VALUES ('Head First Design Patterns', 'Eric Freeman', 854, '2016-12-12');
INSERT INTO books (name, author, page_count, publish_date) VALUES ('HTML und CSS', 'Eric Freeman', 254, '2011-01-12');
INSERT INTO books (name, author, page_count, publish_date) VALUES ('Java: Learn Java in One Day', 'Jamie Chan', 100, '2019-08-08');
