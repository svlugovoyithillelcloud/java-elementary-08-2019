
DROP TABLE IF EXISTS books;

CREATE TABLE books (
  id SERIAL,
  name VARCHAR(255) NOT NULL,
  author VARCHAR(255) NOT NULL,
  page_count INT NOT NULL,
  publish_date DATE NOT NULL,

  CONSTRAINT actors_PK PRIMARY KEY (id)
);




