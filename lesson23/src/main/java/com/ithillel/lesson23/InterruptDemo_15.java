package com.ithillel.lesson23;

import java.util.Random;

public class InterruptDemo_15 {
    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 1000000000; i++) {
//                if (Thread.currentThread().isInterrupted()) {
//                    System.out.println("Thread was interrupted...");
//                    break;
//                }
                Random random = new Random();
                Math.sin(random.nextDouble());
            }
        });

        System.out.println("Starting thread...");

        t1.start();

//        Thread.sleep(5000);
//        t1.stop(); // deprecated
//        t1.interrupt(); //не прерывает, меняет состояние

        t1.join();


        System.out.println("Thread finished...");


    }
}
