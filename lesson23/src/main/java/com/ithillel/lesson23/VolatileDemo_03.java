package com.ithillel.lesson23;

public class VolatileDemo_03 {

    private static int MY_INT = 0; //.....And the change listener loop infinitely...
//    private static volatile int MY_INT = 0;

    //The volatile keyword tells the JVM that it may be modified by another thread.
    // Each thread has its own stack, and so its own copy of variables it can access.
    // When a thread is created, it copies the value of all accessible variables in its own memory.

    public static void main(String[] args) {

        new ChangeListener().start();
        new ChangeMaker().start();

    }


    static class ChangeListener extends Thread {
        @Override
        public void run() {
            int local_value = MY_INT;
            while (local_value < 5) {
                if (local_value != MY_INT) {
                    System.out.println("Got Change for MY_INT : " +  MY_INT);
                    local_value = MY_INT;
                }
            }
        }
    }

    static class ChangeMaker extends Thread {
        @Override
        public void run() {

            int local_value = MY_INT;
            while (MY_INT < 5) {
                System.out.println("Incrementing MY_INT to " + (local_value + 1));
                MY_INT = ++local_value;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
