package com.ithillel.lesson23;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

public class CompletableFutureDemo_17 {
    public static void main(String[] args) throws InterruptedException {

        //Executors.newCachedThreadPool()
        CompletableFuture<Void> future3 = CompletableFuture.runAsync(() -> System.out.println("Hi from runAsync with newCachedThreadPool"), Executors.newCachedThreadPool());

        //ForkJoinPool.commonPool()
        //supplyAsync -> Supplier
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> "Hi from supplyAsync");

        //runAsync -> Runnable
        CompletableFuture<Void> future2 = CompletableFuture.runAsync(() -> System.out.println("Hi from runAsync"));

        //callback
        future1.thenAccept(System.out::println);

        //chain of callbacks
        CompletableFuture<String> future4 = CompletableFuture.supplyAsync(() -> "Hi, ");
        future4
                .thenApply(res -> res + "Java ")
                .thenApply(res -> {
                    String result = res + "Students!!!";
                    System.out.println(result);
                    return result;
                });


        Thread.sleep(15000);

        //https://annimon.com/article/3462
    }
}
