package com.ithillel.lesson23;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DeadlockDemoLock3_14 {
    public static void main(String[] args) throws InterruptedException {

        RunnerS3 runner = new RunnerS3();

        Thread t1 = new Thread(() -> {
            runner.firstThread();
        });
        Thread t2 = new Thread(() -> {
            runner.secondThread();
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        runner.finish();
    }
}

class RunnerS3 {
    private AccountS3 acc1 = new AccountS3();
    private AccountS3 acc2 = new AccountS3();

    private Lock lock1 = new ReentrantLock();
    private Lock lock2 = new ReentrantLock();

    public void firstThread() {
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            lock1.lock();
            lock2.lock();
            try {
                AccountS3.transfer(acc1, acc2, random.nextInt(100));
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }

    public void secondThread() {
        Random random = new Random();

        for (int i = 0; i < 10000; i++) {
            lock1.lock();
            lock2.lock();
//            lock2.lock(); //deadlock cause
//            lock1.lock();
            try {
                AccountS3.transfer(acc2, acc1, random.nextInt(100));
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }

    public void finish() {
        System.out.println("Account 1 - " + acc1.getBalance());
        System.out.println("Account 2 - " + acc2.getBalance());
        System.out.println("Total - " + (acc1.getBalance() + acc2.getBalance()));
    }

}

class AccountS3 {
    private int balance = 10000;

    public void deposit(int amount) {
        balance += amount;
    }

    public void withdraw(int amount) {
        balance -= amount;
    }

    public int getBalance() {
        return balance;
    }

    public static void transfer(AccountS3 a1, AccountS3 a2, int amount) {
        a1.withdraw(amount);
        a2.deposit(amount);
    }
}