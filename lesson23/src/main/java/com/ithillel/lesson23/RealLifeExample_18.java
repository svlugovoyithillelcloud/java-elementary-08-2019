package com.ithillel.lesson23;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RealLifeExample_18 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Client client = new Client();
        List<String> list = new ArrayList<>();

        long start = System.nanoTime();

        String resp1 = client.goToFirstResource();
        String resp2 = client.goToSecondResource();
        String resp3 = client.goToThirdResource();
        list.add(resp1);
        list.add(resp2);
        list.add(resp3);
        System.out.println(list);

//        ExecutorService service = Executors.newCachedThreadPool();
//        Future<String> future1 = service.submit(client::goToFirstResource);
//        Future<String> future2 = service.submit(client::goToSecondResource);
//        String resp3 = client.goToThirdResource();
//        String resp1 = future1.get();
//        String resp2 = future2.get();
//        list.add(resp1);
//        list.add(resp2);
//        list.add(resp3);
//        System.out.println(list);
//        service.shutdown();

        long end = System.nanoTime();
        System.out.println("Execution time - " + (end - start) / 1000000000 + " sec");

    }
}

class Client {
    public String goToFirstResource() {
        System.out.println("Requesting 1st resource...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "First Response";
    }

    public String goToSecondResource() {
        System.out.println("Requesting 2nd resource...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Second Response";
    }

    public String goToThirdResource() {
        System.out.println("Requesting 3rd resource...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Third Response";
    }

}
