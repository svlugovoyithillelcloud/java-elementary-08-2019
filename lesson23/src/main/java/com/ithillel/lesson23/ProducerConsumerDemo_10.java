package com.ithillel.lesson23;

import java.util.LinkedList;
import java.util.Queue;

public class ProducerConsumerDemo_10 {

    public static void main(String[] args) throws InterruptedException {

        ProducerConsumer pc = new ProducerConsumer();

        Thread t1 = new Thread(() -> {
            try {
                pc.produce();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                pc.consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();
    }
}

class ProducerConsumer {

    private Queue<Integer> queue = new LinkedList<>();
    private static final int LIMIT = 10;
    private final Object lock = new Object();

    public void produce() throws InterruptedException {
        int value = 0;
        while (true) {
            synchronized (lock) {
                while (queue.size() == LIMIT){ //if тоже будет работать, но while дополнительная проверка ==10
                    lock.wait();
                }
                queue.offer(value++);
                System.out.println(queue);
                lock.notify();
            }
        }

    }

    public void consume() throws InterruptedException {
        while (true) {
            synchronized (lock) {
                while (queue.size() == 0){
                    lock.wait();
                }
                Integer poll = queue.poll();
                System.out.println(poll);
                System.out.println("Queue size - " + queue.size());
                lock.notify();
            }
            Thread.sleep(1000);
        }
    }
}
