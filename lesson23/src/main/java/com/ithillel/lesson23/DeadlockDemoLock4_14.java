package com.ithillel.lesson23;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DeadlockDemoLock4_14 {
    public static void main(String[] args) throws InterruptedException {

        RunnerS4 runner = new RunnerS4();

        Thread t1 = new Thread(() -> {
            runner.firstThread();
        });
        Thread t2 = new Thread(() -> {
            runner.secondThread();
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        runner.finish();
    }
}

class RunnerS4 {
    private AccountS4 acc1 = new AccountS4();
    private AccountS4 acc2 = new AccountS4();

    private Lock lock1 = new ReentrantLock();
    private Lock lock2 = new ReentrantLock();

    private void takeLocks(Lock l1, Lock l2) {
        boolean firstLockTaken = false;
        boolean secondLockTaken = false;
        while (true) {
            try {
                firstLockTaken = l1.tryLock();
                secondLockTaken = l2.tryLock();
            } finally {
                if (firstLockTaken && secondLockTaken) {
                    return;
                }
                if (firstLockTaken) {
                    l1.unlock();
                }
                if (secondLockTaken) {
                    l2.unlock();
                }
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void firstThread() {
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            takeLocks(lock1, lock2);
            try {
                AccountS4.transfer(acc1, acc2, random.nextInt(100));
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }

    public void secondThread() {
        Random random = new Random();

        for (int i = 0; i < 10000; i++) {
            takeLocks(lock2, lock1);
            try {
                AccountS4.transfer(acc2, acc1, random.nextInt(100));
            } finally {
                lock1.unlock();
                lock2.unlock();
            }
        }
    }

    public void finish() {
        System.out.println("Account 1 - " + acc1.getBalance());
        System.out.println("Account 2 - " + acc2.getBalance());
        System.out.println("Total - " + (acc1.getBalance() + acc2.getBalance()));
    }

}

class AccountS4 {
    private int balance = 10000;

    public void deposit(int amount) {
        balance += amount;
    }

    public void withdraw(int amount) {
        balance -= amount;
    }

    public int getBalance() {
        return balance;
    }

    public static void transfer(AccountS4 a1, AccountS4 a2, int amount) {
        a1.withdraw(amount);
        a2.deposit(amount);
    }
}