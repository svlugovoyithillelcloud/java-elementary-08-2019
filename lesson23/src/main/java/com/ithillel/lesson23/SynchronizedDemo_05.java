package com.ithillel.lesson23;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SynchronizedDemo_05 {

    public static void main(String[] args) throws InterruptedException {
        Worker worker = new Worker();
        worker.main();

    }
}


class Worker {
    Object lock1 = new Object();
    Object lock2 = new Object();

    private List<Integer> list1 = new ArrayList<>();
    private List<Integer> list2 = new ArrayList<>();

    public void addToList1() {
//    public synchronized void addToList1() { // решили race condition, но Execution time - 5 сек
//        synchronized (lock1){ //// решили race condition и Execution time меньше 5 сек (2,5 сек)
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                list1.add(ThreadLocalRandom.current().nextInt(1, 100));
//            }
    }

    public void addToList2() {
//    public synchronized void addToList2() {
//        synchronized (lock2){
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                list2.add(ThreadLocalRandom.current().nextInt(1, 100));
//            }
    }

    public void doWork() {
        for (int i = 0; i < 1000; i++) {
            addToList1();
            addToList2();
        }
    }

    public void main() {
        long before = System.nanoTime();

        doWork();

        long after = System.nanoTime();
        System.out.println("Execution time - " + (after - before)/ 1000000 + " ms");

        System.out.println("List 1: " + list1.size());
        System.out.println("List 2: " + list2.size());
    }

//    public void main() throws InterruptedException {
//        long before = System.nanoTime();
//
//        //хотим задействовать 2 ядра и добавить в 2 раза больше элементов в листы за тоже время 2,5 сек
//        //race condition
//        Thread t1 = new Thread(() -> doWork());
//        Thread t2 = new Thread(() -> doWork());
//
//        t1.start();
//        t2.start();
//        t1.join();
//        t2.join();
//
//
//        long after = System.nanoTime();
//        System.out.println("Execution time - " + (after - before)/ 1000000 + " ms");
//
//        System.out.println("List 1: " + list1.size());
//        System.out.println("List 2: " + list2.size());
//    }

}