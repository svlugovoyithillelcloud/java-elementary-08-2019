package com.ithillel.lesson23;

import java.util.Scanner;

public class VolatileDemo_02 {
    public static void main(String[] args) {

        MyThread1 t1 = new MyThread1();
        t1.start();


        Scanner scanner =new Scanner(System.in);
        scanner.nextLine();

        t1.shutdown();
    }
}

class MyThread1 extends Thread {

    private volatile boolean running = true;
    //https://en.wikipedia.org/wiki/Cache_coherence

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep(500);
                System.out.println("Hello, I am - " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown(){
        running = false;
    }
}