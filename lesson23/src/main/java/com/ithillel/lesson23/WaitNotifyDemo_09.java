package com.ithillel.lesson23;

import java.util.Scanner;

public class WaitNotifyDemo_09 {
    public static void main(String[] args) throws InterruptedException {

        Object obj = new Object();
//        obj.wait();
//        obj.wait(1000);
//        obj.wait(1000, 1000);
//        obj.notify();
//        obj.notifyAll();

        WaitAndNotify wn = new WaitAndNotify();

        Thread t1 = new Thread(() -> {
            try {
                wn.produce();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                wn.consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();
    }
}

class WaitAndNotify {

    public void produce() throws InterruptedException {
        synchronized (this) {
            System.out.println("Producer thread started...");

            wait(); //только внутри synchronized, вызывается на (this)
            // 1 - отдаем лок, освобождаем монитор
            // 2 - ждем пока будет вызван notify() на этом же мониторе

//            wait(5000); // таймаут, поток продолжит работу через 5 сек (если монитор будет свободный)

            System.out.println("Producer thread resumed...");
        }
    }

    public void consume() throws InterruptedException {
        Thread.sleep(1000); //ждем чтоб поток продюсер точно был первым
        Scanner scanner = new Scanner(System.in);

        synchronized (this){
            System.out.println("Waiting for return key pressed...");
            scanner.nextLine();
            notify(); //не освобождает монитор, просто уведоммляет другие потоки
//            notifyAll();

            System.out.println("notify() потоков, которые  wait() - но монитор я отдам через 10 сек");
            Thread.sleep(10000);
        }

    }
}
