package com.ithillel.lesson23;

// Theoretically, to ‘yield’ means to let go, to give up, to surrender.
// A yielding thread tells the virtual machine that it’s willing to let other threads be scheduled in its place.
// This indicates that it’s not doing something too critical.
// Note that it’s only a hint, though, and not guaranteed to have any effect at all.

public class YieldDemo_07 {
    public static void main(String[] args) {

        Thread producer = new Producer();
        Thread consumer = new Consumer();

        producer.setPriority(Thread.MIN_PRIORITY); //Min Priority
        consumer.setPriority(Thread.MAX_PRIORITY); //Max Priority

        //In below example program, I have created two threads named producer and consumer for no specific reason.
        // Producer is set to minimum priority and consumer is set to maximum priority.
        // I will run below code with/without commenting the line Thread.yield().
        // Without yield(), though the output changes sometimes,
        // but usually first all consumer lines are printed and then all producer lines.
        //
        //With using yield() method, both prints one line at a time and pass the chance to another thread,
        // almost all the time.

        producer.start();
        consumer.start();
    }
}

class Producer extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("I am Producer : Produced Item " + i);
//            Thread.yield();
        }
    }
}

class Consumer extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("I am Consumer : Consumed Item " + i);
//            Thread.yield();
        }
    }
}