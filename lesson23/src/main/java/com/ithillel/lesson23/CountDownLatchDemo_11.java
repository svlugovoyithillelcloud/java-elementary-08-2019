package com.ithillel.lesson23;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CountDownLatchDemo_11 {
    public static void main(String[] args) throws InterruptedException {

        CountDownLatch cdl = new CountDownLatch(3); // 3 раза отсчитать и защелка будет открыта

        ExecutorService es = Executors.newFixedThreadPool(3);

        for (int i = 0; i < 3; i++) {
            es.submit(new Processor(cdl));
        }

        cdl.await(); //main поток будет ждать пока защелка откроется
        System.out.println("Latch was opened, main thread is processins...");

        es.shutdown();
    }
}

class Processor implements Runnable {

    private CountDownLatch cdl;

    public Processor(CountDownLatch cdl) {
        this.cdl = cdl;
    }

    @Override
    public void run() {
        try {
            System.out.println("Started thread - " + Thread.currentThread().getName());
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cdl.countDown();
    }
}