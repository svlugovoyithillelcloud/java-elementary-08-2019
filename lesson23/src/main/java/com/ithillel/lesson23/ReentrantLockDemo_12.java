package com.ithillel.lesson23;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockDemo_12 {
    public static void main(String[] args) throws InterruptedException {

        Task task = new Task();

        Thread t1 = new Thread(() -> {
            task.firstThread();
        });
        Thread t2 = new Thread(() -> {
            task.secondThread();
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        task.showCounterValue();
    }
}

class Task {
    private int counter;
//    private Lock lock = new ReentrantLock();

    private void increment() {
        for (int i = 0; i < 10000; i++) {
            counter++;
        }
    }

    public void firstThread() {
//        lock.lock();
        increment();
//        lock.unlock(); //работает также как и synchronized
    }

    public void secondThread() {
//        lock.lock();
        increment();
//        lock.unlock();
    }

    public void showCounterValue() {
        System.out.println(counter);
    }
}
