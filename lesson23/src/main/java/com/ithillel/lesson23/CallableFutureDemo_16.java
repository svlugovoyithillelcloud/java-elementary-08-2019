package com.ithillel.lesson23;

import java.util.concurrent.*;

public class CallableFutureDemo_16 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService es = Executors.newFixedThreadPool(1);

        Future<Integer> future = es.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                System.out.println("Starting...");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Finished...");
                return calculate();
            }
        });

        Integer result = future.get();
        System.out.println(result);


//        Future<Integer> future2 = es.submit(new Callable<Integer>() {
//            @Override
//            public Integer call() {
//                return newCalculate();
//            }
//        });
//
//        try {
//            future2.get();
//        } catch (ExecutionException ex) {
//            Throwable th = ex.getCause();
//            System.out.println(th.getMessage());
//        }


        es.shutdown();
    }

    public static int calculate() {
        return 5 + 5;
    }

    public static int newCalculate() {
        throw new RuntimeException("Something happens...");
    }
}
