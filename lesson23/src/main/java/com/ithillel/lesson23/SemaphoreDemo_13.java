package com.ithillel.lesson23;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreDemo_13 {
    public static void main(String[] args) {

        //Механизм Semaphore позволяет ограничивать количество одновременных взаимодействий с рессурсом

        Semaphore semaphore = new Semaphore(3); // лимит на количество потоков одновременно использующих рессурс

        try {
            semaphore.acquire(); // начинаем взаимодействовать с рессурсом
            semaphore.acquire();
            semaphore.acquire();

//            semaphore.acquire();
//            System.out.println("Not reachable code...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        semaphore.release(); // отпускаем рессурс
        int permits = semaphore.availablePermits();// сколько разрешений еще доступно
        System.out.println(permits);


//        ExecutorService es = Executors.newFixedThreadPool(10);
//        for (int i = 0; i < 200; i++) {
//            es.submit(() -> {
//                try {
//                    Connection.getInstance().work();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            });
//        }
//
//
//
//
//        es.shutdown();
    }
}

//Singleton
class Connection {
    private static Connection connection = new Connection();
    private int connectionCount;
    private Semaphore semaphore = new Semaphore(10);

    private Connection() {
    }

    public static Connection getInstance() {
        return connection;
    }

    public void doWork() throws InterruptedException {

        synchronized (this) {
            connectionCount++;
            System.out.println(connectionCount);
        }

        Thread.sleep(5000);

        synchronized (this) {
            connectionCount--;
            System.out.println(connectionCount);
        }
    }

    public void work() throws InterruptedException {
        semaphore.acquire();
        try {
            doWork();
        } finally {
            semaphore.release();
        }
    }
}
