package com.ithillel.lesson23;

public class ThreadStartDemo_01 {
    public static void main(String[] args) throws InterruptedException {

        MyThread t1 = new MyThread();

//        t1.run();
//        t1.start();

//        Thread t2 = new Thread(new MyRunner());
//        t2.setDaemon(true);
//        t2.start();

//        Thread t3 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("Hello, I am - " + Thread.currentThread().getName());
//            }
//        });
//        t3.start();
//
//        Thread t4 = new Thread(() -> {
//            System.out.println("Hello, I am - " + Thread.currentThread().getName());
//        });
//        t4.start();

        Thread.sleep(1500);
        System.out.println(Thread.currentThread().getName() + " is finished...");
    }
}

class MyThread extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(500);
                System.out.println("Hello, I am - " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class MyRunner implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(500);
                System.out.println("Hello, I am - " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}