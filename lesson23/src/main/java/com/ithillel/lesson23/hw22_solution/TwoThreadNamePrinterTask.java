package com.ithillel.lesson23.hw22_solution;

import java.util.concurrent.ThreadLocalRandom;

public class TwoThreadNamePrinterTask {
    public static void main(String[] strings) {

        Object lock = new Object();

        new CoolThread(lock).start();
        new CoolThread(lock).start();

    }
}

class CoolThread extends Thread {

    // общий для двух потоков lock
    private Object lock;

    public CoolThread(Object lock) {
        this.lock = lock;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (lock) {
                try {
                    System.out.println(getName());
//                    System.out.println(Thread.currentThread().getName());
                    Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 3000));
                    lock.notify();
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}