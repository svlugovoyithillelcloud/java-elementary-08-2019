package com.ithillel.lesson23;

public class SynchronizedJoinDemo_04 {

    private int counter = 0;

    public void increment() {
        counter++;
    }

//    public synchronized void increment() {
//        counter++;
//    }

    public static void main(String[] args) throws InterruptedException {
        SynchronizedJoinDemo_04 obj = new SynchronizedJoinDemo_04();
        obj.doWork();

    }

    public void doWork() throws InterruptedException {

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increment();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increment();
            }
        });

        t1.start();
        t2.start();
        //race condition


        t1.join(); // в текущем потоке (мейн) ждем окончания t1, стоим тут и дальше не идем (показать while (true) в t1)
        t2.join(); // в текущем потоке (мейн) ждем окончания t2


        System.out.println(counter);
    }
}
