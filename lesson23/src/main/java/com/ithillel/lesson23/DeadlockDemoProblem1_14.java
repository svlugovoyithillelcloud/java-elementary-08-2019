package com.ithillel.lesson23;

import java.util.Random;

public class DeadlockDemoProblem1_14 {
    public static void main(String[] args) throws InterruptedException {

        Runner runner = new Runner();

        Thread t1 = new Thread(() -> {
            runner.firstThread();
        });
        Thread t2 = new Thread(() -> {
            runner.secondThread();
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        runner.finish();
    }
}

class Runner {
    private Account acc1 = new Account();
    private Account acc2 = new Account();

    public void firstThread() {
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            Account.transfer(acc1, acc2, random.nextInt(100)); //гонка
        }
    }

    public void secondThread() {
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            Account.transfer(acc2, acc1, random.nextInt(100)); //гонка
        }
    }

    public void finish() {
        System.out.println("Account 1 - " + acc1.getBalance());
        System.out.println("Account 2 - " + acc2.getBalance());
        System.out.println("Total - " + (acc1.getBalance() + acc2.getBalance()));
    }

}

class Account {
    private int balance = 10000;

    public void deposit(int amount) {
        balance += amount;
    }

    public void withdraw(int amount) {
        balance -= amount;
    }

    public int getBalance() {
        return balance;
    }

    public static void transfer(Account a1, Account a2, int amount) {
        a1.withdraw(amount);
        a2.deposit(amount);
    }
}