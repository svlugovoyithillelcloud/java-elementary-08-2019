package com.ithillel.lesson14.lambdas;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;

public class SupplierDemo_05 {
    public static void main(String[] args) {

        Supplier<Integer> randomIntSupplier = () -> ThreadLocalRandom.current().nextInt(0, 1001);
        System.out.println(randomIntSupplier.get());

        Supplier<StringBuilder> sbSupplier = () -> new StringBuilder("initial");
        System.out.println(sbSupplier.get().append("!!!"));

    }
}
