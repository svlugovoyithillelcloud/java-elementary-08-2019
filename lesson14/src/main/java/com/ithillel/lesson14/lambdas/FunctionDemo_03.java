package com.ithillel.lesson14.lambdas;

import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.function.Function;

public class FunctionDemo_03 {
    public static void main(String[] args) {

        Function<String, Integer> toInteger = s -> Integer.valueOf(s);
        int i = toInteger.apply("999");

        Function<String, String> backToString = toInteger.andThen(String::valueOf);
        backToString.apply("123");

        Function<String, String> capitalize = s -> s.toUpperCase();
        System.out.println(capitalize.apply("hello"));

        Function<String, MyObj> myObjCreator = s -> new MyObj(s);
        System.out.println(myObjCreator.apply("John"));

    }
}

@AllArgsConstructor
@ToString
class MyObj {
    private String name;
}