package com.ithillel.lesson14.lambdas;

public class LambdasDemo_01 {
    public static void main(String[] args) {

        Operation operation = new Operation() {
            @Override
            public int calculate(int x, int y) {
                return x + y;
            }
        };
        int res = operation.calculate(3, 4);
        System.out.println(res);


        Operation add = (x, y) -> x + y;

        Operation sub = (int x, int y) -> x - y;

        Operation div = (x, y) -> {
            System.out.println("hello div");
            return x / y;
        };

        Operation multiple = (x, y) -> {return x * y;};

        int resultAdd = add.calculate(2, 2);
        int resultSub = sub.calculate(10, 5);
        int resultDiv = div.calculate(10, 5);
        int resultMultiple = multiple.calculate(10, 10);

        System.out.println(resultAdd);
        System.out.println(resultSub);
        System.out.println(resultDiv);
        System.out.println(resultMultiple);

        //What Lambda Expressions are compiled to?
        //https://www.logicbig.com/tutorials/core-java-tutorial/java-8-enhancements/java-lambda-functional-aspect.html
        //https://habr.com/ru/post/111456/
    }
}

@FunctionalInterface
interface Operation {

    int calculate(int x, int y);

    //    int calculate2(int x, int y);


//    default void show() {
//        System.out.println("hello");
//    }
//    static void print() {
//        System.out.println("world");
//    }
}