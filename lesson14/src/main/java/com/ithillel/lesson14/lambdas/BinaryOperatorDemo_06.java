package com.ithillel.lesson14.lambdas;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.function.BinaryOperator;

public class BinaryOperatorDemo_06 {
    public static void main(String[] args) {

        BinaryOperator<Integer> summator = (a, b) -> a + b;
        System.out.println(summator.apply(5, 7));

        BinaryOperator<String> concatenator = (s1, s2) -> s1 + s2;
        System.out.println(concatenator.apply("Hello ", "world!!!"));

        BinaryOperator<Person> getYoungerPerson = (person1, person2) -> {
            if (person1 != null && person2 != null) {
                if (person1.getAge() > person2.getAge()) {
                    return person2;
                } else {
                    return person1;
                }
            }
            return null;
        };

        Person john = new Person("John", 55);
        Person anne = new Person("Anne", 25);

        Person younger = getYoungerPerson.apply(john, anne);
        System.out.println(younger);
    }
}

@AllArgsConstructor
@Getter
@ToString
class Person {
    private String name;
    private int age;
}
