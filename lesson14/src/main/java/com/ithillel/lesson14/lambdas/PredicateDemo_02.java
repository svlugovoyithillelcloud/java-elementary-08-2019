package com.ithillel.lesson14.lambdas;

import java.util.function.Predicate;

public class PredicateDemo_02 {
    public static void main(String[] args) {

        Predicate<String> predicate = (s) -> s.length() > 0;

        System.out.println("predicate.test(\"foo\") - " + predicate.test("foo"));
        System.out.println("predicate.negate().test(\"foo\") - " + predicate.negate().test("foo"));


        Predicate<String> isEmpty = s -> s.isEmpty();

        System.out.println("isEmpty.test(\"\") - " + isEmpty.test(""));
        System.out.println("isEmpty.test(\"1\") - " + isEmpty.test("1"));


        Predicate<Object> nonNull = o -> o != null;

        Predicate<String> isUkraine = country -> country.equals("Ukraine");

    }
}
