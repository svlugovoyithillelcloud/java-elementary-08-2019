package com.ithillel.lesson14.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerDemo_04 {
    public static void main(String[] args) {

        Consumer<String> greeter = (name) -> System.out.println("Hello, " + name + "!");
        greeter.accept("John");

        Consumer<List<Integer>> multiply2x = list -> {
            for (int i = 0; i < list.size(); i++)
                list.set(i, 2 * list.get(i));
        };

        List<Integer> ints = Arrays.asList(1, 2, 3);
        multiply2x.accept(ints);

        System.out.println(ints);
    }
}
