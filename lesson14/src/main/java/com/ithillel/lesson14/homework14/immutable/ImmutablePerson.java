package com.ithillel.lesson14.homework14.immutable;

import java.util.Date;
import java.util.List;

public class ImmutablePerson {
    public Long id;
    public String name;
    public Date date;
    public Address address;
    public List<PhoneNumber> phones;
}
