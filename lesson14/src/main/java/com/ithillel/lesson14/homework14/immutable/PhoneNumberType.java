package com.ithillel.lesson14.homework14.immutable;

public enum PhoneNumberType {
    PERSONAL, WORK
}
