package com.ithillel.lesson14.homework14.immutable;

public class PhoneNumber {
    public Long id;
    public String number;
    public PhoneNumberType type;
}
