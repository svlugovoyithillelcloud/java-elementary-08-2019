package com.ithillel.lesson14.hw13_solution;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class CoolNewPerson {
    private Long id;
    private String name;
    private String city;
}
