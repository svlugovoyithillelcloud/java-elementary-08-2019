package com.ithillel.lesson14.hw13_solution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoolPersonHandler {

    public static HashMap<String, String> groupPersonNamesByCity(List<CoolNewPerson> persons) {
        HashMap<String, String> result = new HashMap<>();

        for (CoolNewPerson p : persons) {

            if (result.get(p.getCity()) == null) {
                result.put(p.getCity(), p.getName());
            } else {
                String newValue = result.get(p.getCity()) + ";" + p.getName();
                result.put(p.getCity(), newValue);
            }

        }
        return result;
    }
}


