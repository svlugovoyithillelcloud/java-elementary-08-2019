package com.ithillel.lesson14.streams;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDemo {
    public static void main(String[] args) {

        List<Book> books = Arrays.asList(
                new Book(new Author("John1", "Smith1", 36, Gender.MALE), "Super Book1", 362),
                new Book(new Author("Anne2", "Smith2", 42, Gender.FEMALE), "Super Book2", 762),
                new Book(new Author("Anne2", "Smith2", 42, Gender.FEMALE), "Super Book2", 762),
                new Book(new Author("Anne3", "Smith2", 42, Gender.FEMALE), "Super Book2333", 862),
                new Book(new Author("Anne4", "Smith2", 42, Gender.FEMALE), "Super Book2555", 562),
                new Book(new Author("John3", "Smith3", 22, Gender.MALE), "Super Book3", 302),
                new Book(new Author("Anne4", "Smith4", 66, Gender.FEMALE), "Super Book4", 162),
                new Book(new Author("John5", "Smith5", 15, Gender.MALE), "Super Book5", 562),
                new Book(new Author("Anne6", "Smith6", 34, Gender.FEMALE), "Super Book6", 40),
                new Book(new Author("John7", "Smith7", 40, Gender.MALE), "Super Book7", 262)
        );

        List<String> handled = books.stream()
                .filter(b -> b.getPageCount() > 300)
                .filter(b -> b.getAuthor().getGender().equals(Gender.FEMALE))
                .map(b -> b.getAuthor().getFirstName())
                .map(s -> s.toUpperCase())
                .distinct()
                .limit(2)
                .collect(Collectors.toList());

        System.out.println(handled);

    }
}

@AllArgsConstructor
@Getter
@ToString
class Book {
    private Author author;
    private String name;
    private int pageCount;
}

@AllArgsConstructor
@Getter
@ToString
class Author {
    private String firstName;
    private String lastName;
    private int age;
    private Gender gender;
}

enum Gender {
    MALE, FEMALE
}
