package com.ithillel.lesson14.generics;

import java.util.Arrays;
import java.util.List;

public class GenericsBadDemo_05 {
    public static void main(String[] args) {

        Triple<List<Integer>, Pair<String, String>, Triple<String, Long, StringBuilder>> result = new Triple<>();

        result.first = Arrays.asList(1, 2);
        result.second.first = "Hello";
        result.second.second = "World";
        result.third.first = "Wow";
        result.third.second = 10000000L;
        result.third.third = new StringBuilder("Shit").append("One more shit");
    }
}

class Triple<F, S, T> {
    public F first;
    public S second;
    public T third;
}

class Pair<F, S> {
    public F first;
    public S second;
}
