package com.ithillel.lesson14.generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GenericsCovariantDemo_02 {
    public static void main(String[] args) {

        List<Number> numbers = new ArrayList<>();
        List<Integer> integers = Arrays.asList(1, 2, 3);
//        numbers = integers;
        numbers.add(10.88);

        Number[] nums;
        Integer[] ints = {1, 2, 3};

        nums = ints;
        nums[1] = 29.88F;
    }
}

