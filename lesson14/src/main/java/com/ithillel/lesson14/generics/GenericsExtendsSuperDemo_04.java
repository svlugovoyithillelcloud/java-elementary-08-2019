package com.ithillel.lesson14.generics;

import lombok.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericsExtendsSuperDemo_04 {

    static boolean isValidPeopleList(List<Person> list, Validator<Person> validator) {
//    static boolean isValidPeopleList(List<? extends Person> list, Validator<Person> validator){
        for (Person p : list) {
            if (!validator.isValid(p)) {
                return false;
            }
        }
        list.add(new Person("", ""));
        return true;
    }

    static <T> List<T> filterInvalidPersons(List<T> list, Validator<T> validator) {
//    static <T> List<T> filterInvalidPersons(List<T> list, Validator<? super T> validator) {
        List<T> res = new ArrayList<>();
        for (T el : list) {
            if (!validator.isValid(el)) {
                res.add(el);
            }
        }
        return res;
    }

    public static void main(String[] args) {

        List<Person> people = Arrays.asList(
                new Person("Ivan", "Ivanov"),
                new Person("Petr", "Petrov"),
                new Person("John", "")
        );

        boolean isValid = isValidPeopleList(people, new PersonValidator());
        System.out.println(isValid);

        List<Employee> employees = Arrays.asList(
                new Employee("Ivan", "Ivanov", "IT"),
                new Employee("Petr", "Petrov", "Finance"),
                new Employee("John", "", "HR")
        );

//        boolean isValid2 = isValidPeopleList(employees, new PersonValidator());
//        System.out.println(isValid2);

//        List<Employee> filtered = filterInvalidPersons(employees, new PersonValidator());
//        System.out.println(filtered);
    }

}

@AllArgsConstructor
@Getter
@ToString
class Person {
    private String firstName;
    private String lastName;
}

interface Validator<T> {
    boolean isValid(T value);
}

class PersonValidator implements Validator<Person> {

    @Override
    public boolean isValid(Person person) {
        return !person.getFirstName().isBlank() && !person.getLastName().isBlank();
    }
}

@Getter
@ToString
class Employee extends Person {
    private String department;

    public Employee(String firstName, String lastName, String department) {
        super(firstName, lastName);
        this.department = department;
    }
}
