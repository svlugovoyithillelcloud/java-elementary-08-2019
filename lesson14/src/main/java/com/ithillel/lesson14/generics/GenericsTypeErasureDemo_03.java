package com.ithillel.lesson14.generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericsTypeErasureDemo_03 {
    public static void main(String[] args) {

        List<Integer> integers = Arrays.asList(1, 2, 3);
        List objects = Arrays.asList(1, 2, 3);

        integers.add(4);
        Integer el = integers.get(0);

//        if (integers instanceof List<Integer>){
//
//        }


    }

    public void doSmth(List<String> list){}

//    public void doSmth(List<Number> list){}

}

