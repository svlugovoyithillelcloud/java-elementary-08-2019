package com.ithillel.lesson14.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericsDemo_01 {
    public static void main(String[] args) {

        List strings = new ArrayList();
        strings.add("One");
        strings.add("Two");
        strings.add("Three");

        strings.add(1);
        strings.add(new HelloWorld());

        for (Object string : strings) {
            ((String) string).length();
        }
    }
}

class HelloWorld { }
