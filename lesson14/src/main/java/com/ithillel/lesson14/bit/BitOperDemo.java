package com.ithillel.lesson14.bit;

public class BitOperDemo {
    public static void main(String[] args) {

        int a = 0;
        int b = 1;
        int c = 15;
        int d = -1;
        int e = Integer.MAX_VALUE;
        int f = Integer.MIN_VALUE;

        System.out.println("Представление числа: " + a + " в двоичной системе исчисления: " + Integer.toBinaryString(a));
        System.out.println("Представление числа:" + b + " в двоичной системе исчисления: " + Integer.toBinaryString(b));
        System.out.println("Представление числа:" + c + " в двоичной системе исчисления: " + Integer.toBinaryString(c));
        System.out.println("Представление числа:" + d + " в двоичной системе исчисления: " + keepLeadingZeros(d));
        System.out.println("Представление числа:" + e + " в двоичной системе исчисления: " + keepLeadingZeros(e));
        System.out.println("Представление числа:" + f + " в двоичной системе исчисления: " + Integer.toBinaryString(f));

        int x = 64;
        int y = 2;

        int res1 = x << y;
        System.out.println("64 << 2");
        System.out.println("Представление числа: " + x + " в двоичной системе исчисления: " + keepLeadingZeros(x));
        System.out.println("Представление числа: " + res1 + " в двоичной системе исчисления: " + keepLeadingZeros(res1));

        int res2 = x >> y;
        System.out.println("64 >> 2");
        System.out.println("Представление числа: " + x + " в двоичной системе исчисления: " + keepLeadingZeros(x));
        System.out.println("Представление числа: " + res2 + " в двоичной системе исчисления: " + keepLeadingZeros(res2));

        int res3 = x >>> y;
        System.out.println("64 >>> 2");
        System.out.println("Представление числа: " + x + " в двоичной системе исчисления: " + keepLeadingZeros(x));
        System.out.println("Представление числа: " + res3 + " в двоичной системе исчисления: " + keepLeadingZeros(res3));
    }

    static String keepLeadingZeros (int i){
        return String.format("%32s", Integer.toBinaryString(i)).replace(" ", "0");
    }
}
