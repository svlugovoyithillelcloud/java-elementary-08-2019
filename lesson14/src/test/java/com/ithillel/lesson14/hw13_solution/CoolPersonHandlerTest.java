package com.ithillel.lesson14.hw13_solution;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CoolPersonHandlerTest {

    @Test
    void groupPersonNamesByCity() {
        List<CoolNewPerson> list = new ArrayList<>() {{
            add(new CoolNewPerson(1L, "John", "Kyiv"));
            add(new CoolNewPerson(1L, "Serhii", "Kyiv"));
            add(new CoolNewPerson(1L, "Anne", "Warsaw"));
            add(new CoolNewPerson(1L, "Ivan", "Kyiv"));
            add(new CoolNewPerson(1L, "Ed", "NY"));
            add(new CoolNewPerson(1L, "Rita", "NY"));
        }};

        HashMap<String, String> expected = CoolPersonHandler.groupPersonNamesByCity(list);

        System.out.println(expected);

        assertAll(() -> {
                    assertEquals(3, expected.size());
                    assertTrue(expected.keySet().containsAll(Arrays.asList("Kyiv", "Warsaw", "NY")));
                    assertTrue(expected.get("Kyiv").contains(";"));
                    assertTrue(expected.get("Kyiv").contains("John"));
                    assertTrue(expected.get("Kyiv").contains("Serhii"));
                    assertTrue(expected.get("Kyiv").contains("Ivan"));
                    assertFalse(expected.get("Warsaw").contains(";"));
                    assertTrue(expected.get("Warsaw").contains("Anne"));
                    assertTrue(expected.get("NY").contains(";"));
                    assertTrue(expected.get("NY").contains("Ed"));
                    assertTrue(expected.get("NY").contains("Rita"));
                }
        );
    }
}