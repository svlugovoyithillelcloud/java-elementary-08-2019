package com.ithillel.lesson22;

import java.math.BigInteger;
import java.security.SecureRandom;

public class SimpleProcess {

    public static final int TASK_COUNT = 500000;

    public static void main(String[] args) {
        System.out.println("Starting...");

        Long sum = 0L;
        long start = System.nanoTime();

        for (int i = 0; i <10; i++) {
            SingleProcess sp = new SingleProcess();
            sum += sp.process();
        }
        long end = System.nanoTime();
        System.out.println("Execution time - " + (end - start) / 1000000000 + " sec...");
        System.out.println("Result - " + sum);
    }
}


class SingleProcess {

    public Long process() {
        Long sum = 0L;

        SecureRandom random = new SecureRandom();
        for (int i = 0; i < SimpleProcess.TASK_COUNT; i++) {
            String s = new BigInteger(500, random).toString(32);
            for (char c : s.toCharArray()) {
                sum += c;
            }
        }
        return sum;
    }
}