package com.ithillel.lesson22;

import lombok.Getter;

public class ThreadSync_02 {
    public static void main(String[] args) throws InterruptedException {

        Counter_02 counter = new Counter_02();

        for (int i = 0; i < 200; i++) {
            Process_02 process = new Process_02(counter);
            new Thread(process).start();
        }

        Thread.sleep(2000);
        System.out.println(counter.getCounter());

//        Date date = new Date(1449965478867L);
//        System.out.println(date);


    }
}

class Counter_02 {

    @Getter
    private Long counter = 0L;

    public synchronized void increase() { //synchronized - монитор
        counter++;
    }
}

class Process_02 implements Runnable {
    private Counter_02 counter;

    public Process_02(Counter_02 counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            counter.increase();
        }
    }
}