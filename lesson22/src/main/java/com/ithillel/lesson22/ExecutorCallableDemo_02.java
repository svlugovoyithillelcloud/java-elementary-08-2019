package com.ithillel.lesson22;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorCallableDemo_02 {
    public static void main(String[] args) throws InterruptedException {

        ExecutorService es = Executors.newSingleThreadExecutor();

        Future<String> future = es.submit(new ProcessC());

        boolean done = future.isDone();
        System.out.println(done);

        try {
//            Thread.sleep(4000);
            System.out.println("Before get()");

            String result = future.get(); //можем получить данные

            System.out.println("After get() - " + result); // печатаем результат
        } catch (Exception e) {
            e.printStackTrace(System.out); //ex
        }

        es.shutdown();
    }
}


class ProcessC implements Callable<String> {

    private String str = null;

    @Override
    public String call() {
        try {
            Thread.sleep(2000);
            System.out.println("Ok");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        str.contains("Hello NPE!");
        return "STRING";
    }
}
