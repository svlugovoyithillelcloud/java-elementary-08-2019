package com.ithillel.lesson22;

import lombok.Getter;

import java.util.Date;

public class ThreadSync {
    public static void main(String[] args) throws InterruptedException {

        Counter counter = new Counter();

        for (int i = 0; i < 200; i++) {
            Process process = new Process(counter);
            new Thread(process).start();
        }

        Thread.sleep(2000);
        System.out.println(counter.getCounter());

    }
}

class Counter {

    @Getter
    private Long counter = 0L;

    public void increase() {
        //1. Прочитать текущее значение - 100
        //2. Увеличить на 1 - 101
        //3. Записать новое значение - 101

        counter++;
    }
}

class Process implements Runnable {
    private Counter counter;

    public Process(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            counter.increase();
        }
    }
}