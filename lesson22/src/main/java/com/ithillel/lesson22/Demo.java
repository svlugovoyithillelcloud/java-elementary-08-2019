package com.ithillel.lesson22;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

public class Demo {
    public static void main(String[] args) throws InterruptedException {

        long pid = getPID();

        while (true){
            System.out.println("Working...");
            System.out.println("PID  = " + pid);
        }
    }

    private static long getPID() {
        RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();

        // Get name representing the running Java virtual machine.
        // It returns something like 6460@AURORA. Where the value
        // before the @ symbol is the PID.
        String jvmName = bean.getName();
        System.out.println("Name = " + jvmName);

        // Extract the PID by splitting the string returned by the
        // bean.getName() method.
        return Long.valueOf(jvmName.split("@")[0]);


    }
}
