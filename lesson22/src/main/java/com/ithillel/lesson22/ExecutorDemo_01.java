package com.ithillel.lesson22;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorDemo_01 {
    public static void main(String[] args) throws InterruptedException {

        ExecutorService es = Executors.newSingleThreadExecutor();

        Future<?> future = es.submit(new ProcessDemo()); // аналог - квитанция на руки

        boolean done = future.isDone(); // можем спросить готово или нет
        System.out.println(done);

        try {
//            Thread.sleep(4000);
            System.out.println("Before get()");

            future.get(); //ждет результат, блокирующий

            System.out.println("After get()");
        } catch (Exception e) {
            e.printStackTrace(System.out); //ex
        }

        es.shutdown();
    }
}


class ProcessDemo implements Runnable {

    private String str = null;

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
            System.out.println("Ok");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        str.contains("Hello NPE!");
    }
}
