package com.ithillel.lesson22;

import java.math.BigInteger;
import java.security.SecureRandom;

public class SimpleProcess_04 {

    public static final int TASK_COUNT = 500000;

    public static void main(String[] args) {
        System.out.println("Starting...");

        Long sum = 0L;
        long start = System.nanoTime();

        for (int i = 0; i <10; i++) {
            SingleProcess_04 sp = new SingleProcess_04("ID: " + i);
            Thread thread = new Thread(sp);
            thread.start();
//            sum += sp.process();
        }
        long end = System.nanoTime();
        System.out.println("Execution time - " + (end - start) / 1000000000 + " sec..."); //Execution time - 0 sec...
        System.out.println("Result - " + sum); //Result - 0 - объяснить что происходит...
    }
}


class SingleProcess_04 implements Runnable {

    private String name;

    public SingleProcess_04(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        process();
    }

    public Long process() {
        Long sum = 0L;

        SecureRandom random = new SecureRandom();
        for (int i = 0; i < SimpleProcess.TASK_COUNT; i++) {
            String s = new BigInteger(500, random).toString(32);
            for (char c : s.toCharArray()) {
                sum += c;
            }
        }
        System.out.println(name);
        return sum;
    }
}