package com.ithillel.lesson22;

import lombok.Getter;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class SimpleProcess_05 {

    public static final int TASK_COUNT = 500000;

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Starting...");

        Long sum = 0L;
        long start = System.nanoTime();

        List<SingleProcess_05> list = new ArrayList<>();

        for (int i = 0; i <10; i++) {
            SingleProcess_05 sp = new SingleProcess_05("ID: " + i);
            Thread thread = new Thread(sp);
            thread.start();
//            sum += sp.process();
            list.add(sp);
        }
        long end = System.nanoTime();
        System.out.println("Execution time - " + (end - start) / 1000000000 + " sec..."); //Execution time - 0 sec...
        System.out.println("Result - " + sum); //Result - 0 - объяснить что происходит...


        Thread.sleep(20000);
        for (SingleProcess_05 item : list) {
            System.out.println(item.getSum());
            sum += item.getSum();
        }
        System.out.println("Result - " + sum);
    }
}


class SingleProcess_05 implements Runnable {

    private String name;

    @Getter
    private Long sum;

    public SingleProcess_05(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        sum = process();
    }

    public Long process() {
        Long sum = 0L;

        SecureRandom random = new SecureRandom();
        for (int i = 0; i < SimpleProcess.TASK_COUNT; i++) {
            String s = new BigInteger(500, random).toString(32);
            for (char c : s.toCharArray()) {
                sum += c;
            }
        }
        System.out.println(name);
        return sum;
    }
}