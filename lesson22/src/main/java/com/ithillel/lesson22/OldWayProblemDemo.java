package com.ithillel.lesson22;

public class OldWayProblemDemo {
    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < 3; i++) {
            ProcessOld process = new ProcessOld();
            new Thread(process).start();
        }

        Thread.sleep(5000);

    }
}


class ProcessOld implements Runnable {

    private String str = null;

    @Override
    public void run() {
        str.contains("Hello NPE!");
    }
}
