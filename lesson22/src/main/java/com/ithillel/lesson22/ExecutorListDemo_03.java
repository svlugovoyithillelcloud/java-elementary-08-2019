package com.ithillel.lesson22;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorListDemo_03 {
    public static void main(String[] args) throws Exception {

//        int core = Runtime.getRuntime().availableProcessors();
//        System.out.println(core);

        ExecutorService es = Executors.newFixedThreadPool(3); //пулл потоков

        List<Future<String>> list = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            ProcessL process = new ProcessL();
            Future<String> future = es.submit(process);
            list.add(future); //складываем в список
        }

        for (Future<String> item : list) {
            String res = item.get();
        }

        es.shutdown();
    }
}


class ProcessL implements Callable<String> {

    @Override
    public String call() {
        try {
            System.out.println("Start");
            Thread.sleep(2000 + ((int) (Math.random() * 1000)));
            System.out.println("Stop");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "STRING";
    }
}
