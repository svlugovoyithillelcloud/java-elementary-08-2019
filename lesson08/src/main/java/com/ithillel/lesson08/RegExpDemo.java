package com.ithillel.lesson08;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpDemo {
    public static void main(String[] args) {

        // проверка на соответствие строки шаблону
        Pattern p0 = Pattern.compile(".a.a");
        Matcher m0 = p0.matcher("java");
        System.out.println(m0.matches());

        Pattern p1 = Pattern.compile("a+y");
        Matcher m1 = p1.matcher("aaay");
        System.out.println(m1.matches());

        // поиск и выбор подстроки, заданной шаблоном
        String regex = "(\\w{6,})@(\\w+\\.)([a-z]{2,4})";
        String s = "адреса эл.почты: john@gmail.com, hillel@gmail.com, qwerty@gmail.com, qwerty@gmail.comddddddd!";
        Pattern p2 = Pattern.compile(regex);
        Matcher m2 = p2.matcher(s);
        while (m2.find()) {
            System.out.println("e-mail: " + m2.group());
        }

        // разбиение строки на подстроки с применением шаблона в качестве разделителя
        Pattern p3 = Pattern.compile("\\d+\\s?");
        String[] words = p3.split("java5tiger 77 java6mustang");
        System.out.println(Arrays.toString(words));

        String text = "Мне очень нравится Тайланд. Таиланд это то место куда бы я поехал. тайланд - мечты сбываются!";
        System.out.println(text.replaceAll("[Тт]а[ий]ланд", "Украина"));
    }
}
