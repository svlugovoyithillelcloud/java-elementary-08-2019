package com.ithillel.lesson08;

public class StringBufferDemo {
    public static void main(String[] args) {

        StringBuffer sb = new StringBuffer();
        System.out.println("длина —> " + sb.length());
        System.out.println("размер —>" + sb.capacity());
//         sb = "Java"; // ошибка, только для класса String
        sb.append("Java");
        System.out.println("строка —> " + sb);
        System.out.println("длина —> " + sb.length());
        System.out.println("размер —> " + sb.capacity());
        sb.append("Python");
        System.out.println("строка —> " + sb);
        System.out.println("длина —> " + sb.length());
        System.out.println("размер —> " + sb.capacity());
        System.out.println("реверс —> " + sb.reverse());

        StringBuffer sb1 = new StringBuffer("Java");
        System.out.println(sb1.capacity());
        sb1.append("qwertyuiopasdfghjklzxcvbn1");
        System.out.println(sb1.capacity());
        StringBuffer sb2 = new StringBuffer(32);
        System.out.println(sb2.capacity());
    }

}
