package com.ithillel.lesson08;

public class StringDemo {
    public static void main(String[] args) {

        String separator = "\n=========================";

        String s1 = new String("Hello1");
        char[] chars = {'H', 'e', 'l', 'l', 'o', '2'};
        String s2 = new String(chars);
        String s22 = new String(chars, 3, 2);
        String s3 = "Hello3";

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s22);
        System.out.println(s3 + separator);

        String s4 = s3;
        String s5 = s3.concat("!!!");

        System.out.println(s4 == s3);
        System.out.println((s5 == s3) + separator);

        System.out.println("hello".equals("Hello"));
        System.out.println("hello".equalsIgnoreCase("HellO"));

        System.out.println("hello".substring(2));
        System.out.println("hello".substring(2, 4));
        System.out.println("hello".subSequence(2, 4));

        System.out.println("hello".length());
        System.out.println("hello".indexOf('e'));
        System.out.println("hello".valueOf(21));

        System.out.println("hello".replace('l', 'L'));
        System.out.println("    hello     ".trim());
        System.out.println("hello".charAt(1));
        System.out.println(" ".isEmpty());
        System.out.println("      ".isBlank() + separator);

        String big = "java python js kotlin c++";
        String[] strings = big.split(" ");
        for (String item : strings) {
            System.out.println(item + "!!!");
        }
    }
}
