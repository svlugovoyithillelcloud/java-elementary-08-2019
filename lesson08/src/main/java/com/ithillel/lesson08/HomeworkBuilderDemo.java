package com.ithillel.lesson08;

public class HomeworkBuilderDemo {
    public static void main(String[] args) {

        MyPerson myPerson = new MyPerson.Builder(11L)
                .firstName("John1")
                .lastName("Smith1")
                .email("jo1@gmail.com")
                .city("NY")
                .build();
        System.out.println(myPerson);
//        MyPerson{id=11, firstName='John1', lastName='Smith1', email='jo1@gmail.com', city='NY'}

        MyPerson myDefaultPerson = new MyPerson.Builder(999L)
                .build();

        System.out.println(myDefaultPerson);
//        MyPerson{id=999, firstName='unknown', lastName='unknown', email='unknown', city='unknown'}

        LombokPerson lombokPerson = LombokPerson.builder()
                .id(10L)
                .firstName("John")
                .build();

        System.out.println(lombokPerson);
//        LombokPerson(id=10, firstName=John, lastName=null, email=null, city=null)
    }
}

class MyPerson {
    private final Long id;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String city;

    private MyPerson(Builder builder) {
        id = builder.id;
        firstName = builder.firstName;
        lastName = builder.lastName;
        email = builder.email;
        city = builder.city;
    }

    public static class Builder {
        //required
        private final Long id;

        //com.ithillel.lesson15.optional with default values
        private String firstName = "unknown";
        private String lastName = "unknown";
        private String email = "unknown";
        private String city = "unknown";

        public Builder(Long id) {
            this.id = id;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public MyPerson build() {
            return new MyPerson(this);
        }
    }

    @Override
    public String toString() {
        return "MyPerson{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}

class LombokPerson {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String city;

    private LombokPerson(Long id, String firstName, String lastName, String email, String city) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.city = city;
    }

    public static LombokPerson.LombokPersonBuilder builder() {
        return new LombokPerson.LombokPersonBuilder();
    }

    public String toString() {
        return "LombokPerson(id=" + this.id + ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", email=" + this.email + ", city=" + this.city + ")";
    }

    public static class LombokPersonBuilder {
        private Long id;
        private String firstName;
        private String lastName;
        private String email;
        private String city;

        LombokPersonBuilder() {
        }

        public LombokPerson.LombokPersonBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public LombokPerson.LombokPersonBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public LombokPerson.LombokPersonBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public LombokPerson.LombokPersonBuilder email(String email) {
            this.email = email;
            return this;
        }

        public LombokPerson.LombokPersonBuilder city(String city) {
            this.city = city;
            return this;
        }

        public LombokPerson build() {
            return new LombokPerson(this.id, this.firstName, this.lastName, this.email, this.city);
        }
    }

}