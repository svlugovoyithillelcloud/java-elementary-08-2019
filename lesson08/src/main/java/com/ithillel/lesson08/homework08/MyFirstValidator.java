package com.ithillel.lesson08.homework08;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyFirstValidator {

    //aaa@gmail.com
    public boolean validateGoogleEmail(String email){
        Pattern pattern = Pattern.compile("(\\w{3,})@gmail.com");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    //+380939997755, операторы 093 063 097 096 066 073 091 094 067 068 050
    public boolean validateUkrPhoneNumber(String phone){
        Pattern pattern = Pattern.compile("\\+380(93|63|97|96|66|73|91|94|67|68|50)\\d{7}");
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    //92.60.179.57
    public boolean validateIP4address(String address){
        Pattern pattern = Pattern.compile("\\b([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\b\\." +
                "\\b([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\b\\." +
                "\\b([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\b\\." +
                "\\b([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\b");
        Matcher matcher = pattern.matcher(address);
        return matcher.matches();
    }
}
