package com.ithillel.lesson08;

import java.util.Calendar;
import java.util.Formatter;

public class FormatDemo {
    public static void main(String[] args) {
        Formatter f = new Formatter(); // объявление объекта

        // форматирование текста по формату %S, %c
        f.format("This %s is about %n%S %c", "book", "java", '8');
        System.out.println(f);

        f = new Formatter();
        f.format("Hex: %x, Octal: %o", 11, 100);
        System.out.println(f);

        Calendar cal = Calendar.getInstance();
        f = new Formatter();

        // вывод в 12-часовом временном формате
        f.format("%tr", cal);
        System.out.println(f);

        // полноформатный вывод времени и даты
        f = new Formatter();
        f.format("%tc", cal);
        System.out.println(f);

        // вывод текущего часа и минуты
        f = new Formatter();
        f.format("%tl:%tM", cal, cal);
        System.out.println(f);

        // всевозможный вывод месяца
        f = new Formatter();
        f.format("%tB %tb %tm", cal, cal, cal);
        System.out.println(f);

        // выравнивание вправо
        f = new Formatter();
        f.format("|%20.2f|", 123.123);
        System.out.println(f);

        // задание точности представления для чисел
        f = new Formatter();
        f.format("%.3f", 1111.1111111);
        System.out.println(f);

        // задание точности представления для строк
        f = new Formatter();
        f.format("%.16s", "Now I know class java.util.Formatter");
        System.out.println(f);
    }

}
