package com.ithillel.lesson08.homework08;

import static org.junit.jupiter.api.Assertions.*;

class MyStringUtilsTest {

    @org.junit.jupiter.api.Test
    void reverseString() {
        String expected = "tseT";
        assertEquals(expected, MyStringUtils.reverseString("Test"));
        assertNotEquals(expected, MyStringUtils.reverseString("test"));
    }

    @org.junit.jupiter.api.Test
    void isPalindrome() {
        assertTrue(MyStringUtils.isPalindrome("hannah"));
        assertFalse(MyStringUtils.isPalindrome("hannaha"));
    }
}