package com.ithillel.lesson08.homework08;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MyFirstValidatorTest {

    @Test
    void validateGoogleEmail() {
        MyFirstValidator validator = new MyFirstValidator();
        assertTrue(validator.validateGoogleEmail("AsDfGh@gmail.com"));
        assertFalse(validator.validateGoogleEmail("Assd@gmailcom"));
        assertFalse(validator.validateGoogleEmail("As@gmail.com"));
        assertFalse(validator.validateGoogleEmail("Ash@gmai.com"));
        assertFalse(validator.validateGoogleEmail("Asd@gmail.co"));
    }

    @Test
    void validateUkrPhoneNumber() {
        MyFirstValidator validator = new MyFirstValidator();
        assertTrue(validator.validateUkrPhoneNumber("+380500129505"));
        assertFalse(validator.validateUkrPhoneNumber("+38050012950"));
        assertFalse(validator.validateUkrPhoneNumber("+3805001295o5"));
        assertFalse(validator.validateUkrPhoneNumber("+380510129505"));
        assertFalse(validator.validateUkrPhoneNumber("+390500129505"));
        assertFalse(validator.validateUkrPhoneNumber("380500129505"));
    }

    @Test
    void validateIP4address() {
        MyFirstValidator validator = new MyFirstValidator();
        assertTrue(validator.validateIP4address("0.10.254.255"));
        assertFalse(validator.validateIP4address("256.255.255.255"));
        assertFalse(validator.validateIP4address("-1.10.196.24"));
        assertFalse(validator.validateIP4address("1.10196.24"));
        assertFalse(validator.validateIP4address("1.1 0.196.24"));
        assertFalse(validator.validateIP4address("1.l0.196.24"));

    }
}