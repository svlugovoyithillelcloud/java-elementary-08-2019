package com.ithillel.lesson08.homework08;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyFirstValidatorTest {

    private MyFirstValidator validator;

    @BeforeEach
    void setUp() {
        validator = new MyFirstValidator();
    }

    @Test
    void validateGoogleEmailPositive() {
        assertAll("Should return true",
                () -> assertTrue(validator.validateGoogleEmail("qwerty@gmail.com")),
                () -> assertTrue(validator.validateGoogleEmail("qwe.qwe@gmail.com")),
                () -> assertTrue(validator.validateGoogleEmail("qwe1990@gmail.com"))
        );
    }

    @Test
    void validateGoogleEmailNegative() {
        assertAll("Should return false",
                () -> assertFalse(validator.validateGoogleEmail("@gmail.com")),
                () -> assertFalse(validator.validateGoogleEmail("qwe@ggmail.cm")),
                () -> assertFalse(validator.validateGoogleEmail("qwe1990@gmall.com"))
        );
    }

    @Test
    void validateUkrPhoneNumberPositive() {
        assertAll("Should return true",
                () -> assertTrue(validator.validateUkrPhoneNumber("+380939997755")),
                () -> assertTrue(validator.validateUkrPhoneNumber("+380679997755")),
                () -> assertTrue(validator.validateUkrPhoneNumber("+380639997755"))
        );
    }

    @Test
    void validateUkrPhoneNumberNegative() {
        assertAll("Should return false",
                () -> assertFalse(validator.validateUkrPhoneNumber("qwe@gmail.com")),
                () -> assertFalse(validator.validateUkrPhoneNumber("380939997755")),
                () -> assertFalse(validator.validateUkrPhoneNumber("+380589997755"))
        );
    }

    @Test
    void validateIP4address() {
        assertAll("Should validate ipv4",
                () -> assertTrue(validator.validateIP4address("127.0.0.1")),
                () -> assertFalse(validator.validateIP4address("500.130.130.55")),
                () -> assertFalse(validator.validateIP4address("qwe.130.130.55")),
                () -> assertFalse(validator.validateIP4address(null))
        );
    }
}