package com.ithillel.lesson08.homework08;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyStringUtilsTest {

    @Test
    void reverseString() {
        assertAll("Should reverse string",
                () -> assertEquals("olleh", MyStringUtils.reverseString("hello")),
                () -> assertEquals("", MyStringUtils.reverseString("")),
                () -> assertEquals("o", MyStringUtils.reverseString("o"))
        );
    }

    @Test
    void reverseStringWithException() {
        assertThrows(IllegalArgumentException.class, () -> {
            MyStringUtils.reverseString(null);
        });
    }

    @Test
    void isPalindromePositive() {
        assertAll("Should return true",
                () -> assertTrue(MyStringUtils.isPalindrome("level")),
                () -> assertTrue(MyStringUtils.isPalindrome("leveL")),
                () -> assertTrue(MyStringUtils.isPalindrome("f")),
                () -> assertTrue(MyStringUtils.isPalindrome("bob bob"))
        );
    }

    @Test
    void isPalindromeNegative() {
        assertAll("Should return false",
                () -> assertFalse(MyStringUtils.isPalindrome("hello")),
                () -> assertFalse(MyStringUtils.isPalindrome("fa")),
                () -> assertFalse(MyStringUtils.isPalindrome("")),
                () -> assertFalse(MyStringUtils.isPalindrome("   ")),
                () -> assertFalse(MyStringUtils.isPalindrome(null))
        );
    }
}