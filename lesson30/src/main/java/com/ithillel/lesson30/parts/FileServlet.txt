@WebServlet(urlPatterns = {"/file"})
@MultipartConfig(location = "/Users/serhiiluhovyi/Movies")
public class FileServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        for (Part part : req.getParts()) {
            if (part.getName().equals("author-name")) {
                InputStream inputStream = part.getInputStream();
                InputStreamReader isr = new InputStreamReader(inputStream);
                String authorName = new BufferedReader(isr)
                        .lines()
                        .collect(Collectors.joining("\n"));
                log(authorName);
            } else {
                part.write(UUID.randomUUID().toString() + part.getSubmittedFileName());
            }
        }

        resp.sendRedirect("/lesson30/static/success.html");
    }

}
