1) packaging - war
2) servlet-api
3) MainServlet extends HttpServlet - lifecycle Servlet + HttpServlet overview
4) MainServlet - override methods - 3
5) webapp -> WEB-INF -> web.xml
6) tomcat start
7) war -> tomcat
8) check
http://localhost:8080/lesson30-1.0-SNAPSHOT/my-servlet
9) implement doGet()
10) check result
http://localhost:8080/lesson30-1.0-SNAPSHOT/my-servlet
11) setup deploy from idea

1) SecondServlet extends HttpServlet - @WebServlet(urlPatterns = {"/second-servlet", "/my-cool-servlet/*"})
2) implement doGet()
http://localhost:8080/lesson30/second-servlet
http://localhost:8080/lesson30/second-servlet?one=12&page=555&one=one_more_value
http://localhost:8080/lesson30/my-cool-servlet/12/status/?one=12&page=555&one=one_more_value

1) ThirdServlet
http://localhost:8080/lesson30/static/form.html
    <servlet-mapping>
        <servlet-name>default</servlet-name>
        <url-pattern>/*</url-pattern>
    </servlet-mapping>

1) FileServlet demo

