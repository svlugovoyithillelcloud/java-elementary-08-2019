package com.ithillel.lesson30;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MainServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        log("Method init.");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log("Method service...");
        resp.getWriter().write("Method service start\n");
        super.service(req, resp);
        resp.getWriter().write("Method service end\n");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log("Method doGet...");
        resp.getWriter().write("Method doGet\n");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log("Method doPost...");
        resp.getWriter().write("Method doPost\n");
    }

    @Override
    public void destroy() {
        log("Method destroy.");
    }

}

