package com.ithillel.lesson10.hw09_solution.exception;

public class MyNegativeIntegerException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "%s is not a positive integer value.";

    public MyNegativeIntegerException(String value) {
        super(String.format(MESSAGE_TEMPLATE, value));
    }
}
