package com.ithillel.lesson10.hw09_solution.exception;

public class MyGreatThenMaxIntegerException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "%s is great than max integer value.";

    public MyGreatThenMaxIntegerException(String value) {
        super(String.format(MESSAGE_TEMPLATE, value));
    }
}
