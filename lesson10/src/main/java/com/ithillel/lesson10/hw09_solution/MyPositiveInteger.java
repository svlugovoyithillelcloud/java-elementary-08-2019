package com.ithillel.lesson10.hw09_solution;

import com.ithillel.lesson10.hw09_solution.exception.MyGreatThenMaxIntegerException;
import com.ithillel.lesson10.hw09_solution.exception.MyNegativeIntegerException;
import com.ithillel.lesson10.hw09_solution.exception.MyNumberFormatException;

public class MyPositiveInteger {

    static int parseInt(String s) {
        if (s == null) {
            throw new MyNumberFormatException("null");
        }

        if ((int) s.charAt(0) == 45) {
            throw new MyNegativeIntegerException(s);
        }

        if (s.length() > String.valueOf(Integer.MAX_VALUE).length()) {
            throw new MyGreatThenMaxIntegerException(s);
        }

        int num = 0;
        for (int i = 0; i < s.length(); i++) {
            if (((int) s.charAt(i) >= 48) && ((int) s.charAt(i) <= 57)) {
                num = num * 10 + ((int) s.charAt(i) - 48);
            } else {
                throw new MyNumberFormatException(s);
            }

        }
        return num;
    }
}