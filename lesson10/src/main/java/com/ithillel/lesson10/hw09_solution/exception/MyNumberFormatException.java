package com.ithillel.lesson10.hw09_solution.exception;

public class MyNumberFormatException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "%s is not a integer.";

    public MyNumberFormatException(String value) {
        super(String.format(MESSAGE_TEMPLATE, value));
    }
}
