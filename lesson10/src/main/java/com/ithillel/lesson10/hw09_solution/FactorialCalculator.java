package com.ithillel.lesson10.hw09_solution;

import java.util.stream.IntStream;

public class FactorialCalculator {

    public int calculateFactorial(int i) {
        return System.nanoTime() % 2 == 0
                ? factorialUsingForLoop(i)
                : factorialUsingStreams(i);
    }

    private int factorialUsingForLoop(int n) {
        int fact = 1;
        for (int i = 2; i <= n; i++) {
            fact = fact * i;
        }
        return fact;
    }

    private int factorialUsingStreams(int n) {
        return IntStream.rangeClosed(1, n)
                .reduce(1, (int x, int y) -> x * y);
    }

}
