package com.ithillel.lesson10;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileSystemDemoNio_07 {
    public static void main(String[] args) throws IOException {

        Path path = Paths.get("/Users/serhiiluhovyi/Movies/", "demo.txt");
        OutputStream os = new FileOutputStream("/Users/serhiiluhovyi/Movies/copy_demo_files.txt");

        Files.copy(path, os);

        Stream<String> lines = Files.lines(path);

        lines
                .map(s -> ">>> " + s + " <<<")
                .forEach(System.out::println);

    }
}
