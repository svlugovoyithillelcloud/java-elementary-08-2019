public class Summator {
    public static void main(String[] args) {

        int sum = Summator.sum(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println("Result: " + sum);

    }

    public static int sum(int a, int b){
        return a + b;
    }
}
