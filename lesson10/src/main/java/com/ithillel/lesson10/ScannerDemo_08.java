package com.ithillel.lesson10;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class ScannerDemo_08 {
    public static void main(String[] args) {

        String filename = "/Users/serhiiluhovyi/Movies/scan.txt";
        Scanner scan = null;

        try {
            FileReader fr = new FileReader(filename);
            scan = new Scanner(fr);

            // чтение из файла
            while (scan.hasNext()) {
                if (scan.hasNextInt()) {
                    System.out.println(scan.nextInt() + " :int");
                } else if (scan.hasNextBoolean()) {
                    System.out.println(scan.nextBoolean() + " :boolean");
                } else if (scan.hasNextDouble()) {
                    System.out.println(scan.nextDouble() + " :double");
                } else {
                    System.out.println(scan.next() + " :String");
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println(e);
        } finally {
            if (scan != null) {
                scan.close();
            }
        }

        System.out.print("Enter a string : ");
        Scanner scanner = new Scanner(System. in);
        String inputString = scanner.nextLine();
        System.out.println("String read from console is : \n"+inputString);

    }
}
