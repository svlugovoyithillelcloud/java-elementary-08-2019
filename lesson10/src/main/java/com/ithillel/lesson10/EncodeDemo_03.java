package com.ithillel.lesson10;

import java.io.*;

public class EncodeDemo_03 {
    public static void main(String[] args) throws IOException {

        FileInputStream fis = new FileInputStream("/Users/serhiiluhovyi/Movies/demo.txt");
        Reader fr = new InputStreamReader(fis, "cp1251");
        BufferedReader br = new BufferedReader(fr);

        String line = br.readLine();

        while (line !=null){
            System.out.println(line);
            line = br.readLine();
        }

        br.close();
        fr.close();
        fis.close();

    }
}
