package com.ithillel.lesson10.homework10.stats;

public class TextStatistics {
    private String fileName;
    private String absolutePathToFile;
    private int linesCount;
    private int wordsCount;
    private int maxLineLength;
    private int minLineLength;
    private int spasesCount;
}
