package com.ithillel.lesson10;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

public class FileSystemDemo_04 {
    public static void main(String[] args) throws IOException {

        File f = new File("/Users/serhiiluhovyi/Movies/copy_demo.txt");
        File dir = new File("/Users/serhiiluhovyi/Movies/1");
        File dirs = new File("/Users/serhiiluhovyi/Movies/2/3/4");
        File dir0 = new File("/Users/serhiiluhovyi/Movies/");

        System.out.println("exists : " + f.exists());
        System.out.println("canExecute : " + f.canExecute());
        System.out.println("canRead : " + f.canRead());
        System.out.println("canWrite : " + f.canWrite());
        System.out.println("getAbsolutePath : " + f.getAbsolutePath());
        System.out.println("length : " + f.length());
        System.out.println("lastModified : " + new Date(f.lastModified()));
        f.renameTo(new File("/Users/serhiiluhovyi/Movies/copy_demo55555.txt"));
//        f.delete();

//        dir.mkdir();
//        dirs.mkdirs();
        System.out.println(Arrays.toString(dir0.list()));

    }
}
