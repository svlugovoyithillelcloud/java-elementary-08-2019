package com.ithillel.lesson10;

import java.io.*;

public class FileByteCopyDemo_01 {
    public static void main(String[] args) throws Exception {
        copyByteByByte();
//        copyWithBytesBuffer();
//        copyTryWithResources();
    }

    private static void copyByteByByte() throws Exception {
        InputStream is = new FileInputStream("/Users/serhiiluhovyi/Movies/ruby.mp4");
        OutputStream os = new FileOutputStream("/Users/serhiiluhovyi/Movies/copy_ruby.mp4");

        int r = is.read();
        while (r != -1) {
            os.write(r);
            r = is.read();
        }
        is.close();
        os.close();
        //calculate exec time
    }

    ;

    private static void copyWithBytesBuffer() {
        InputStream is = null;
        OutputStream os = null;

        try {
            is = new FileInputStream("/Users/serhiiluhovyi/Movies/vcs.mp4");
            os = new FileOutputStream("/Users/serhiiluhovyi/Movies/copy_vcs2.mp4");

            byte[] buffer = new byte[4096];
            int r = is.read(buffer);
            while (r != -1) {
                os.write(buffer, 0, r);
                r = is.read(buffer);
            }

        } catch (FileNotFoundException e) {
            //log.error("...");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // ignore exception while closing
                }
            }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    // ignore exception while closing
                }
            }
        }
    }

    private static void copyTryWithResources() {
        try (InputStream is = new FileInputStream("/Users/serhiiluhovyi/Movies/vcs.mp4");
             OutputStream os = new FileOutputStream("/Users/serhiiluhovyi/Movies/copy_vcs3.mp4");
        ) {
            byte[] buffer = new byte[4096];
            int r = is.read(buffer);
            while (r != -1) {
                os.write(buffer, 0, r);
                r = is.read(buffer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
