package com.ithillel.lesson10;

import java.io.*;

public class SerializationDemo_09 {
    public static void main(String[] args) {

        // создание и запись объекта
        Student.university = "KPI";
        Student ob = new Student(11L, "John Smith", "qwerty123");
        System.out.println(ob);

        String file = "/Users/serhiiluhovyi/Movies/student.data";
        Serializator sz = new Serializator();
        boolean b = sz.serialization(ob, file);
        Student.university = "NAU"; // изменение значения static-поля
        System.out.println(ob);

        // чтение и вывод объекта
        Student res = null;
        try {
            res = sz.deserialization(file);
        } catch (InvalidObjectException e) {
            e.printStackTrace();
        }
        Student.university = "Garward"; // изменение значения static-поля
        System.out.println(res);
    }
}

class Student implements Serializable {
    private long id;
    private String name;
    private transient String password;
    protected static String university;
    private static final long serialVersionUID = 1L;

    public Student(long id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Student{" +
                "university=" + university +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

class Serializator {
    public boolean serialization(Student s, String fileName) {
        boolean flag = false;
        File f = new File(fileName);
        ObjectOutputStream ostream = null;
        try {
            FileOutputStream fos = new FileOutputStream(f);
            if (fos != null) {
                ostream = new ObjectOutputStream(fos);
                ostream.writeObject(s); // сериализация
                flag = true;

            }
        } catch (FileNotFoundException e) {
            System.err.println("Файл не может быть создан: " + e);
        } catch (NotSerializableException e) {
            System.err.println("Класс не поддерживает сериализацию: " + e);
        } catch (IOException e) {
            System.err.println(e);
        } finally {
            try {
                if (ostream != null) {
                    ostream.close();
                }
            } catch (IOException e) {
                System.err.println("ошибка закрытия потока");
            }
        }
        return flag;
    }

    public Student deserialization(String fileName) throws InvalidObjectException {
        File fr = new File(fileName);
        ObjectInputStream istream = null;
        try {
            FileInputStream fis = new FileInputStream(fr);
            istream = new ObjectInputStream(fis);
            // десериализация
            Student st = (Student) istream.readObject();
            return st;
        } catch (ClassNotFoundException ce) {
            System.err.println("Класс не существует: " + ce);
        } catch (FileNotFoundException e) {
            System.err.println("Файл для десериализации не существует: " + e);
        } catch (InvalidClassException ioe) {
            System.err.println("Несовпадение версий классов: " + ioe);
        } catch (IOException ioe) {
            System.err.println("Общая I/O ошибка: " + ioe);
        } finally {
            try {
                if (istream != null) {
                    istream.close();
                }
            } catch (IOException e) {
                System.err.println("ошибка закрытия потока ");
            }
        }
        throw new InvalidObjectException("объект не восстановлен");
    }
}