package com.ithillel.lesson10;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class NIOChannelDemo_06 {
    public static void main(String[] args) throws IOException {

//        channelDemo();
        transferDemo();

    }

    private static void channelDemo() throws IOException {
        FileInputStream fis = new FileInputStream("/Users/serhiiluhovyi/Movies/vcs.mp4");
        FileOutputStream fos = new FileOutputStream("/Users/serhiiluhovyi/Movies/copy_vcs_nio.mp4");

        FileChannel inChannel = fis.getChannel();
        FileChannel outChannel = fos.getChannel();

        ByteBuffer inBb = ByteBuffer.allocate(4096);
        ByteBuffer outBb = ByteBuffer.allocate(4096);

        int r = inChannel.read(inBb);
        while (r != -1) {
            inBb.flip();
            while (inBb.hasRemaining()) {
                byte b = inBb.get();
                outBb.put(b);
            }

            outBb.flip();
            outChannel.write(outBb);

            inBb.clear();
            outBb.clear();

            r = inChannel.read(inBb);
        }
        fis.close();
        fos.close();
    }

    private static void transferDemo() throws IOException {
        FileInputStream fis = new FileInputStream("/Users/serhiiluhovyi/Movies/vcs.mp4");
        FileOutputStream fos = new FileOutputStream("/Users/serhiiluhovyi/Movies/copy_vcs_nio_transfer.mp4");

        FileChannel inChannel = fis.getChannel();
        FileChannel outChannel = fos.getChannel();

        inChannel.transferTo(0, inChannel.size(), outChannel);

        fis.close();
        fos.close();
    }
}
