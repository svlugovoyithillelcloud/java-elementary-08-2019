package com.ithillel.lesson10;

import java.io.*;

public class ReaderWriterDemo_02 {
    public static void main(String[] args) throws Exception {
//        copyCharByChar();
        copyWithCharsBuffer();
    }

    private static void copyCharByChar() throws Exception {
        Reader r = new FileReader("/Users/serhiiluhovyi/Movies/demo.txt");
        Writer w = new FileWriter("/Users/serhiiluhovyi/Movies/copy_demo1.txt");

        int c = r.read();
        while (c != -1) {
            w.write(c);
            c = r.read();
        }
        r.close();
        w.close();
    }

    private static void copyWithCharsBuffer() throws Exception {
        Reader r = new FileReader("/Users/serhiiluhovyi/Movies/demo.txt");
        Writer w = new FileWriter("/Users/serhiiluhovyi/Movies/copy_demo2.txt");

        char[] buffer = new char[4096];
        int c = r.read(buffer);
        while (c != -1) {
            w.write(buffer, 0, c);
            c = r.read(buffer);
        }
        r.close();
        w.close();
    }


}
