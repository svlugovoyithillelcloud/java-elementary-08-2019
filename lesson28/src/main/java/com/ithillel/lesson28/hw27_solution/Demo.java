package com.ithillel.lesson28.hw27_solution;

import java.util.stream.Stream;

public class Demo {

    public static void main(String[] args) {

        Stream<String> stream = Stream.of("a", "ab", "c", "ad", "e");

        try {
            stream
                    .filter(el -> el.startsWith("a"))
                    .map(el -> {
                        throw new RuntimeException();
                    });
        } catch (Exception ex) {
            System.out.println("rte");
        }


    }
}
