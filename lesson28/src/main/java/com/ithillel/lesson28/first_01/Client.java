package com.ithillel.lesson28.first_01;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Client {

    public static final int PORT = 19000;
    public static final String HOST = "localhost";

    public static void main(String[] args) {

        Socket socket = null;
        try {
            socket = new Socket(HOST, PORT);

            try (InputStream in = socket.getInputStream();
                 OutputStream out = socket.getOutputStream()) {

                String line = "Hello World!!!";
                out.write(line.getBytes());
                out.flush();

                System.out.println("Send message to sever - " + line);

                byte[] buf = new byte[32 * 1024];
                int readBytes = in.read(buf);

                System.out.printf("Server response >>> %s", new String(buf, 0, readBytes));

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            IoUtil.closeQuietly(socket);
        }
    }
}
