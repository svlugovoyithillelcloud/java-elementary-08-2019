package com.ithillel.lesson28.first_01;

import java.net.ServerSocket;
import java.net.Socket;

public class IoUtil {

    public static void closeQuietly(Socket socket) {
        try {
            socket.close();
        } catch (Exception e) {
        }
    }

    public static void closeQuietly(ServerSocket socket) {
        try {
            socket.close();
        } catch (Exception e) {
        }
    }
}
