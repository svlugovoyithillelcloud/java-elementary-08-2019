package com.ithillel.lesson28.first_01;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static final int PORT = 19000;

    public static void main(String[] args) {

        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(PORT);
            System.out.println("Server started, waiting for connection");

            Socket socket = serverSocket.accept();
            System.out.println("Accepted. " + serverSocket.getInetAddress());

            try (InputStream in = socket.getInputStream();
                 OutputStream out = socket.getOutputStream()) {

                byte[] buf = new byte[32 * 1024];
                int readBytes = in.read(buf);
                String line = new String(buf, 0, readBytes);
                System.out.println("Received Client message >>> " + line);

                String response = "!!!===== " + line + " =====!!!";

                out.write(response.getBytes());
                out.flush();

                System.out.println("Send response to client");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            IoUtil.closeQuietly(serverSocket);
        }
    }
}
