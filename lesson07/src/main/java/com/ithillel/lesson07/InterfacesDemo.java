package com.ithillel.lesson07;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class InterfacesDemo {
    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(2, 3);
        ShapeAction<Rectangle> rectangleAction = new RectangleAction();
        System.out.println(rectangleAction.computeSquare(rectangle));
    }
}

interface ShapeAction <T extends AbstractShape> {
    double computeSquare(T shape);
}

abstract class AbstractShape {}

@AllArgsConstructor
@Getter
class Rectangle extends AbstractShape {
    private double a;
    private double b;
}

class RectangleAction implements ShapeAction<Rectangle> {
    @Override
    public double computeSquare(Rectangle shape) {
        return shape.getA() * shape.getB();
    }
}

class Square {

}