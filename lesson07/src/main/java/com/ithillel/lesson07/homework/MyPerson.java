package com.ithillel.lesson07.homework;

public class MyPerson {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String city;

    public MyPerson(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
        this.city = builder.city;
    }

    public static MyPerson.Builder builder() {
        return new MyPerson.Builder();
    }

    public static class Builder {
        private Long id = 999L;
        private String firstName = "unknown";
        private String lastName = "unknown";
        private String email = "unknown";
        private String city = "unknown";

        Builder() {

        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public MyPerson build() {
            return new MyPerson(this);
        }

    }

    @Override
    public String toString() {
        return "MyPerson{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
